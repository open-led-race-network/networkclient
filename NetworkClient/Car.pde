/**
 * Car Class
 */
static class Car {
     int     id;
     String  name;
     String  curDev;    // in which OLR is now the Car (current race)    
     String  status;    // Racing, Leving, Leave
     int     speed;     // used on Car Leave, Car Enter

     Car ()  {
       this.id = -1;
       this.name = null;
       this.curDev=null;
       this.status=null;
       this.speed=-1;
     }
     Car (int id, String name )  {
       this.id = id;
       this.name = name;
       this.curDev="NONE";
       this.status=null;
       this.speed=-1;
     }
     Car (int id, String name, String cDev )  {
       this.id = id;
       this.name = name;
       this.curDev=cDev;
       this.status=null;
       this.speed=-1;
     }
     Car (int id, String name, String cDev, String s )  {
       this.id = id;
       this.name = name;
       this.curDev=cDev;
       this.status=s;
       this.speed=-1;
     }

    void setFields(int id, String name, String cDev, String s, int speed) {
       this.id = id;
       this.name = name;
       this.curDev=cDev;
       this.status=s;
       this.speed=speed;
    }
    
    int getId(){
      return(this.id);
    }

    String getName(){
      return(this.name);
    }

    boolean curDevIsValid() {
      if(this.curDev == null || this.curDev.equals("NONE") ){
        return(false);
      }
      return(true);
    }
    String getCurDev(){
      return(this.curDev);
    }
    void setCurDev(String cDev){
      this.curDev=cDev;
    }

    String getStatus(){
      return(this.status);
    }
    void setStatus(String s){
      this.status=s;
    }
    
    void setSpeed(int n){
      this.speed=n;  
    }
    int getSpeed(){
      return(this.speed);
    }
    
    public String toString ()  {
      return ("id:" + id + " name:" + name + " curDev:" + curDev + " status:" + status );
    }
} 



/**
 * CarTelemetry Class
 * Holds telemetry data   
 */
class CarTelemetry {
    int     id;
    String  trackId;
    int     lap;
    int     rPos;

     CarTelemetry ()  {
       this.id = -1;
       this.trackId = null;
       this.lap = -1;
       this.rPos = -1;
     }
     CarTelemetry (int id, String tid, int lap, int rPos )  {
       this.id = id;
       this.trackId = tid;
       this.lap = lap;
       this.rPos = rPos;
     }

    void setFields(int id, String tid, int lap, int rPos ) {
       this.id = id;
       this.trackId = tid;
       this.lap = lap;
       this.rPos = rPos;
    }
    
    public String toString ()  {
      return ("id:" + id + " trackId:" + trackId + " lap:" + lap + " rPos:" + rPos );
    }
} 
