/**
 *  Generic Item with attributes:
 *    id          Unique Identifier 
 *    name        Name
 *    status      Status 
 *    timeStamp   Last modification time
 */

class OLRNBaseItem {
  public String _id;  
  public String _name;  
  public String _status;  
  public String _tstamp;

  OLRNBaseItem ()   {
    this._id     = null;
    this._name   = null;
    this._status = null;
    this._tstamp = LocalDateTime.now().toString(); 
  }

  OLRNBaseItem (String _id, String _name, String _status)   {
    this._id     = _id.trim();
    this._name   = _name;
    this._status = _status;
    this._tstamp = LocalDateTime.now().toString(); 
  }


  /**
   *  used when the obj was created with empty fields (OLRNBaseItem() constructor)
   */
  void setFields(String id, String name, String status) {
    this._id     = id;
    this._name   = name;
    this._status = status;
    this._tstamp = LocalDateTime.now().toString(); 
  }

  void updStatus(String status) {
    this._status = status;
    this._tstamp = LocalDateTime.now().toString(); 
  }

  void updNameStatus(String name, String status) {
    this._name   = name;
    this._status = status;
    this._tstamp = LocalDateTime.now().toString(); 
  }
  boolean chkParams(String dStr, String id, String name, String status) {
    if( name == null || name.length() == 0){
      System.out.printf("[%s] ERROR - Empty [name] param\n",dStr);
      return false;
    }
    return(chkParams(dStr,id,status));
  }
  
  boolean chkParams(String dStr, String id, String status) {
    if( status == null || status.length() == 0){
      System.out.printf("[%s] ERROR - Empty [status] param\n",dStr);
      return false;
    }
    return(chkParams(dStr,id));
  }
  
  boolean chkParams(String dStr, String id) {
    if( id == null || id.length() == 0){
      System.out.printf("[%s] ERROR - Empty [Id] param\n",dStr);
      return false;
    }
    return(true);
  }
  
} 



class NetworkMessage {
  String topic;
  MqttMessage message;

  NetworkMessage(String topic, MqttMessage message) {
    this.topic = topic;
    this.message = message;
  }
}




class NetworkMessageOLD {
  int        priority ;
  String     topic;
  JSONObject jsonPayload;
  boolean    isRetained;
  int        qos;

  NetworkMessageOLD(String txt, JSONObject json) {
    this.topic = txt;
    this.priority = 10; 
    this.jsonPayload = json;
    this.isRetained = false;
    this.qos=-1;
  }

  NetworkMessageOLD(String txt, JSONObject json, boolean ret, int q) {
    this.topic = txt;
    this.priority = 10; 
    this.jsonPayload = json;
    this.isRetained = ret;
    this.qos= q;
  }
  
  NetworkMessageOLD(String txt, JSONObject json, int t) {
    this.topic = txt;
    this.priority = t;
    this.jsonPayload = json;
    this.isRetained = false;
    this.qos=-1;
  }


  NetworkMessageOLD(String txt, JSONObject json, int t, boolean ret, int q) {
    this.topic = txt;
    this.priority = t; 
    this.jsonPayload = json;
    this.isRetained = ret;
    this.qos= q;
  }
}
