
class RaceDevice extends PhysicalDevice {
  int startHere ;
  int laps;
  int repeat;
  int finishHere;
  int outTunnelNotifyPos;
  String  currentPhaseCode;
    
  private char cmdBuf[];
 
  private boolean configured;
  
  
  private Trace t;
  private static final String _CLASS_ = "RaceDevice";
  
  private static final int DEFAULT_OUTTUNNELNOTIFYPOS = 10;

  public  Deque<String> fromBoardQ;

    RaceDevice(String sname, JSONObject pd, String logStr) {
      super (sname,pd,logStr);
      this.startHere = -1; 
      this.laps = -1;
      this.repeat = -1 ;
      this.finishHere = -1 ;
      this.outTunnelNotifyPos = -1;
      this.currentPhaseCode = null;

      String boardId  = getJSONStringOrExit(pd,"id");

      
      
      if(logStr.equals("-") ){
        t = new Trace(Trace.NONE);
      } else {
        t = new Trace(logStr);
      }      
      t.setPublisherId(boardId);
      t.setClassId(_CLASS_);
      t.log(Trace.METHOD, String.format("Constructor(pd, %s)\n",logStr));

      // passed to readCmd()
      cmdBuf = new char[READBUFFERSIZE];

      // Queue storing the Received commands
      fromBoardQ = new ArrayDeque<String>();

      configured=false;
  }   
    
    void setRaceParams(int p, int l, int r, int f, int o) {
      
      // fare check values -> ret errstr on error 
      
      this.startHere = p; 
      this.laps = l;
      this.repeat = r ;
      this.finishHere = f ;
      this.outTunnelNotifyPos=o;

      configured=true;
      return;
    }   

    void setRaceParams(int p, int l, int r, int f) {
      setRaceParams(p, l, r, f, DEFAULT_OUTTUNNELNOTIFYPOS);
    }


  /**
   * set current Race Phase (= Race Phase of the connected Board)
   */
  void setRacePhase(String p){
    this.currentPhaseCode = p;
  }  

  /**
   * get current Race Phase (= Race Phase of the connected Board)
   */
  String getRacePhase(){
    return(currentPhaseCode);
  }  
  
  
  public void sendConfig() {
    t.log(Trace.METHOD + Trace.DETAILED, String.format("sendConfig(): sending cfg to [%s][%s][%s]\n", this.port, super.name, super.description));
    if(! configured){
      t.error("sendConfig(): not yet configured (missing setRaceParams() call ?)");
      return;
    }
    
// 2023-08-14 / commentato invio di R1...board firmware dovrebbe essere gia in config status...    
//    sendRacePhase(ProtocolSerial.Cmd.RacePhase.Phase.ENTERCFG);
String strcfg = String.format("%c%c",ProtocolSerial.Cmd.EnterCfgMode.ID,  ProtocolSerial.EOC); 
t.log(Trace.METHOD + Trace.DETAILED, String.format("sendConfig(): sending ENTER CFG [%s][%s]:[%s]\n", this.port, super.name, strcfg));
sendString(strcfg);

    
    String str = ProtocolSerial.Cmd.SetConfig.getCommand(this.startHere,this.laps,this.repeat,this.finishHere);
    sendString(str);

    // TODO LUCA: manca mandare OutTunnelDistanceNotify in cmd separato 
    
    return;
    
  }

  

  /**
   * send Race Status command to the board 
   */
  void sendRacePhase(String sCode){
    String str = ProtocolSerial.Cmd.RacePhase.getCommand(sCode);
    if(str != null) {
      t.log(Trace.METHOD + Trace.DETAILED, String.format("sendRacePhase(): sending [%s] to [%s][%s][%s]\n", str, this.port, super.name, super.description));
      sendString(str);
      setRacePhase(sCode);
    }
    return;
  }



  
  /**
   *
   */
  void sendCarEnter(int carN, int speed){
    String str = ProtocolSerial.Cmd.CarEnter.getCommand(carN,speed);
    if(str != null) {
      sendString(str);
    }
    return;
  }

  /**
   *
   */
  void sendCarWin(int carN){
    String str = ProtocolSerial.Cmd.CarWin.getCommand(carN);
    if(str != null) {
      sendString(str);
    }
    return;
  }
  
  
  
  public void loop () {
    
    // Read from Serial
    int bytesRead = readCmd(cmdBuf);
    if(bytesRead != 0){
      char buf[] = new char[bytesRead];
      for(int i=0;i<bytesRead;i++){
        buf[i]=cmdBuf[i];
      } 
      // OFFER to [fromBoardQ] commands received from Board  
      if(ProtocolSerial.Cmd.isValid(buf[0])) {
        boolean ok = fromBoardQ.offer(String.valueOf(buf));
        if(! ok) {
          t.error(String.format("loop() ERROR: Serial [fromBoardQ] QUEUE FULL - offer() returns FALSE\n"));
        }
       if(buf[0]== ProtocolSerial.Cmd.CarLeave.ID) {
         String binStr = String.format("%8s", Integer.toBinaryString(Integer.valueOf((int)buf[1]))).replace(' ', '0');
         t.log(Trace.DETAILED, String.format("loop():Port[%s]->recv:[%c][%8s][EOC] -> added to QUEUE\n", this.port, buf[0], binStr ));
       } else if(buf[0]== ProtocolSerial.Cmd.CarTelemetry.ID) {
         t.log(Trace.LOWLEVEL,  String.format("loop():Port[%s]->recv:[%s] -> added to QUEUE\n", this.port, String.valueOf(buf)) );
       }  else {
         t.log(Trace.METHOD + Trace.DETAILED, String.format("loop():Port[%s]->recv:[%s] -> added to QUEUE\n", this.port, String.valueOf(buf)) );
       }

      } else {
        t.error(String.format("loop() ERROR:Port[%s]->invalid Command Received:[%s]\n", this.port, String.valueOf(buf)));
      }
    }
    
  } // loop()




}
  
