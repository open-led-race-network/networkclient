import java.time.LocalDateTime; 
import java.time.format.DateTimeFormatter;
import java.time.Instant; 
    
static class Utils {  
  
  /*
  *  application-wide Timestamp format
  */
  static String strNow(){
      LocalDateTime now = LocalDateTime.now();
      DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
      return(now.format(formatter));
  }  

  static long tstampNow(){
    return(Instant.now().getEpochSecond());
  }

  /**
   *  Check for Valid Race ID
   *  Please Note:
   *    The UI uses thwe char "_" in fields ids 
   *    as a SEPARATOR (Ex: JOINRACE_HAM001 will
   *    be the name of a generated button for a
   *    client to JOIN race with id=[HAM001].
   *    !!! The "_" char is NOT ALLOWED in the RaceId !!!
   */
  static String checkValidId(String s){
    if(s.trim().length() == 0){
      return("Empty String");
    }
    if(s.contains(" ")){
      return("No \"Space\" char allowed");
    }
    if(s.contains("_")){
      return("No \"_\" char allowed");
    }
    return(checkValidName(s));
  }
  
  /**
   *  Check for Valid Race Names
   */
  static String checkValidName(String s){
    if(s.trim().length() == 0){
      return("Empty String");
    }
    if( ! s.matches("[ a-zA-Z0-9_-]*")) {
         return("Invalid Chars: only letters, numbers, \"-\", \"_\" allowed");
    }
    return(null);
  }


  /**
   *  Returns -1,0,1 if Version_a is Older,Equal,Newer than Version_b)
   *  returns -2 on errors
   */
  static int compareSoftwareVersion(String a, String b){
    if(a.trim().equals(b.trim()))
      return(0);

    int ia[] = new int[3];
    int ib[] = new int[3];
    String[] sa = a.split("\\.");
    String[] sb = b.split("\\.");
    if(sa.length != 3 || sb.length != 3){
             return(-2); 
    }
    ia[0]= Utils.parseInt(sa[0], -1, "Utils.compareSoftwareVersion(sa[0]):");
    ia[1]= Utils.parseInt(sa[1], -1, "Utils.compareSoftwareVersion(sa[1]):");
    ia[2]= Utils.parseInt(sa[2], -1, "Utils.compareSoftwareVersion(sa[2]):");      
    ib[0]= Utils.parseInt(sb[0], -1, "Utils.compareSoftwareVersion(sb[0]):");
    ib[1]= Utils.parseInt(sb[1], -1, "Utils.compareSoftwareVersion(sb[1]):");
    ib[2]= Utils.parseInt(sb[2], -1, "Utils.compareSoftwareVersion(sb[2]):");    
    
    return(compareSoftwareVersion(ia,ib));
  }

  /**
   *  Returns -1,0,1 if Version_a is Older,Equal,Newer than Version_b)
   *  returns -2 on errors
   */
  static int compareSoftwareVersion(int[] ia, int[] ib){
    if(ia.length !=3) {
      System.err.printf("Utils.compareSoftwareVersion(): First array need [3] elements ->%d found\n",ia.length);  
      return(-2);
    }
    if(ib.length !=3) {
      System.err.printf("Utils.compareSoftwareVersion(): Second array need [3] elements ->%d found\n",ib.length);  
      return(-2);
    }      
    for(int i=0; i<3; i++){
      if(ia[i] < 0 || ib[i] < 0){
        System.err.printf("Utils.compareSoftwareVersion(): Element n[%d] of one of the arrays is negative\n",i);  
        return(-2);
      }
    }
    // Compare versions
    for(int i=0; i<3; i++){
      if(ia[i] < ib[i] ){
        return(-1);
      }
      if(ia[i] > ib[i] ){
        return(1);
      }
    }
    return(0);
  }



  /**
   *  Return an Int form String (or onErr value on exception)
   */
  static int parseInt(String s, int onErr, String caller){
    try {  
      int i = Integer.parseInt(s.trim());    
      return(i);
    } catch (NumberFormatException nfe)  {  
      System.err.printf("%s.parseInt() Exception:%s\n",caller,nfe.getMessage());  
      return(onErr);
    }  
  }
  /*
   */
  static int parseInt(String s, int onErr){
    return(parseInt(s,onErr,""));
  }
  
  
  /**
   *  Return a Float form String (or onErr value on exception)
   */
  static float parseFloat(String s, int onErr, String caller){
    try { 
      float fval = Float.parseFloat(s); 
      return(fval);
    } catch (Exception e) { 
      System.err.printf("%s.parseFloat() Exception:%s\n",caller,e.getMessage());
    }   
    return(onErr);
  }  
  static float parseFloat(String s, int onErr){
    return(parseFloat(s,onErr,""));
  }
  
  

  static String dumpHash(HashMap hm){

      StringBuilder str = new StringBuilder();
      Set set = hm.entrySet();
      Iterator i = set.iterator();
      while(i.hasNext()) {
         Map.Entry me = (Map.Entry)i.next();
         str.append(me.getKey() + ": " + me.getValue() + "  ");
      }
      return(str.toString());
  }

}


  JSONObject getJSONObjectOrExit(JSONObject json, String key) {
    JSONObject ret = json.getJSONObject(key);
    if(ret == null){
       System.err.printf("getJSONObjectOrExit():Error! Json cfg file - Missing [%s] section.\n", key);
       noLoop();
       exit();
    }
    return(ret);
  }


  JSONArray getJSONArrayOrExit(JSONObject json, String key) {
    JSONArray ret = json.getJSONArray(key);
    if(ret == null){
       System.err.printf("getJSONArrayOrExit():Error! Json cfg file - Missing [%s] section.\n", key);
       noLoop();
       exit();
    }
    return(ret);
  }


  String getJSONStringOrExit(JSONObject json, String key) {
    String str = json.getString(key, "-");
    if(str.equals("-") ) {
      System.err.printf("getJSONStringOrExit():Error in Json cfg file - Missing [%s] parameter.\n",key);
      noLoop();
      exit();
    }  
    return(str.trim());
  }
  
  int FindStringOrExit(String str, String key) {
    int pos = str.indexOf(key);
    if(pos == -1 ) {
      System.err.printf("FindStringOrExit():\nError in Json cfg file - Missing:[%s] placeholder in Config String:[%s]\n",key, str);
      noLoop();
      exit();
    }  
    return(pos);
  }
  
  
import java.security.SecureRandom;
import java.util.Locale;
import java.util.Objects;
import java.util.Random;


/**
 * Usage examples

  Create an insecure generator for 8-character identifiers:
    RandomString gen = new RandomString(8, ThreadLocalRandom.current());

  Create a secure generator for session identifiers:
    RandomString session = new RandomString();

  Create a generator with easy-to-read codes for printing. The strings are longer than full alphanumeric strings to compensate for using fewer symbols:
    String easy = RandomString.digits + "ACEFGHJKLMNPQRUVWXYabcdefhijkprstuvwx";
    RandomString tickets = new RandomString(23, new SecureRandom(), easy);
 *
 */
public class RandomString {

    /**
     * Generate a random string.
     */
    public String nextString() {
        for (int idx = 0; idx < buf.length; ++idx)
            buf[idx] = symbols[random.nextInt(symbols.length)];
        return new String(buf);
    }

    public static final String upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    public static final String lower = "abcdefghijklmnopqrstuvwxyz";

    public static final String digits = "0123456789";

    public static final String alphanum = upper + lower + digits;

    private final Random random;

    private final char[] symbols;

    private final char[] buf;

    public RandomString(int length, Random random, String symbols) {
        if (length < 1) throw new IllegalArgumentException();
        if (symbols.length() < 2) throw new IllegalArgumentException();
        this.random = Objects.requireNonNull(random);
        this.symbols = symbols.toCharArray();
        this.buf = new char[length];
    }

    /**
     * Create an alphanumeric string generator.
     */
    public RandomString(int length, Random random) {
        this(length, random, alphanum);
    }

    /**
     * Create an alphanumeric strings from a secure generator.
     */
    public RandomString(int length) {
        this(length, new SecureRandom());
    }

    /**
     * Create session identifiers.
     */
    public RandomString() {
        this(21);
    }
  
}  
  
