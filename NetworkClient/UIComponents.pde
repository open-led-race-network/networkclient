/*********************************************************
  
  UI Components  
  =============
  
  This source file contains
  -------------------------
    - Declaration of Global Variables related to UI components
         GDroplist, ScrollableGrid, GWindow, .... objects
         
    - Settings for the UI components
         Position, Size, Color, etc
         
    - createUI() 
         Called in setup() to instantiate UI objects

*********************************************************/

import java.awt.Rectangle;
import java.awt.Font;


// Main Sketch windos size
void settings() {
  size (800, 700, JAVA2D);
}


///////////////////////////////////
// Global Vars related to the UI //
///////////////////////////////////

// Local Client Selection Dropdown List
// (The software manage MORE than one OLR Phisical Device locally connected)
GDropList lcDroplist;
GLabel    lcDroplist_label;

GButton    g_createRace_button; 

GGroup     grpCreateRace;
boolean    g_grpCreateRaceIsVisible;
GTextField g_newRace_R_Name; 
GLabel     g_newRace_R_Name_label; 
GButton    g_newRace_submit; 

// Display Areas for Race and Device Lists
//   It uses ScrollableGrid{} CLASS (in ScrollableGrid.pde TAB)
ScrollableGrid  g_rArea   = null;  // Race List Display Object
Rectangle       g_rRect   = null;  
long            g_rArea_lastUpdate = 0;  // check for changes in Race list

ScrollableGrid  g_rtArea  = null;  // Device List Display Object
Rectangle       g_rtRect  = null;
long            g_rtArea_lastUpdate = 0;  // check for changes in Device list

String footerText = "[R]Reset  [F]Footer  [L]Line Num  [H]Header  [Arrows/Mouse Wheel] scroll";

/*********************************************************
  Function called in the setup() CREATE UI objects
*********************************************************/
public void createUI() {
  
  g_t.log(Trace.METHOD, String.format("createUI()\n"));  
 
  G4P.messagesEnabled(false);
  G4P.setGlobalColorScheme(GCScheme.CYAN_SCHEME);
  G4P.setMouseOverEnabled(false);  
  
  // Local Client Droplist
  ////////////////////////
  lcDroplist_label = new GLabel(this, 30, 8, 170, 20);
  lcDroplist_label.setTextAlign(GAlign.RIGHT, GAlign.MIDDLE);
  lcDroplist_label.setText("My Network Device:");
  lcDroplist_label.setFont(new Font("Monospaced", Font.PLAIN, 14));
  lcDroplist_label.setTextBold();
  lcDroplist_label.setOpaque(false);
  
  lcDroplist = new GDropList(this, 210, 10, 540, 100, 5, 30);
  lcDroplist.setItems(g_localNetworkDevices.getList(), 0); 
  lcDroplist.setLocalColorScheme(GCScheme.ORANGE_SCHEME);
  lcDroplist.setFont(new Font("Monospaced", Font.PLAIN, 14));



  // Create New Race Input Fields and Button
  //////////////////////////////////////////
  
  g_createRace_button = new GButton(this, 10, 40, 80, 50);
  g_createRace_button.setText("Create New Race");
  g_createRace_button.setLocalColorScheme(GCScheme.ORANGE_SCHEME);
  g_createRace_button.addEventHandler(this, "g_createRace_button_click");

  grpCreateRace = new GGroup(this);


  g_newRace_R_Name_label = new GLabel(this, 100, 40, 80, 20);
  g_newRace_R_Name_label.setTextAlign(GAlign.LEFT, GAlign.BOTTOM);
  g_newRace_R_Name_label.setText("Name: ");
  g_newRace_R_Name_label.setLocalColorScheme(GCScheme.CYAN_SCHEME);
  g_newRace_R_Name_label.setOpaque(false);

  g_newRace_R_Name = new GTextField(this, 100, 62, 300, 30, G4P.SCROLLBARS_NONE);
  g_newRace_R_Name.setLocalColorScheme(GCScheme.CYAN_SCHEME);
  g_newRace_R_Name.setOpaque(true);
  
  // SUBMIT Button
  g_newRace_submit = new GButton(this, 420, 62, 80, 30);
  g_newRace_submit.setText("Submit");
  g_newRace_submit.setLocalColorScheme(GCScheme.CYAN_SCHEME);
  g_newRace_submit.addEventHandler(this, "g_newRace_submit_click");

  g_createRace_button.setVisible(false);
  grpCreateRace.addControls(g_newRace_R_Name_label,g_newRace_R_Name,g_newRace_submit);
  grpCreateRace.setVisible(false);
  g_grpCreateRaceIsVisible = false;
  

  // Display Areas for Race and Device Lists
  /////////////////////////////////////////////
  g_rRect   = new Rectangle(10, 100, 780, 280); // Race List Display Area
  g_rtRect  = new Rectangle(10, 400, 780, 280); // Device List Display Area
  g_rArea   = new ScrollableGrid(this, g_rRect.x, g_rRect.y, g_rRect.width, g_rRect.height);
  g_rtArea  = new ScrollableGrid(this, g_rtRect.x, g_rtRect.y, g_rtRect.width, g_rtRect.height);
  
  g_rArea.setBackground(#f7f7f7);
  g_rArea.setLineNumColor(#81bd4d);
  g_rArea.setCellColors(#f7f7f7, #f5f5f5, #222222); 
  g_rArea.setFooterColors(#f4f4f4, #f1f1f1, #444444);
  g_rArea.setTitleColors(#fdfdd2, #d2e7e7, #080b74);
  g_rArea.setHeaderColors(#e5e9ed, #d5d9dd, #111111); 

  g_rtArea.setBackground(#f7f7f7);
  g_rtArea.setLineNumColor(#81bd4d);
  g_rtArea.setCellColors(#f7f7f7, #f5f5f5, #222222); 
  g_rtArea.setFooterColors(#f4f4f4, #f1f1f1, #444444); 
  g_rtArea.setTitleColors(#d2ded6, #d2e7e7, #005310); 
  g_rtArea.setHeaderColors(#e5e9ed, #d5d9dd, #111111); 

}
