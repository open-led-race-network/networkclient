/**
   ____                     _      ______ _____    _____                
  / __ \                   | |    |  ____|  __ \  |  __ \               
 | |  | |_ __   ___ _ __   | |    | |__  | |  | | | |__) |__ _  ___ ___ 
 | |  | | '_ \ / _ \ '_ \  | |    |  __| | |  | | |  _  // _` |/ __/ _ \
 | |__| | |_) |  __/ | | | | |____| |____| |__| | | | \ \ (_| | (_|  __/
  \____/| .__/ \___|_| |_| |______|______|_____/  |_|  \_\__,_|\___\___|
        | |   _  _     _                  _             _ _ _   _          
        |_|  | \| |___| |___ __ _____ _ _| |__   ___ __| (_| |_(_)___ _ _      
             | .` / -_|  _\ V  V / _ | '_| / /  / -_/ _` | |  _| / _ | ' \ 
             |_|\_\___|\__|\_/\_/\___|_| |_\_\  \___\__,_|_|\__|_\___|_||_|
                                                                
                                                                                                       
 Networked Test Client for Open LED Race Network
 
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.


   --see changelog.txt
   
 */
// Current Software Version
final String client_version = "0.9.8";

/*
 * Version of the Arduino Software required
 *    int req_firmware_version[] = {Major,Minor,Patch}
 * The software version returned by the board (x.y.z)
 * is checked and considered valid if:
 *   x==Major AND y >= Minor   
 * 
 */
final int req_firmware_version[] = {0,9,9};   

 
import java.time.LocalDateTime; 
import java.time.format.DateTimeFormatter;
import java.lang.System.*;
import java.io.IOException;
import java.io.File;
import java.io.OutputStream ;
import java.io.FileOutputStream; 
import java.io.PrintStream;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ArrayBlockingQueue;

import com.fazecast.jSerialComm.*;  
import g4p_controls.*;    

////////////////////////////////////////////////////////////////////
// [frameRate]:                                                   //
//   setup()--->frameRate(frameRate);                             //
// -------------------------------------------------------------  //
// The default value (120) is usually ok for 2 NetworkDevices     //
// configured in the JSON cfg file.                               //
// To manage more Local Devices a value > 120 may be needed.      //
// Usually the first clue of an insufficient framerate            //
// is when we see a "delay" in Serial Communications              //
// received FROM Board.                                           //
// Ex:                                                            //
//  Telemetry data from Board appears in the Console LOG output   //
//  delayed (we receive car positions not corresponding to what   //
//  we see in the OLR - a car is at Position b>a when we receive  //
//  telemetry message for position a)                             //
////////////////////////////////////////////////////////////////////
final int frameRate = 120;

/////////////////////////////////////////
// JSON Config File Name               //
// The program looks for this file in: //
//    sketchPath("data/")              //
// folder                              //      
///////////////////////////////////////// 
final String jsonFileName = "config.json"; 
boolean configFileExist = false;


///////////////////////////////////////////////
// GLOBAL Vars (name start with "g_" prefix) //
//           -------------                   //
// UI-related Global vars are declared       // 
// in [UIComponents] source file             //
///////////////////////////////////////////////       
LocalNetworkDevices g_localNetworkDevices;                  // LocalNetworkDevices OBJ
int                 g_curLocalNetDevice = 0;                // Index of the LocalNetworkDevice currently displayed in the UI 
int                 g_prevLocalNetDevice = 0;               // Used to update OLR Visualization when g_curLocalNetDevice changes
boolean             g_localNetDeviceIsConfiguring = false;  
Trace               g_t;                                    // Trace/Log Obj


String _CLASS_ = "UI";   // Class Name used in Trace/Log for global "g_t" object


public void setup(){
  
  String fname = sketchPath("data/") + jsonFileName;
  File f = dataFile(fname);
  String filePath = f.getPath();
  boolean exist = f.isFile();
  if(exist) {
    frameRate(frameRate);
    configFileExist = true;
    loadConfig(); // creates Global "Trace t" object used here and in UIComponents,UIHandlers
    createUI();  
  } 
}



public void draw(){
  background(#e5e9ed);
  if(configFileExist) {
    g_localNetworkDevices.loop();
    updateUI();
  } else {
    showMissingConfigFile();
  }
}




/**
 * 
 */
public void updateUI(){

  // Set visible=[y|n] for the [Create New Race] button  
  checkNewRaceButton();
  
  // Check if the lists need to be reloaded in the UI (new data received via OLRNetwork)
  checkRaceList();
  checkDeviceList();
  
  // Display lists
  g_rArea.showGrid();
  g_rtArea.showGrid();
}

/**
 * Set Visible=[y] the "Create new Race" button
 * when the LocalClient is in the right Status
 * and the user is not already creating a new race
 */
void checkNewRaceButton (){

  String thisClientStatus = g_localNetworkDevices.getStatus(g_curLocalNetDevice);
  if(thisClientStatus == null || thisClientStatus.equals("-") ) {
    /* This may happens in the Startup phase, when the NetworkDevice is 
       connecting to the network and still have not receive back 
       his own Status=xxx message
    */
    return;
  }
  if ( ! g_grpCreateRaceIsVisible) {
    if( thisClientStatus.equals(Protocol.PDevice.Status.AVAILABLE) ){
      g_createRace_button.setVisible(true);
    } else {
      g_createRace_button.setVisible(false);
    }
  }
  return;
}

/**
 * Check if the Internal Timestamp of the DeviceList 
 * Object has changed from the last time.
 * If this is the case, GET the new content and FILL the 
 * Display object with the new data
 */
void checkDeviceList() {
  long listTstamp = g_localNetworkDevices.getDeviceListTstamp(g_curLocalNetDevice);
  if(g_rtArea_lastUpdate != listTstamp) {
    g_t.log(Trace.DETAILED, String.format("checkDeviceList() - Device List UPDATED...fillGrid():[%d][%d]\n", g_rtArea_lastUpdate, listTstamp));
    String newList[] = uiGetDeviceList();
    g_rtArea.fillGrid(newList);
    g_rtArea.setTitle(" OpenLedRace Devices", 14);
    g_rtArea.setFirstRowAsHeader();
    g_rtArea_lastUpdate = listTstamp;
  }
  return;   
}      

/**
 *  Get the String [][] from Device List and
 *  prepare the String[] in the format required 
 *  by ScrollableGrid
 *
 * @return String[] in the correct format for ScrollableGrid   
 */
String [] uiGetDeviceList() {

  String thisClientId=g_localNetworkDevices.getId(g_curLocalNetDevice);

  // getDeviceListContent() returns: String[n][5] [n]:Device / [5]:{id,sname,name,status,tstamp}
  String[][] data = g_localNetworkDevices.getDeviceListContent(g_curLocalNetDevice);
  String [] s = new String[data.length + 1];
  s[0] = new String("User; Device; Status ");
  for(int i = 0 ; i < data.length ; i++) {
    String id = data[i][0];
    String sname = data[i][1];
    String name = data[i][2];
    String status = data[i][3];
    // Copy field and decode Status
    if(thisClientId.equals(id)){
      // IT'S ME: Display color = Hilight (<L> tag)
      s[i+1] = String.format("<L>%s ;<L> %s;<L>%s",sname, name,Protocol.PDevice.status.decode(status));
    } else {
      s[i+1] = String.format("%s ; %s;%s",sname, name,Protocol.PDevice.status.decode(status));
    }
  }
  return(s);
}

/**
 * Prepare the "g_rArea" content to be displayed
 * - If the LocalClient is in the "Configuring a Race"
 *   the RaceList visualization Area will be used to
 *   display the Configuration TextFields
 * - In any other case the area will be filled with
 *   Races List data.
 */
  void checkRaceList() {
    String localDeviceStatus = g_localNetworkDevices.getStatus(g_curLocalNetDevice); // Versione che usa Attribute di NetworkDevice
    long listTstamp = g_localNetworkDevices.getRaceListTstamp(g_curLocalNetDevice);    
    
    if(localDeviceStatus == null || localDeviceStatus.equals("-") ){
      return; // see note in checkNewRaceButton()
    }
    //Client Status="Configuring a Race" ---> Display the Configure Interface 
    if(localDeviceStatus.equals(Protocol.PDevice.Status.CONFIGURING)) {
      if( ( ! g_localNetDeviceIsConfiguring) || (g_prevLocalNetDevice != g_curLocalNetDevice)) {
        g_t.log(Trace.BASE, String.format("checkRaceList() - NetDevice Status=Configuring Race ---> Display the Configure Interface:[%d][%d]\n", g_rArea_lastUpdate, listTstamp));

        g_localNetDeviceIsConfiguring = true; // will be reset when the user press DONE button
        String raceData[] = g_localNetworkDevices.getRace(g_curLocalNetDevice);
        String newList[] = uiGetConfigList(raceData[0].trim());
        g_rArea.fillGrid(newList);
        g_rArea.setTitle(String.format("      Configure Race [%s]", raceData[1]) , 14);
        g_rArea.setFirstRowAsHeader();
        g_prevLocalNetDevice = g_curLocalNetDevice;
      }      
    } else {
      // Display the usual Race List interface 
      if(g_rArea_lastUpdate != listTstamp) {
        g_t.log(Trace.DETAILED, String.format("checkRaceList() - Race List UPDATED...fillGrid():[%d][%d]\n", g_rArea_lastUpdate, listTstamp));
        String newList[] = uiGetRaceList();      
        g_rArea.fillGrid(newList);
        g_rArea.setTitle(" Active Races", 14);
        g_rArea_lastUpdate = listTstamp;
      }
    }
     return;   
}    

/**
 *  Get the String [][] Arrays from [Device List] and [Partecipants]
 *  and prepare the String[] in the format required by ScrollableGrid
 *  If needed, ADD buttons for [Join], [Delete], [Confirm], etc 
 *
 * @return String[] in the correct format for ScrollableGrid   
 */
String [] uiGetRaceList() {
  
//println("uiGetRaceList()....RaceList->toString()");  
//println(g_localNetworkDevices.raceListToString(g_curLocalNetDevice));  
  
  String thisClientId=g_localNetworkDevices.getId(g_curLocalNetDevice);
  String thisClientStatus = g_localNetworkDevices.getStatus(g_curLocalNetDevice); 
  StringBuilder sb = new StringBuilder();
  Formatter fmt = new Formatter(sb);
  ArrayList<String> content= new ArrayList<String>();

  content.add("Race Name; Status");

  // getRaceListContent() returns: String[n][5] [n]:Race / [5]:{id,name,status,tstamp,partecipants_n}
  String[][] rl = g_localNetworkDevices.getRaceListContent(g_curLocalNetDevice);
  for(int i = 0 ; i < rl.length ; i++){
    String raceId = rl[i][0].trim();
    String raceName = rl[i][1].trim();
    String raceStatus = rl[i][2].trim();
    String raceStatusD = Protocol.PRace.status.decode(raceStatus);
    int pN = 0;
    try { pN = Integer.parseInt(rl[i][4].trim());}catch (NumberFormatException e){;}

    // ADD Race Data content
    fmt.format(String.format("%s;%s", raceName, raceStatusD));
    
    // ADD JOIN Race Button ? 
    if(raceStatus.equals(Protocol.PRace.Status.ACCEPTING) && 
        ( thisClientStatus.equals(Protocol.PDevice.Status.AVAILABLE) )            
       ){                              
      fmt.format("; %s", uiAddButton("Join","JOINRACE_"+raceId, 80, 20));            
    }      
    // ADD DELETE Race Button ?
    if( pN == 0) {
      fmt.format("; %s", uiAddButton("Delete","DELETE_"+raceId, 80, 20)); 
    }
    
    // ADD Configure Race Button ?
    String[] myRace = g_localNetworkDevices.getRace(g_curLocalNetDevice);
    if( myRace != null && raceId.equals(myRace[0]) && 
        g_localNetworkDevices.isRaceReadyToConfigure(g_curLocalNetDevice, raceId) 
      ){
        fmt.format("; %s", uiAddButton("Configure","CONFRACE_"+raceId, 80, 30));
    }
    
    // Add "Race" line with buttons line to ArrayList 
    content.add(sb.toString()); 
    sb.setLength(0); //empty StringBuilder
   
    // getRacePartecipants() returns: String[n][2] - [n]:Partecipant / [2]:{id,status}
    String pt[] = g_localNetworkDevices.getRacePartecipantsId(g_curLocalNetDevice, raceId);
    for(int j = 0 ; j < pt.length ; j++){
      String devId = pt[j].trim();
      // ADD Partecipants Data content / Empty first field ; Id = Name in the same output field
      String devName = g_localNetworkDevices.getDevName(g_curLocalNetDevice,devId);
      if(thisClientId.equals(devId)){
        if(raceStatus.equals(Protocol.PRace.Status.CONFIGURING)) { 
          fmt.format(" ;<L> %s - %s; PLEASE WAIT...", devId, devName);
        } else {
          fmt.format(" ;<L> %s - %s", devId, devName);
        }   
      } else {
        fmt.format(" ; %s - %s", devId, devName);
      }
      //  ADD CONFIRM and LEAVE Buttons ?
      if(thisClientId.equals(devId) &&
         thisClientStatus.equals(Protocol.PDevice.Status.SUBSCRIBING) 
        ){
          fmt.format("; %s", uiAddButton("Confirm","CONFIRMRACE_"+raceId, 80, 20));
          fmt.format("; %s", uiAddButton("Leave","LEAVERACE_"+raceId, 80, 20));
      }
      //  ADD Play Again and LEAVE Buttons ?
      if(thisClientId.equals(devId) &&
         thisClientStatus.equals(Protocol.PDevice.Status.RACEDONE) 
        ){
          fmt.format("; %s", uiAddButton("Play Again","PLAYAGAINRACE_"+raceId, 80, 20));
          fmt.format("; %s", uiAddButton("Leave","LEAVERACE_"+raceId, 80, 20));
      }
      
      // Add line to ArrayList
      content.add(sb.toString());
      sb.setLength(0); //empty StringBuilder
    } // for partecipant
  } // for race
  return(content.toArray(new String[0]));
}



/**
 *  Get the String [][] of Partecipants from RaceList for Race.id=raceId
 *  and modify it to ADD TEXTFIELDS and BUTTON for the Race Configuration
 *
 * @return String[] in the correct format for ScrollableGrid   
 */
String [] uiGetConfigList(String raceId) {
  String thisClientId=g_localNetworkDevices.getId(g_curLocalNetDevice);
  // getRacePartecipants() returns: String[n][2] - [n]:Partecipant / [2]:{id,status}
  String pt[] = g_localNetworkDevices.getRacePartecipantsId(g_curLocalNetDevice, raceId);
  String [] s = new String[(pt.length * 2) + 3];
  s[0] = " ; Position ; Laps ; Repeat ";
  int j=0;  
  
  for(int i = 0 ; i < pt.length ; i++) {
    String PartecipantId = pt[i].trim();
    j=((i+1)*2)-1;

    String devName = g_localNetworkDevices.getDevName(g_curLocalNetDevice,PartecipantId);
    String devSName = g_localNetworkDevices.getShortName(g_curLocalNetDevice,PartecipantId);
    if(thisClientId.equals(PartecipantId)){
      s[j] = String.format("<L>%s;<L>%s", devSName, devName);
    } else {
      s[j] = String.format("%s;%s", devSName, devName);
    }
    // Position
    s[j+1] = String.format(" ; TEXTFIELD, Position, %d , RCFG_POS_%s, %d, %d, %d", i, PartecipantId, 0, 130 , 20) ;
    // Laps n
    s[j+1] = s[j+1].concat(String.format(" ; TEXTFIELD, Laps , %d , RCFG_LOOP_%s, %d, %d, %d", 5, PartecipantId, 0, 130 , 20)) ;    
    // Repeat n
    s[j+1] = s[j+1].concat(String.format(" ; TEXTFIELD, Repeat , %d , RCFG_REPEAT_%s, %d, %d, %d", 2, PartecipantId, 0, 130 , 20)) ;
  }
  s[j+2] = String.format("; ; ");
  s[j+3] = String.format("; ; %s, %s, %s, %d,%d,%d", 
                                "BUTTON",          // The keyword telling ScrollableGrid Class that this cell is a Button
                                "Done",            // Button Text
                                "RCFGDONE_" +  raceId , // ButtonTag - Identify Button
                                                         // used in UIHandlers--->handleButtonEvents()
                                0 ,               // Button Numeric Id - not used (uses ButtonTag)
                                80,               // Button Width
                                20                // Button Height
                                );      
  return(s);
}


String uiAddButton(String text, String tag, int w, int h) {            

  return(String.format("%s, %s, %s, %d,%d,%d", 
                  "BUTTON", // The keyword telling ScrollableGrid Class that this cell is a Button
                  text,     // Button Text
                  tag ,     // ButtonTag - Identify Button
                            // used in UIHandlers--->handleButtonEvents()
                  0 ,       // Button Numeric Id - not used (uses ButtonTag)
                  w,        // Button Width
                  h         // Button Height
                  ));            
}




void loadConfig() {

  String fname = sketchPath("data/") + jsonFileName;
   
  String redirConsole = null ;
  
  JSONObject json=null;
  try {
    json = loadJSONObject(fname);
  } catch (RuntimeException e) {  
    System.err.printf("Error loading cfg file:[%s] - [%s]\n ", fname, e);
    noLoop();
    exit();
  }
  
  if(json != null ) {
    
    // Base parameters
    //////////////////
    JSONObject base = json.getJSONObject("Base");
    if(base != null ) {  
  
      String logStr   = trim(base.getString("LogLevel" , "-"));    
      if(logStr.equals("-") ){
        g_t = new Trace(Trace.NONE);
      } else {
        g_t = new Trace(logStr);
      }
      g_t.setClassId(_CLASS_);
      g_t.log(Trace.METHOD, String.format("loadConfig()\n"));

      g_t.log(Trace.LOWLEVEL, String.format("loadConfig() - Loaded JSON Object:\n%s\n",json.toString()));

      // Console output redirection
      String r = trim(base.getString("redirConsole"));
      if(r != null) {
        redirConsole = new String(trim(r));
        if( ! redirConsole.equals("-") ) {
          try { 
            String file = sketchPath("")+ redirConsole;
            FileOutputStream outStr = new FileOutputStream(file, false);
            PrintStream printStream = new PrintStream(outStr);
            System.setOut(printStream);
            System.setErr(printStream);
          } catch (IOException e) {
            System.err.println("Unrecoverable Error - Check path, or filename, or security manager! "+e);
            exit();
          }
        }
      }

      // Code name
      String cn = trim(base.getString("codeName"));
      if(cn != null ) surface.setTitle(cn + " - " + client_version);
 
 
      // App Version 
      // When the JSON CFG file is "generated" online (and downloaded by user)
      // the Value for the "app_version" Attribute is SET to the "client_version"
      // if was made for
      // Here we check if the Config File was generated for this Client Version
      String appVer = trim(base.getString("app_version"));
      if(appVer != null ){
        if( ! appVer.equals(client_version)){
          System.err.printf("Unrecoverable Error - JSON Config file was generated for a diferent Client version:[%s] - This Clien Version:[%s]\n ", appVer, client_version);
          noLoop();
          exit();
        }
      } 
 
      if ( json.isNull("LocalNetworkDevices")) {
         System.err.printf("Unrecoverable Error - Json cfg file:[%s] - Missing \"LocalNetworkDevices\" section.", fname);
         noLoop();
         exit();
      }      
      // Instantiate [LocalNetworkDevices] object passing the specific JSON cfg [LocalClients] section to the constuctor 
      JSONObject jLocalClients = json.getJSONObject("LocalNetworkDevices" );
      g_localNetworkDevices = new LocalNetworkDevices(this, jLocalClients) ;
  
      // Set Publisher ID in LOG/TRACE object
      // String netDeviceSName=g_localNetworkDevices.getSName(g_curLocalNetDevice);
      // g_t.setPublisherId(netDeviceSName);
      String netDeviceId=g_localNetworkDevices.getId(g_curLocalNetDevice);
      g_t.setPublisherId(netDeviceId);


    } else {
      System.err.printf("Unrecoverable Error - Json cfg file:[%s] - Missing \"base\" section.", fname);
      exit();
    }
 
  } else {
     System.err.printf("Unrecoverable Error - JSON Object not loaded - Check filename:[%s]",fname);
  }

}

/**
 *  Display Missing Config Message
 */
 
boolean overButton = false;

void showMissingConfigFile() {

  fill(51);
  textSize(24);
  textAlign(LEFT);
  text("JSON Config file not found", 20, 40);
/***  
  textSize(18);
  textAlign(LEFT);
  text("Please follow the link ABOVE to get Config File for your device.", 20, 170);
  
  if (overButton == true) {
    fill(255);
  } else {
    noFill();
  }
  rect(105, 60, 75, 75);
  line(135, 105, 155, 85);
  line(140, 85, 155, 85);
  line(155, 85, 155, 100);
***/  
}

/***
void mousePressed() {
  if (overButton) { 
    link("http://www.openrelayrace.org");
  }
}

void mouseMoved() { 
  checkButtons(); 
}
  
void mouseDragged() {
  checkButtons(); 
}

void checkButtons() {
  if (mouseX > 105 && mouseX < 180 && mouseY > 60 && mouseY <135) {
    overButton = true;   
  } else {
    overButton = false;
  }
}
**/

/**
 *  Override Processing exit()
 *  Publish Status=Offline for Local Devices
 *  before super.exit();
 */
  void exit(){
  if(g_localNetworkDevices != null) {
    g_localNetworkDevices.exit();
  }
  super.exit();
}
