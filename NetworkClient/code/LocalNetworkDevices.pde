/**  
  Class COMPOSITION / AGGREGATION
  -------------------------------
  
  At Startup the software reads a Json Config file and creates:  
    - one [LocalClients] object containing:
      - an array of [NetworkDevice] objects containing:
        - a List of network connected devices
        - a List if active races
        - one PhysicalDevice object 
        ...etc
     
  The COMPOSITION structure is represented below       
  
  LocalNetworkDevices
    |
    |-> NetworkDevice 1
    |        |
    |        |-> OLRNClient extends MQTTClient 
    |        |
    |        |-> Device List
    |        |    |-> Device 1 [Id][Name][Status]
    |        |    |-> ...........    
    |        |    |-> Device n [Id][Name][Status]
    |        |    
    |        |-> Race List
    |        |    |-> Race 1 [Id][Name][Status]
    |        |    |   |-> Partecipant 1 [Id][Status]
    |        |    |   |-> .......
    |        |    |   |-> Partecipant n [Id][Status]
    |        |    |   
    |        |    |-> Race_n [Id][Name][Status]
    |        |        |-> Partecipant 1 [Id][Status]
    |        |        |-> .......
    |        |        |-> Partecipant n [Id][Status]
    |        |   
    |        |-> RaceDevice extends PhysicalDevice
    |             |
    |             |-> Serial (talk to OLR Board (Ardino)
    |             |    
    |             |-> Cars
    |
    |
    |-> NetworkDevice 2
             |-> ....

 */
 
import java.util.Arrays;
import java.util.*;
import  java.lang.Object;


/*
 * LocalNetworkDevices class 
 */
class LocalNetworkDevices {

  // List of NetworkDevice objects (ONE Client <---> ONE Phisical OLR) 
  private NetworkDevice[] cList;
  private int curLookupNetDevice;
  
  private Trace t;
  private static final String _CLASS_ = "LocalNetworkDevices";
    
  /*
  *  Constructor
  */
  LocalNetworkDevices (PApplet p, JSONObject json)   {
        
    String logStr   = trim(json.getString("LogLevel" , "-"));    
    if(logStr.equals("-") ){
      t = new Trace(Trace.NONE);
    } else {
      t = new Trace(logStr);
    }
    t.setClassId(_CLASS_);
    t.log(Trace.METHOD, String.format("Constructor()\n"));
    
    procConstructorJParam(p, json);
    
    curLookupNetDevice=-1;
  }

  
  /*
  *  This function have to be called in main draw() function.
  *  For each PhisicalDevive.NetworkDevice :
  *    - Check connection (-> Connect)
  *    - Check MQTT Setup done ( -> OLRSetupOLRN: Publish "i'm here" + Subscribe to base topics) 
  */
  public void loop () {
  
    int         cNum = cList.length;
    for (int i = 0; i < cNum; i++) {
      
      // Manage Connect on MQTT + OLR network
      if(cList[i].isConnected()){                    
        if( ! cList[i].isOnline()) {  
          cList[i].setupOLRN();                  // MQTT Broker connected -> Setup OLRNetwork
        }
      } else {
        if( ! cList[i].isConnecting() ) {
          cList[i].connect();                    // Connect to MQTT Broker
        }
      }
      
      // Network setup Ok -> NetworkDevice.loop()
      if( cList[i].isConnected() && cList[i].isOnline() ) {
        cList[i].loop();                          
      }
      
      // Manage Board Lookup 
      if( ! cList[i].phRaceDev.isPortValid() ) {
        int curDevIdx = getCurLookupNetDevice(); 
        if(curDevIdx != -1 && curDevIdx == i){   // Coordinates port lookup between NetworkDevices
          cList[i].phRaceDev.findBoard();
        }
      }
    }
  }
  
  /**
   *  Coordinates Phisical Device Port lookup 
   */
  int  getCurLookupNetDevice() {
    
    if(cList.length <= 0) {                     // Empty list - No Local Devices configured in JSON cfg
      return(-1);
    }
    if(curLookupNetDevice == -1) {              // First call
      curLookupNetDevice = 0 ;                  // starts with first NetworkDevice in the list
      cList[curLookupNetDevice].phRaceDev.isSearching=true;
      return(curLookupNetDevice);
    }
    if(cList[curLookupNetDevice].phRaceDev.isSearching){
      return(curLookupNetDevice);               // Current NetworkDevice did not complete a scan of any available port
    }
    
    curLookupNetDevice ++;                      // Get Next in cList
    
    if(curLookupNetDevice >= cList.length){
      curLookupNetDevice = 0 ;                  // End Of List: restart from first one 
    }
    // Look for the next one not yet associated in the 'last' part of the list
    for (int i = curLookupNetDevice; i < cList.length; i++) {
      if( ! cList[curLookupNetDevice].phRaceDev.isPortValid) {  // skip NetworkDevices already associated with a port
        cList[curLookupNetDevice].phRaceDev.isSearching=true;
        return(curLookupNetDevice);
      } 
    }
    // Look for the next one not yet associated in the 'first' part of the list
    for (int i = 0; i < curLookupNetDevice; i++) {
      if( ! cList[curLookupNetDevice].phRaceDev.isPortValid) {  // skip NetworkDevices already associated with a port
        cList[curLookupNetDevice].phRaceDev.isSearching=true;
        return(curLookupNetDevice);
      } 
    }
    return(-1);  // no more NetworkDevice needing to find the Serial Port 
  }  
  
  
  /**
   * Called from the main sketch on user exit
   */ 
  public void exit () {

    int         cNum = cList.length;
    for (int i = 0; i < cNum; i++) {
       cList[i].exitOLRN();
    }
  }
  
  
  private void  procConstructorJParam(PApplet p, JSONObject json) {    

    JSONArray cData = json.getJSONArray("NetworkDevice");
    if(cData == null){
       t.error(String.format("Error! Json cfg file - Missing \"LocalClients\" section."));
       exit();
    }

    int cNum = cData.size();
    cList = new NetworkDevice[cNum];  
  
   for (int i = 0; i < cNum; i++) {
      JSONObject jc = cData.getJSONObject(i); // NetworkDevice[i]
      String sname      = jc.getString("sName");
      String name       = jc.getString("name");
      String id         = jc.getString("id");
      t.log(Trace.DETAILED, String.format("procConstructorJParam():Instantiate NetworkDevice:[%s][%s][%s]....\n", id, sname, name));   
      cList[i] = new NetworkDevice (p, id, sname, name, jc); 
    }
    
  } // procConstructorJParam()



  /**
   * Select the Cliend at idx pos (cList[idx])
   * and SENDs [Create Race] to network
   *
   * @return null on Success
   * @return a string with ERROR MESSAGE on Error  
   */   
  String sendCreateRace(int idx, String raceName) {
    return(cList[idx].sendCreateRace(raceName));
  }
  
  /**
   * Select the Cliend at idx pos (cList[idx])
   * and SENDs [Subscribe to Race] to network
   *
   * @return null on Success
   * @return a string with ERROR MESSAGE on Error  
   */    
  String sendSubscribeToRace(int idx, String raceId) {
    return(cList[idx].sendSubscribeToRace(raceId));
  }
  
  /**
   * Select the Cliend at idx pos (cList[idx])
   * and SENDs [Leave Race] to network
   *
   * @return null on Success
   * @return a string with ERROR MESSAGE on Error  
   */   
  String sendLeaveRace(int idx, String raceId) {
    return(cList[idx].sendLeaveRace(raceId));
  }
  
  /**
   * Select the Cliend at idx pos (cList[idx])
   * and SENDs [Delete Race] to network
   *
   * @return null on Success
   * @return a string with ERROR MESSAGE on Error  
   */     
  String sendDeleteRace(int idx, String raceId) {
    return(cList[idx].sendDeleteRace(raceId));
  }

  /**
   * Select the Cliend at idx pos (cList[idx])
   * and SENDs [Confirm Race Subscription] to network
   *
   * @return null on Success
   * @return a string with ERROR MESSAGE on Error  
   */
  String sendConfirmSubscriptionToRace(int idx, String raceId) {
    return(cList[idx].sendConfirmSubscriptionToRace(raceId));
  }

  /**
   * Select the Cliend at idx pos (cList[idx])
   * and SENDs [Configure Race] to network
   *
   * @return null on Success
   * @return a string with ERROR MESSAGE on Error  
   */
  String sendConfigureRace(int idx, String raceId) {
    return(cList[idx].sendConfigureRace(raceId));
  }
  

  String sendRaceConfigParam(int idx, Protocol.Network.PDU pdu) {
    return(cList[idx].sendRaceConfigParam(pdu));
  }
  
  /**
   * Select the Cliend at idx pos (cList[idx])
   * and SENDs [play Again Race] to network
   *
   * @return null on Success
   * @return a string with ERROR MESSAGE on Error  
   */
  String sendPlayAgainRace(int idx, String raceId) {
    return(cList[idx].sendPlayAgainRace(raceId));
  }


 /**
   * @return int: number of local clients (=locally connected devices) 
   */  
  int  getNetworkDevicesNum() {
    return(cList.length);
  }

  /**
   * @return String[]: List of local NetworkDevices description (=locally connected devices) 
   */  
  String[] getList() {    
    String[] item; 
    item = new String[cList.length];
    for(int i = 0; i<cList.length; i++){
      item[i] = new String(cList[i].getId().concat(" - ").concat(cList[i].getName())  );
    }
    return(item);
  }  


  /**
   * Select the Cliend at idx pos (cList[idx])
   * and return it's [_status]
   */
  String getStatus (int idx){
    if(idx >= cList.length )   return null;
    return(cList[idx]._status);  
  }
    
  /**
   * Select the Cliend at idx pos (cList[idx])
   * and return it's [sName]
   */
  String getSName(int idx){
    if(idx >= cList.length )   return null;
    return(cList[idx].getSName());
  }

  /**
   * Select the Cliend at idx pos (cList[idx])
   * and return it's [id]
   */
  String getId(int idx){
    if(idx >= cList.length )   return null;
    return(cList[idx].getId());
  }

  /**
   * Select the Cliend at idx pos (cList[idx])
   * and return the race where it appears as
   * a partecipant 
   *
   * @return String[]: {id, name, status } 
   * @return null:      on partecipant not found in any Race   
   */  
  public String[] getRace(int idx)  {
    if(idx >= cList.length )   return null;    
    return(cList[idx].raceList.getWherePartner(cList[idx].getId()));
  }  

 
  /**
   * Select the Cliend at idx pos (cList[idx])
   * and return it's DeviceList content
   */
  public String[][] getDeviceListFullContent(int idx){
    return(cList[idx].deviceList.getFullContent());
  }
  
  /**
   * Select the Cliend at idx pos (cList[idx])
   * and return it's DeviceList content
   */
  public String[][] getDeviceListContent(int idx){
    return(cList[idx].deviceList.getContent());
  }
  
  /**
   *  Return the Timestamp of the last Device List Update 
   */
  long getDeviceListTstamp(int idx) {
    if(idx >= cList.length )   return -1;    
    return(cList[idx].deviceList.getUpdateTstamp());
  }

  /**
   *  Return the Timestamp of the last Race List Update 
   */
  long getRaceListTstamp(int idx) {
    if(idx >= cList.length )   return -1;    
    return(cList[idx].raceList.getUpdateTstamp());
  }



  /**    
   * Select the Cliend at idx pos (cList[idx])
   * and find the Name for a Device in the DeviceList
   * where Device.id == "devId" param 
   * 
   * @return  String: string containing the name 
   * @return  null  : on idx out of range error
   * @return  "-"   : if Device(devId) was not found in the deviceList
   */
  String getDevName(int idx, String devId) { 
    if(idx >= cList.length || idx < 0)   return null;
    return(cList[idx].deviceList.getName(devId));
  }

  /**
   * see getDevName()
   */
  String getShortName(int idx, String devId) { 
    if(idx >= cList.length || idx < 0)   return null;
    return(cList[idx].deviceList.getShortName(devId));
  }


  /**
   * Select the Cliend at idx pos (cList[idx])
   * and returns raceExist()   
   *
   * @return boolean: [Y|N]
   */    
  boolean raceExist(int idx, String raceId) { 
    if(idx >= cList.length || idx < 0)   return false;
    return(cList[idx].raceList.exist(raceId));
  }

  /**
   * Select the Cliend at idx pos (cList[idx])
   * and returns isReadyToConfigure for Race=raceId (every 
   * partecipant has already confirmed it's subscription) 
   *
   * @return boolean: [Y|N]
   */    
  public boolean isRaceReadyToConfigure(int idx, String raceId){
    if(idx >= cList.length )   return false;    
    String p[] = cList[idx].raceList.getPartnersId(raceId);
    if(p.length < 2){
      return(false); // Configure only races with more than one partecipant
    }
    for(int j = 0 ; j < p.length ; j++){
      String devId = p[j].trim();
      String status = cList[idx].deviceList.getStatus(devId);
      if (! status.equals(Protocol.PDevice.Status.SUBSCRIBED)){
        return(false);
      }      
    }
    return(true);
  }

  /**
   * Select the Cliend at idx pos (cList[idx])
   * and return it's 1st level Race List Content 
   * (no partecipant nested list) 
   *
   * @return String[n][5] [n]:Race / [5]:{id,name,status,tstamp,partecipants_n}
   */
  public String[][] getRaceListContent(int idx){
    return(cList[idx].raceList.getContent());
  }

  /**
   * debug only
   */
  public String raceListToString(int idx){
    return(cList[idx].raceList.toString());
  }

  /**
   * Select the Cliend at idx pos (cList[idx]) and
   * get Partecipants List for Race=raceId 
   * (2nd level Race List Content) 
   *
   * @return String[n] - [n]:Partecipant id
   */
  public String[] getRacePartecipantsId(int idx, String raceId){
    return(cList[idx].raceList.getPartnersId(raceId));
  }

}   
