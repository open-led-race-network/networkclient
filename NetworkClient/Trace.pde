class Trace {

    public static final int NONE      =  0;  // no trace, errors only
    public static final int BASE      =  1;  // basic program flow
    public static final int METHOD    =  2;  // constructors and methods invocation
    public static final int INTERNALS =  4;  // basic internals method operations
    public static final int DETAILED  =  8;  // detailed internals method operations
    public static final int LOWLEVEL  = 16;  // low level operations (ex: each byte received over serial)
    public static final int ALL       = BASE + METHOD + METHOD + INTERNALS + DETAILED + LOWLEVEL ; 

    private int    level = NONE;
    private String publisherId = null;
    private String classId     = null;
    
    Trace(){}
    
    Trace(int l){
      setLevel(l);
    }
    
    Trace(String s){
      setLevel(s);
    }
    
    void setLevel(int l){
      this.level=l;
    }
    
    void setPublisherId (String str) {
      this.publisherId=str;
    }
    
    void setClassId (String str) {
      this.classId=str;
    }
    
    
    
    int setLevel(String toParse){
      int res=NONE;
      String[] fields = toParse.split("\\|");
      for(int i=0;i<fields.length;i++){
        switch(trim(fields[i])){
          case "BASE":       res += BASE;       break;
          case "METHOD":     res += METHOD;     break;
          case "INTERNALS":  res += INTERNALS;  break;
          case "DETAILED":   res += DETAILED;   break;          
          case "LOWLEVEL":   res += LOWLEVEL;   break;
          case "ALL":        res =  BASE + METHOD + METHOD + INTERNALS + DETAILED + LOWLEVEL ;   break;
        }
      }       
      setLevel(res);
      return(res);
    } 
  
    void log(int l, String str){
      log(l,str,System.out);
    }
  
    void error(String str){
      log(0xFF,str,System.err);
    }
  
    void log(int l, String str, PrintStream printOut){
      if((l & this.level) != 0 ) {
        if(publisherId != null) {
          if(classId != null) {
            printOut.printf("[%d] [%s] %s.%s",System.currentTimeMillis(), this.publisherId, this.classId, str);
          } else {
            printOut.printf("[%d] [%s] %s",System.currentTimeMillis(), this.publisherId, str);
          }
        } else {
          if(classId != null) {
            printOut.printf("[%d] %s.%s",System.currentTimeMillis(), this.classId, str);
          } else {
            printOut.printf("[%d] %s",System.currentTimeMillis(),str);
          }
        }
      }
    } 
    
    
    
  
}
