
/**
 * OLRBoard Communication fields - Serial
 *
 * PLEASE NOTE:
 *    The format of the Serial Communication is shared 
 *    with Arduino Code.
 *    Modifications of this part of the protocol 
 *    need to involve both codesets
 */
static class ProtocolSerial {
    
    public static final int SAFETYSLEEPTIME      = 50;
    public static final int DEVICESENDQUEUESIZE  = 1024;
    public static final int DEVICERECVQUEUESIZE  = 1024;
    
     public static final int UID_MAXLEN          = 32;  // Unique Board Id (UID) maw length

     private static final Map<String, String> COMMAND_LIST = new HashMap<String, String>();

     public static final byte EOC = '\n';        // End Of Command
     public static final byte PSEP = ',';        // Parameter Separator
     public static final String PSEPSTR = ",";   // Parameter Separator
          
     // Commands
     static class Cmd {     
       
       public static final String OK = "OK";       // Answer from Board on request operation OK 
       public static final String ERROR = "NOK";   // Answer from Board on request operation error
       public static final String NOCOMMAND = "-"; // Returned on Command Code not found

       
       static class GetUID    { public static final byte ID = '$'; }
       static class SetUID    { public static final byte ID = ':'; }
       static class Handshake { public static final byte ID = '#'; }
       static class EnterCfgMode  { public static final byte ID = '@'; }
       static class LeaveCfgMode  { public static final byte ID = '*'; }
       static class SaveCfg       { public static final byte ID = 'W'; }
       static class EnterNetMode  { public static final byte ID = 'n'; } // Network/Relay Race mode

       static class GetSoftwareId { public static final byte ID = '?'; }       
       
              
       static class GetSoftwareVersion {       public static final byte ID = '%';
         /**
          * Extract parameters from Serial-received "Get Software Version" answer
          *   format: m.n.p 
          *      m: [0-9]       - Major
          *      n: [1-9]       - Minor
          *      p: [0-9][0-9]? -Patch (range 0-99)
          *    ex: 0.9.1 
          */         
         public static int[] getParam(String content) {           
           //int[] p = new int[3];
           int ver[] = new int[3];
           String[] sver = content.split("\\.");
           if(sver.length != 3){
             return ver; // returns 0.0.0
           }
           /***
           ver[0]= Integer.parseInt(sver[0]);
           ver[1]= Integer.parseInt(sver[1]);
           ver[2]= Integer.parseInt(sver[2]);
           ***/
           ver[0]= Utils.parseInt(sver[0],0, "ProtocolSerial.GetSoftwareVersion():");
           ver[1]= Utils.parseInt(sver[1],0, "ProtocolSerial.GetSoftwareVersion():");
           ver[2]= Utils.parseInt(sver[2],0, "ProtocolSerial.GetSoftwareVersion():");
           return ver;
           
         }
         
       }


       static class GetBoardConfig {       public static final byte ID = 'Q';
       
        static class BoardConfig {
          HashMap track;
          HashMap ramp;
          HashMap race;
          HashMap battery;
          boolean track_read;
          boolean ramp_read;
          boolean race_read;
          boolean battery_read;
           

          BoardConfig(){
            this.track = new HashMap<String, Integer>();
            this.ramp = new HashMap<String, Integer>();
            this.race = new HashMap<String, Integer>();
            this.battery = new HashMap<String, Integer>();
            this.track_read=false;
            this.ramp_read=false;
            this.race_read=false;
            this.battery_read=false;
          }
          
          boolean isComplete() {
            if(this.track_read && this.ramp_read && this.race_read && this.battery_read){
              return(true);
            }
            return(false);
          }

          void reset() {
            this.track_read=false;
            this.ramp_read=false;
            this.race_read=false;
            this.battery_read=false;
          }
          
          String toString(){
            StringBuilder str = new StringBuilder();
            str.append(String.format("track-> %s\n", Utils.dumpHash(track)));
            str.append(String.format("ramp-> %s\n", Utils.dumpHash(ramp)));
            str.append(String.format("race-> %s\n", Utils.dumpHash(race)));
            str.append(String.format("battery-> %s\n", Utils.dumpHash(battery)));
            return(str.toString());
          }
          
        }
       
         /**
          * Extract parameters from Serial-received "Get Board Config" answer
          * and fill the HashMaps in BoardConfig cfg param
          * It has to be called 3 times, one for each line received when board answer to Q request)
          * 
          *   Complete Board Answer example: 
                 QTRACK:300,300,0,-1,60,0,0.006,0.015
          *      QRAMP:80,90,100,6,0
          *      QRACE:1,5,1,1,0,0
          */
         public static String getParam(String content, BoardConfig cfg) {
           
           if(content.indexOf("TK")!=-1){  // Track
             String track = content.substring(content.indexOf(":") + 1); // Get substring with data 
             String[] trackfield = track.split(",");
             if(trackfield.length != 9) {
               return(String.format("String content error! Invalid Format -> TRACK contains %d fields (8 expected)\n",  trackfield.length ));
             }
             int nled_total = Utils.parseInt(trackfield[0],-10, "ProtocolSerial.Cmd.GetBoardConfig.getParam() trackfield[0]:");
             cfg.track.put("MAXLED", Integer.valueOf(nled_total));
             int nled_main = Utils.parseInt(trackfield[1],-10, "ProtocolSerial.Cmd.GetBoardConfig.getParam() trackfield[1]:");
             cfg.track.put("NLEDMAIN", Integer.valueOf(nled_main));
             int nled_aux = Utils.parseInt(trackfield[2],-10, "ProtocolSerial.Cmd.GetBoardConfig.getParam() trackfield[2]:");
             cfg.track.put("NLEDAUX", Integer.valueOf(nled_aux));
             int init_aux = Utils.parseInt(trackfield[3],-10, "ProtocolSerial.Cmd.GetBoardConfig.getParam(): trackfield[3]");
             cfg.track.put("INITAUX", Integer.valueOf(init_aux));
             int box_len = Utils.parseInt(trackfield[4],-10, "ProtocolSerial.Cmd.GetBoardConfig.getParam(): trackfield[4]");
             cfg.track.put("BOXLEN", Integer.valueOf(box_len));
             
             int box_alwaysOn = Utils.parseInt(trackfield[5],-10, "ProtocolSerial.Cmd.GetBoardConfig.getParam(): trackfield[5]");
             cfg.track.put("BOXALWAYSON", Integer.valueOf(box_alwaysOn));
             float kg_const = Utils.parseFloat(trackfield[6],-10, "ProtocolSerial.Cmd.GetBoardConfig.getParam(): trackfield[6]");
             cfg.track.put("KGCONSTANT", Float.valueOf(kg_const));
             float kf_const = Utils.parseFloat(trackfield[7],-10, "ProtocolSerial.Cmd.GetBoardConfig.getParam(): trackfield[7]");
             cfg.track.put("KFCONSTANT", Float.valueOf(kf_const));
             int auto_start = Utils.parseInt(trackfield[8],-10, "ProtocolSerial.Cmd.GetBoardConfig.getParam(): trackfield[8]");
             cfg.track.put("AUTOSTART", Integer.valueOf(auto_start));
             
             cfg.track_read=true;
             return(null);
           }
           
           if(content.indexOf("RP")!=-1){ // Ramp-Slope
             String ramp = content.substring(content.indexOf(":") + 1); // Get substring with data 
             String[] rampfield = ramp.split(",");
             if(rampfield.length != 5) {
               return(String.format("String content error! Invalid Format -> RAMP contains %d fields (5 expected)\n",  rampfield.length ));
             }
             int init = Utils.parseInt(rampfield[0],-10, "ProtocolSerial.Cmd.GetBoardConfig.getParam(): rampfield[0]");
             cfg.ramp.put("INIT", Integer.valueOf(init));
             int center = Utils.parseInt(rampfield[1],-10, "ProtocolSerial.Cmd.GetBoardConfig.getParam(): rampfield[1]");
             cfg.ramp.put("CENTER", Integer.valueOf(center));
             int end = Utils.parseInt(rampfield[2],-10, "ProtocolSerial.Cmd.GetBoardConfig.getParam(): rampfield[2]");
             cfg.ramp.put("END", Integer.valueOf(end));
             int high = Utils.parseInt(rampfield[3],-10, "ProtocolSerial.Cmd.GetBoardConfig.getParam(): rampfield[3]");
             cfg.ramp.put("HEIGHT", Integer.valueOf(high));
             
             int ramp_alwaysOn = Utils.parseInt(rampfield[4],-10, "ProtocolSerial.Cmd.GetBoardConfig.getParam(): rampfield[4]");
             cfg.ramp.put("ALWAYSON", Integer.valueOf(ramp_alwaysOn));
             
             cfg.ramp_read=true;
             return(null);
           }
           
           if(content.indexOf("RC")!=-1){ // Race
             String race = content.substring(content.indexOf(":") + 1); // Get substring with data 
             String[] racefield = race.split(",");
             if(racefield.length != 8) {
               return(String.format("String content error! Invalid Format -> RACE contains %d fields (8 expected)\n",  racefield.length ));
             }
             int startline = Utils.parseInt(racefield[0],-10, "ProtocolSerial.Cmd.GetBoardConfig.getParam(): racefield[0]");
             cfg.race.put("START", Integer.valueOf(startline));
             int nlap = Utils.parseInt(racefield[1],-10, "ProtocolSerial.Cmd.GetBoardConfig.getParam(): racefield[1]");
             cfg.race.put("LAPS", Integer.valueOf(nlap));
             int nrepeat = Utils.parseInt(racefield[2],-10, "ProtocolSerial.Cmd.GetBoardConfig.getParam(): racefield[2]");
             cfg.race.put("REPEAT", Integer.valueOf(nrepeat));
             int finishline = Utils.parseInt(racefield[3],-10, "ProtocolSerial.Cmd.GetBoardConfig.getParam(): racefield[3]");
             cfg.race.put("FINISH", Integer.valueOf(finishline));
             int player3 = Utils.parseInt(racefield[4],-10, "ProtocolSerial.Cmd.GetBoardConfig.getParam(): racefield[4]");
             cfg.race.put("PLAYER3", Integer.valueOf(player3));
             int player4 = Utils.parseInt(racefield[5],-10, "ProtocolSerial.Cmd.GetBoardConfig.getParam(): racefield[5]");
             cfg.race.put("PLAYER4", Integer.valueOf(player4));

             int demo_mode = Utils.parseInt(racefield[6],-10, "ProtocolSerial.Cmd.GetBoardConfig.getParam(): racefield[6]");
             cfg.race.put("DEMO", Integer.valueOf(demo_mode));
             int network_race = Utils.parseInt(racefield[7],-10, "ProtocolSerial.Cmd.GetBoardConfig.getParam(): racefield[7]");
             cfg.race.put("NETWORK", Integer.valueOf(network_race));

             cfg.race_read=true;
             return(null);
           }
           
           if(content.indexOf("BT")!=-1){ // Battery
             String race = content.substring(content.indexOf(":") + 1); // Get substring with data 
             String[] battfield = race.split(",");
             if(battfield.length != 4) {
               return(String.format("String content error! Invalid Format -> BATTERY contains %d fields (4 expected)\n",  battfield.length ));
             }
             int delta = Utils.parseInt(battfield[0],-10, "ProtocolSerial.Cmd.GetBoardConfig.getParam(): battfield[0]");
             cfg.battery.put("DELTA", Integer.valueOf(delta));
             int min = Utils.parseInt(battfield[1],-10, "ProtocolSerial.Cmd.GetBoardConfig.getParam(): battfield[1]");
             cfg.battery.put("MIN", Integer.valueOf(min));
             int boost_scaler = Utils.parseInt(battfield[2],-10, "ProtocolSerial.Cmd.GetBoardConfig.getParam(): battfield[2]");
             cfg.battery.put("BOOST", Integer.valueOf(boost_scaler));
             int active = Utils.parseInt(battfield[3],-10, "ProtocolSerial.Cmd.GetBoardConfig.getParam(): battfield[3]");
             cfg.battery.put("ACTIVE", Integer.valueOf(active));
             
             cfg.battery_read=true;
             return(null);
           }
           
           
           return(String.format("String content error! Unknown dataset id:[%s]\n",  content ));
         }
         
       } // GetBoardConfig
       
     
       
       static class SendLog   { public static final byte ID = '!';
       
         // Extract parameters from Serial-received command
         public static String[] getParam(String content){ 
           String p[] = new String[2];
           int sep = content.indexOf(PSEPSTR);
           p[0] = content.substring(0, sep);
           p[1] = content.substring(sep+1);
           if(p[0]==null || p[1]==null){
             return(null);
           }
           return(p);
         }
         
         // Check if the Serial-received errorLevel neds to be logged as an error in Application 
         public static boolean isError(String level){
           if(trim(level).equals("1")){
             return(false);
           }
           return(true);
         }
         
         // Convert Serial-received errorLevel in Application logLevel
         public static int getLogLevel(String level){
           switch(trim(level)) {
           case "1":
             return(Trace.INTERNALS);
           case "2":
             return(Trace.BASE);           
           }
           return(Trace.ALL);
         }
       } // SendLog
       

       
       static class SetConfig { public static final byte ID = 'C';

         // return a formatted String (Command + parameters) to be sent to the Board (Serial)
         public static String getCommand(int p, int l, int r, int f){
           StringBuilder cmdBld = new StringBuilder();
           cmdBld.append(String.format("%c%s%c", ProtocolSerial.Cmd.SetConfig.ID , String.valueOf(p), ProtocolSerial.PSEP ));
           cmdBld.append(String.format("%s%c" , String.valueOf(l), ProtocolSerial.PSEP ));
           cmdBld.append(String.format("%s%c" , String.valueOf(r), ProtocolSerial.PSEP ));
           cmdBld.append(String.format("%s%c" , String.valueOf(f), ProtocolSerial.EOC ));  
           return(cmdBld.toString());
         }
       }
       
       static class SetTrackLen { public static final byte ID = 'T';

         // return a formatted String (Command + parameters) to be sent to the Board (Serial)
         public static String getCommand(int n){
           return(String.format("%c%s%c", ProtocolSerial.Cmd.SetTrackLen.ID , String.valueOf(n), ProtocolSerial.EOC ));
         }
       }

       static class SetAutostart { public static final byte ID = 'G';

         // return a formatted String (Command + parameters) to be sent to the Board (Serial)
         public static String getCommand(int n){
           return(String.format("%c%s%c", ProtocolSerial.Cmd.SetAutostart.ID , String.valueOf(n), ProtocolSerial.EOC ));
         }
       }

       static class SetPlayers { public static final byte ID = 'P';

         // return a formatted String (Command + parameters) to be sent to the Board (Serial)
         public static String getCommand(int n){
           return(String.format("%c%s%c", ProtocolSerial.Cmd.SetPlayers.ID , String.valueOf(n), ProtocolSerial.EOC ));
         }
       }

       static class SetBoxLen { public static final byte ID = 'B';

         // return a formatted String (Command + parameters) to be sent to the Board (Serial)
         public static String getCommand(int n, int alwaysOn){
           return(String.format("%c%s%c%s%c", ProtocolSerial.Cmd.SetBoxLen.ID , 
                                             String.valueOf(n), 
                                             ProtocolSerial.PSEP,
                                             String.valueOf(alwaysOn), 
                                             ProtocolSerial.EOC ));
         }
       }

       static class SetSlopeConfig { public static final byte ID = 'A';

         // return a formatted String (Command + parameters) to be sent to the Board (Serial)
         public static String getCommand(int start, int center, int end, int height, int alwaysOn){
           StringBuilder cmdBld = new StringBuilder();
           cmdBld.append(String.format("%c%s%c", ProtocolSerial.Cmd.SetSlopeConfig.ID , String.valueOf(start), ProtocolSerial.PSEP ));
           cmdBld.append(String.format("%s%c" , String.valueOf(center), ProtocolSerial.PSEP ));
           cmdBld.append(String.format("%s%c" , String.valueOf(end), ProtocolSerial.PSEP ));
           cmdBld.append(String.format("%s%c" , String.valueOf(height), ProtocolSerial.PSEP ));
           cmdBld.append(String.format("%s%c" , String.valueOf(alwaysOn), ProtocolSerial.EOC ));  
           return(cmdBld.toString());
         }
       }
       
       static class SetBatteryConfig { public static final byte ID = 'E';

         // return a formatted String (Command + parameters) to be sent to the Board (Serial)
         public static String getCommand(int delta, int min, int boost, int active){
           StringBuilder cmdBld = new StringBuilder();
           cmdBld.append(String.format("%c%s%c", ProtocolSerial.Cmd.SetBatteryConfig.ID , String.valueOf(delta), ProtocolSerial.PSEP ));
           cmdBld.append(String.format("%s%c" , String.valueOf(min), ProtocolSerial.PSEP ));
           cmdBld.append(String.format("%s%c" , String.valueOf(boost), ProtocolSerial.PSEP ));
           cmdBld.append(String.format("%s%c" , String.valueOf(active), ProtocolSerial.EOC ));  
           return(cmdBld.toString());
         }
       }
       

       static class SetDemoModeConfig { public static final byte ID = 'M';
         // return a formatted String (Command + parameters) to be sent to the Board (Serial)
         public static String getCommand(int n){
         return(String.format("%c%s%c", ProtocolSerial.Cmd.SetDemoModeConfig.ID , String.valueOf(n), ProtocolSerial.EOC ));
         }
       }       

       
       static class SetOutTunnelNotify { public static final byte ID = 'N';  }
       
       static class RacePhase          { public static final byte ID = 'R';
         
         public static CodeList  phase  = new CodeList();
         static { 
           phase.add(Phase.ENTERCFG,  "Go to config mode",    1);
           phase.add(Phase.CFGCOMPLETE,  "Configuration Phase Completed",    1);           
           phase.add(Phase.READY,     "Ready",    3);
           phase.add(Phase.COUNTDOWN, "Countdown",4);
           phase.add(Phase.RACING,    "Racing",   5);
           phase.add(Phase.PAUSED,    "Paused",   6);
           phase.add(Phase.RESUME,    "Resume",   7);
           phase.add(Phase.COMPLETE,  "Complete", 8);
         }
         static class Phase {
           static final String ENTERCFG    = "1";
           static final String CFGCOMPLETE = "2"; 
           static final String READY       = "3";
           static final String COUNTDOWN   = "4";
           static final String RACING      = "5";
           static final String PAUSED      = "6";
           static final String RESUME      = "7";
           static final String COMPLETE    = "8";
         }
        
         // return a formatted String to be sent to the Board (Serial)         
         public static String getCommand(String sCode){
            String code=phase.validateCode(sCode);
            if(code == null){
              System.err.printf("ProtocolSerial.Cmd.RacePhase.getCommand():[%s] invalid phase code:[%s]\n",sCode);
              return(null);
            }
            String cmd = String.format("%c%s%c", ProtocolSerial.Cmd.RacePhase.ID , code, ProtocolSerial.EOC);
            return(cmd);
           }         
         }
       
       static class CarTelemetry {       public static final byte ID = 'p';
         /**
          * Extract parameters from Serial-received command
          *   format: NumTrackNlap,Rpos
          *      Num:   [1-9]  - Car Number
          *      Track: [1-9] - Track Name
          *      NLap:  [1-9][0-9]? - Lap Number
          *      Rpos:  [1-9][0-9]? - Position in race
          *      Batt:  [1-9][0-9]?[0-9]? - Battery Level
          *    ex: 0M1,86,98
          */         
         public static String[] getParam(String content) {
           //int num = Integer.parseInt(String.valueOf(ch))
           String p[] = new String[5];
           p[0] = content.substring(0,1);
           p[1] = content.substring(1,2);
           int sep = content.indexOf(PSEPSTR);  // first ','
           p[2] = content.substring(2, sep);
           int sep2 = content.indexOf(PSEPSTR,sep+1); // next ','
           p[3] = content.substring(sep+1,sep2);
           p[4] = content.substring(sep2+1);
           /**          
           p[3] = content.substring(sep+1);
           **/
           if(p[0]==null || p[1]==null || p[2]==null || p[3]==null || p[4]==null ){
             return(null);
           }
           return(p);
         }
       }
 
       static class CarLiving     { public static final byte ID = 'r';   }
       static class CarEntering   { public static final byte ID = 't';   }

       static class CarLeave      { public static final byte ID = 's';   
         /**
          * Extract parameters from Serial-received command
          *
          *  1st char is a bit-encoded value for car Num and Speed
          *    bits [0-4] speed (bit 0=LSB)
          *    bits [5-7] car Num (bit 7=MSB)
          *  Example:   
          *    byte val =  (byte) 0b10100100; 
          *    // car 101, speed 00100 
          */
          static int[] getParam(String content) {
                 int data = (byte) content.charAt(0);
                 
                 int carMask =  0b00000111;
                 int ncar =  carMask & ( data >>> 5 ); // ">>>" Logical right shift,  ">>" Arithmetic right shift
                 //int ncar =  carMask & ( (data & 0xFF )>>> 5 ); // "& 0xFF" mask the low-order 8 bits only (to prevent sign-extension)
                 int speedMask = 0b00011111;
                 int speed = speedMask & data;
                 return(new int[] {ncar,speed});
          } 
       }
      
       static class CarEnter      { public static final byte ID = 'u';
         /**
          * return a formatted String to be sent to the Board (Serial)
          *   see: CarLeave.getParam() above
          */
         public static String getCommand(int carN, int speed){  
           int speedMask = 0b00011111;
           int packed = (carN << 5) | (speed  & speedMask) ;
           String cmd = String.format("%c%c%c", ProtocolSerial.Cmd.CarEnter.ID ,  (char) packed , ProtocolSerial.EOC);
           return(cmd);
         } 
       }
       
       static class CarWin        { public static final byte ID = 'w';
         // return a formatted String to be sent to the Board (Serial)
         public static String getCommand(int carN){
           String cmd = String.format("%c%d%c", ProtocolSerial.Cmd.CarWin.ID , carN, ProtocolSerial.EOC);
           return(cmd);
         }
       }


       //Populate lookup table on loading
       static  {
         COMMAND_LIST.put(Character.toString((char)GetUID.ID), "GetUID");
         COMMAND_LIST.put(Character.toString((char)SetUID.ID), "SetUID");
         COMMAND_LIST.put(Character.toString((char)Handshake.ID), "Handshake");
         COMMAND_LIST.put(Character.toString((char)EnterCfgMode.ID), "EnterCfgMode");
         COMMAND_LIST.put(Character.toString((char)SaveCfg.ID), "SaveConfig");
         COMMAND_LIST.put(Character.toString((char)EnterNetMode.ID), "EnterNewtorkModeConfig");
         COMMAND_LIST.put(Character.toString((char)GetSoftwareId.ID), "GetSoftwareId");
         COMMAND_LIST.put(Character.toString((char)GetSoftwareVersion.ID), "GetSoftwareVer");
         COMMAND_LIST.put(Character.toString((char)GetBoardConfig.ID), "GetBoardConfig");
         COMMAND_LIST.put(Character.toString((char)SendLog.ID), "SendLog");
         COMMAND_LIST.put(Character.toString((char)SetConfig.ID), "SetConfig");
         COMMAND_LIST.put(Character.toString((char)SetTrackLen.ID), "SetTrackLen");
         COMMAND_LIST.put(Character.toString((char)SetAutostart.ID), "SetAutostart");
         COMMAND_LIST.put(Character.toString((char)SetPlayers.ID), "SetPlayers");
         
         COMMAND_LIST.put(Character.toString((char)SetDemoModeConfig.ID), "SetDemoModeConfig");

         COMMAND_LIST.put(Character.toString((char)SetBoxLen.ID), "SetBoxLen");
         COMMAND_LIST.put(Character.toString((char)SetSlopeConfig.ID), "SetSlopeConfig");
         COMMAND_LIST.put(Character.toString((char)SetBatteryConfig.ID), "SetBatteryConfig");
         COMMAND_LIST.put(Character.toString((char)SetOutTunnelNotify.ID), "SetOutTunnelNotify");
         COMMAND_LIST.put(Character.toString((char)RacePhase.ID), "RacePhase");
         COMMAND_LIST.put(Character.toString((char)CarTelemetry.ID), "CarTelemetry");
         COMMAND_LIST.put(Character.toString((char)CarLiving.ID), "CarLiving");
         COMMAND_LIST.put(Character.toString((char)CarLeave.ID), "CarLeave");
         COMMAND_LIST.put(Character.toString((char)CarEntering.ID), "CarEntering");
         COMMAND_LIST.put(Character.toString((char)CarEnter.ID), "CarEnter");
         COMMAND_LIST.put(Character.toString((char)CarWin.ID), "CarWin");
       }
       
       public static String getName(char val) {
         return COMMAND_LIST.getOrDefault(String.valueOf(val),NOCOMMAND);
       }
       
       public static boolean isValid (char val) {
         if(getName(val).equals(NOCOMMAND)){
           return(false);
         }
         return(true);
       }

    } // Cmd

} // Serial
