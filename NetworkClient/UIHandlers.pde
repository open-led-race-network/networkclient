/* 
 * Application "Logic" of the UI:   
 *   Handlers for UI events
 */

/**
 * Intercept keys pressed and executes actions on ScrollableGrid object
 */
void keyPressed() {

  ScrollableGrid currentArea=null; 

  // Mouse inside Race List Display Area ? 
  if (mouseX > g_rRect.x && mouseX < (g_rRect.x + g_rRect.width) && 
      mouseY > g_rRect.y && mouseY < (g_rRect.y + g_rRect.height )) {
     currentArea = g_rArea;
  }
  else if(mouseX > g_rtRect.x && mouseX < (g_rtRect.x + g_rtRect.width) && 
          mouseY > g_rtRect.y && mouseY < (g_rtRect.y + g_rtRect.height )) {
    currentArea = g_rtArea;
  }

  if(currentArea != null) {
    // manage LEFT,UP,RIGHT,DOWN event
     currentArea.defaultKeyEvent(keyCode); 
  } // currentArea != null
  
} 


/*******************************************************************************
  Intercept Mouse wheel and calls the ScrollableGrid.scroll()method 
     (it manages scroll by mouse wheel action)
 *******************************************************************************/
void mouseWheel(MouseEvent event) {

  ScrollableGrid currentArea=null; 

  // Mouse inside Race List Display Area ? 
  if (mouseX > g_rRect.x && mouseX < (g_rRect.x + g_rRect.width) && 
      mouseY > g_rRect.y && mouseY < (g_rRect.y + g_rRect.height )) {
     currentArea = g_rArea;
  }
  else if(mouseX > g_rtRect.x && mouseX < (g_rtRect.x + g_rtRect.width) && 
          mouseY > g_rtRect.y && mouseY < (g_rtRect.y + g_rtRect.height )) {
    currentArea = g_rtArea;
  }  
  if(currentArea != null) {
    currentArea.scroll(event.getCount());
  }  
}
 

/**
 * GENERIC buttonEvents handler
 *   Intercepts BUTTON events for buttons that don't have a specific event handler
 *   like  DINAMICALLY created ones (ex: CreateRace buttons) 
 */
public void handleButtonEvents(GButton button, GEvent event) {
    g_t.log(Trace.DETAILED, String.format("handleButtonEvents() - Button[%s] tag[%s] GEvent[%s]\n",button.getText(), button.tag, event ));

    // CONFIRM PARTECIPATION button 
    if(button.tag.indexOf("CONFIRMRACE_") != -1 ) {
      g_t.log(Trace.BASE, String.format("handleButtonEvents() - CONFIRM PARTECIPATION TO A RACE BUTTON: tag[%s]\n",button.tag ));

      String raceId = getRaceButtonTag("Confirm Partecipation",button.tag);
      if(raceId != null){
        String error = g_localNetworkDevices.sendConfirmSubscriptionToRace(g_curLocalNetDevice,raceId);
        if(error != null ) { 
           G4P.showMessage(this, error , "Confirm Subscription to Race Error" , G4P.ERROR_MESSAGE);
        }    
      }
    } 
    
    // LEAVE RACE button 
    else if(button.tag.indexOf("LEAVERACE_") != -1 ) { 
      g_t.log(Trace.BASE, String.format("handleButtonEvents() - LEAVE RACE BUTTON: tag[%s]\n",button.tag ));

      String raceId = getRaceButtonTag("Leave Race",button.tag);
      if(raceId != null){
        String error = g_localNetworkDevices.sendLeaveRace(g_curLocalNetDevice,raceId);
        if(error != null ) { 
           G4P.showMessage(this, error , "Leave Race Error" , G4P.ERROR_MESSAGE);
        }    
      }  
    }

    // DELETE RACE button generated in the Race List
    else if(button.tag.indexOf("DELETE_") != -1 ) { 
      g_t.log(Trace.BASE, String.format("handleButtonEvents() - DELETE RACE BUTTON: tag[%s]\n",button.tag ));

      String raceId = getRaceButtonTag("Delete Race",button.tag);
      if(raceId != null){
        String error = g_localNetworkDevices.sendDeleteRace(g_curLocalNetDevice,raceId);
        if(error != null ) { 
           G4P.showMessage(this, error , "Delete Race Error" , G4P.ERROR_MESSAGE);
        }    
      }
    }
     
    // JOIN RACE button generated in the Race List
    else if(button.tag.indexOf("JOINRACE_") != -1 ) { 
      g_t.log(Trace.BASE, String.format("handleButtonEvents() - JOIN RACE BUTTON: tag[%s]\n",button.tag ));

      String raceId = getRaceButtonTag("Join Race",button.tag);
      if(raceId != null){
        String error = g_localNetworkDevices.sendSubscribeToRace(g_curLocalNetDevice,raceId);
        if(error != null ) { 
           G4P.showMessage(this, error , "Join Race Error" , G4P.ERROR_MESSAGE);
        }    
      }  
    } 

    // CONFIGURE RACE button generated in the Race List
    else if(button.tag.indexOf("CONFRACE_") != -1 ) { 
      g_t.log(Trace.BASE, String.format("handleButtonEvents() - CONFIGURE RACE BUTTON: tag[%s]\n",button.tag ));

      String raceId = getRaceButtonTag("Config Race",button.tag);
      if(raceId != null){
        String error = g_localNetworkDevices.sendConfigureRace(g_curLocalNetDevice,raceId);
        if(error != null ) { 
           G4P.showMessage(this, error , "Config Race Error" , G4P.ERROR_MESSAGE);
        }    
      }  
    } 

    // "Done" configuring a Race Button 
    else if(button.tag.indexOf("RCFGDONE_") != -1 ) { 
      g_t.log(Trace.BASE, String.format("handleButtonEvents() - DONE CONFIGURING A RACE BUTTON: tag[%s]\n",button.tag ));
        
      String raceId = getRaceButtonTag("Config Race",button.tag);
      if(raceId != null) {      
          g_t.log(Trace.DETAILED, String.format("handleButtonEvents() - Performing CONFIG Race [%s]\n",raceId ));

          // Get Race Partecipants
          //   getRacePartecipants() returns: String[n][2] - [n]:Partecipant / [2]:{id,status}
          String devs[] = g_localNetworkDevices.getRacePartecipantsId(g_curLocalNetDevice, raceId);
     
          int cliNum = devs.length;
          RaceConfigList raceCfgList = new RaceConfigList(cliNum);
          String error = raceCfg_fillCfgList(devs, raceCfgList);
          if(error != null ) { 
            G4P.showMessage(this, error , "configure Race Error" , G4P.ERROR_MESSAGE);
            return;
          }
          
          Protocol.Network.PDU pdu = new Protocol.Network.PDU();
          error = Protocol.Network.Channel.RaceConfiguration.encodePDUPayload(raceCfgList, pdu);
          if(error != null ) { 
            G4P.showMessage(this, error , "configure Race Error" , G4P.ERROR_MESSAGE);
            return;
          }
    
          // SEND RaceConfigCommand
          //error = g_localNetworkDevices.sendRaceConfigParam(g_curLocalNetDevice,jsonCmd);
          error = g_localNetworkDevices.sendRaceConfigParam(g_curLocalNetDevice, pdu);
          if(error != null ) { 
             G4P.showMessage(this, error , "configure Race Error" , G4P.ERROR_MESSAGE);
          }
      }    
    }  

   // PLAY AGAIN RACE button generated in the Race List
    else if(button.tag.indexOf("PLAYAGAINRACE_") != -1 ) { 
      g_t.log(Trace.BASE, String.format("handleButtonEvents() - PLAY_AGAIN RACE BUTTON: tag[%s]\n",button.tag ));
      
      String raceId = getRaceButtonTag("Play Again Race",button.tag);
      if(raceId != null){
        String error = g_localNetworkDevices.sendPlayAgainRace(g_curLocalNetDevice,raceId);
        if(error != null ) { 
           G4P.showMessage(this, error , "Play Again Race Error" , G4P.ERROR_MESSAGE);
        }    
      }  
    } 

} 
 

/**
 * Split Button TAG, get RaceId and check if race exist
 * @return RaceId
 */
private String getRaceButtonTag(String dStr, String bTag) { 

      String [] list = split (bTag, "_" );
      if(list.length != 2) {
        g_t.error(String.format("%s:getRaceButtonTag() - split (button.tag, \"_\" ) ERROR - Expected 2 parts, Found [%d]\n", dStr, list.length));
        G4P.showMessage(this, dStr+ ": split (button.tag, \"_\" ) ERROR - See Logfile" , "Leave Race Error", G4P.ERROR_MESSAGE);
      } else {
        String raceId = list[1].trim();
        boolean exist = g_localNetworkDevices.raceExist(g_curLocalNetDevice, raceId);
        if( ! exist) {
          g_t.error(String.format("%s:getRaceButtonTag - RaceId NOT FOUND:[%s]\n", dStr, raceId));
          G4P.showMessage(this, dStr + ":  RaceId NOT FOUND ERROR - See Logfile" , "Leave Race Error", G4P.ERROR_MESSAGE);
        } else {   
          return(raceId);
        } 
      } 
    return(null);    
} 

/**
 * fills the List of Configuration Objects with User Input values for each partecipant (Position, Loop, Repeat)
 */
String  raceCfg_fillCfgList(String [] devs, RaceConfigList cfgList) {

    // Prepare cfgList 
    for(int i=0; i< devs.length; i++){
      cfgList.add( new RaceConfig(devs[i]) );
    } 

    // Get the list of GTextField currently existing in the ScrollableGrid
    // and Replace the undefined (-1) values for RaceConfig objects 
    // with the GTextField values.
    //
    // GTextField were defined in uiGetConfigList() with tags: 
    //         [RCFG_POS_<devId>], [RCFG_LOOP_<DevId>], [RCFG_REPEAT_<DevId>] 
    GTextField f[] = g_rArea.getGTextfields();
    for(int i=0; i<f.length;i++) {
      if(f[i].tag.indexOf("RCFG_") != -1){ 
        String [] l = split (f[i].tag, "_" );
        String devId = l[2].trim();  
        Integer val = -1;
        try { val = Integer.parseInt(f[i].getText().trim());}catch (NumberFormatException e){;}
        // get object in list
        RaceConfig rec = cfgList.getRaceConfig(devId);
        if(rec == null){
          return(String.format("raceCfg_fillCfgList() SOFTWARE ERROR: Device [%s] not found in cfgList\n",devId));
        }
        if(f[i].tag.indexOf("RCFG_POS_") != -1){
          rec.setPos(val); // <devId>_P, val  
        }
        if(f[i].tag.indexOf("RCFG_LOOP_") != -1){
          rec.setLaps(val); // <devId>_L, val
        }
        if(f[i].tag.indexOf("RCFG_REPEAT_") != -1){
          rec.setRepeat(val); // <devId>_R, val  
        }
      }
    }
    return(null);
}


/**
 * event Handlers for LOCAL CLIENT SELECTION Droplist
 */
void handleDropListEvents(GDropList list, GEvent event)    { 
  if (list == lcDroplist)  {
    int idx=lcDroplist.getSelectedIndex();
    g_t.log(Trace.DETAILED, String.format("handleDropListEvents() - Item [%d] Selected - [%s]\n",idx, lcDroplist.getSelectedText()));

    if(idx < g_localNetworkDevices.getNetworkDevicesNum()) {
      g_prevLocalNetDevice = g_curLocalNetDevice;
      g_curLocalNetDevice = idx;
      
      // Set Publisher ID in LOG/TRACE object
      //String netDeviceSName=g_localNetworkDevices.getSName(g_curLocalNetDevice);
      //g_t.setPublisherId(netDeviceSName);
      String netDeviceId=g_localNetworkDevices.getId(g_curLocalNetDevice);
      g_t.setPublisherId(netDeviceId);
      
    }
    g_rtArea_lastUpdate=0;
    g_rArea_lastUpdate=0;
  } else {
    g_t.log(Trace.DETAILED, String.format("handleDropListEvents() - UNEXPECTED DropList EVENT:[%s]\n",event));
  }
}

// Crate New Race Button
public void g_createRace_button_click(GButton source, GEvent event) {
    g_t.log(Trace.BASE, String.format("g_createRace_button_click() - ADDRACE BUTTON\n"));

    g_createRace_button.setVisible(false);
    grpCreateRace.setVisible(true);
    g_grpCreateRaceIsVisible = true;
}

// Submit New Race Parameters Button
public void g_newRace_submit_click(GButton source, GEvent event) {
  g_t.log(Trace.BASE, String.format("g_newRace_submit_click() - SUBMIT NEW RACE NAME BUTTON\n"));
  
  String rName = g_newRace_R_Name.getText();
  String errName = Utils.checkValidName(rName);
  if(errName != null ) {  
     G4P.showMessage(this, "Race Name: " + errName , "Create Race Error" , G4P.ERROR_MESSAGE);
  } else {    
    // CREATE RACE
    String error = g_localNetworkDevices.sendCreateRace(g_curLocalNetDevice,rName);
    if(error != null ) { 
       G4P.showMessage(this , error, "Create Race Error" , G4P.ERROR_MESSAGE);
    }
  }
  grpCreateRace.setVisible(false);
  g_grpCreateRaceIsVisible = false;
  
} 
