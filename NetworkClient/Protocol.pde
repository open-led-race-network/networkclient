/**
 *
 *  "Protocol" Class is a structure of STATIC nested classes 
 *  and STATIC metods implementing the Network and Serial Protocols 
 *  used in OLRNetwork
 *  
 *  The FIRST SUBLEVEL contains definition used to encode/decode 
 *  object when transmitted/received over the network.
 *    PDevice, PRace, PCar, etc contain static definitions 
 *    of JSON Names and Valid codesets for Device, Race, Car, 
 *    etc object.
 *    At the program level they will be accessed like:
 *      Protocol.PDevice.Status.jName   // JSON name (-> "TS")
 *      Protocol.PDevice.Status.ONLINE  // JSON Value (-> "01")
 *        
 *  The "Network.PDU" - Protocol Data Unit - contains fields 
 *  of the MQTT message used in the protocol (Payload, isRetained,QoS)
 *    
 *  The "Network.Channel" inner classes DEFINE THE FORMAT of the JSON objects
 *  transmitted FOR EACH SINGLE channel (Topic), implementing methods to
 *  encode/decode the message.
 *  Currently implemented channels:
 *    Network.Channel.DeviceStatus -> OLR/basePool/device/status/<DeviceId> 
 *    Network.Channel.RaceStatus -> OLR/basePool/race/status/<RaceId> 
 *    Network.Channel.RaceParticipantStatus -> OLR/basePool/race/<RaceId>/Participants/<DeviceId> 
 *    Network.Channel.RaceConfiguration -> OLR/basePool/race/<RaceId>/Config 
 *    Network.Channel.RaceCar -> OLR/basePool/race/<RaceId>/Cars/<CarId>
 *    Network.Channel.RaceTelemetry -> OLR/basePool/race/<RaceId>/Telemetry
 *
 *  So, for example, "Network.Channel.DeviceStatus" will be used, in the 
 *  main program, to:
 *  - transmit a change in Status of a Device Object 
 *  - deserialize a JSON Payload received and get a Device Object
 *
 *  The "Network.Serial" inner classes DEFINE THE FORMAT of the COMMANDS
 *  used in the Serial communication with the ORL Board
 *
 */  
//import java.util.HashMap;
//import java.util.Map;

static public class Protocol {
  
  // PROTOCOL and CLIENT VERSION
  //   Ver number identifies MQTT Payloads
  //   Messages with different versions are 
  //   DROPPED by the Client
  //   -- allows different client to exist simultaneously
  //   -- in the same Network (Topics Set)
  static class OLRNVersion {
    public static final String jName = "VV";
    public static final String ver   = "0.4";
  }

    
  // MQTT coded QOS values 
  ////////////////////////
  static class MQTT_QOS {
      public static final int AT_MOST_ONCE    = 0;
      public static final int AT_LEAST_ONCE   = 1;
      public static final int EXACTLY_ONCE    = 2;
  }

  
  public static final String TEXTPAYLOAD_HEADER  = "X:";
  
  

  /**
   * Device related fields - Network
   */
  static class PDevice {

     public static CodeList  status  = new CodeList();
     static { 
       status.add(Status.OFFLINE,         "Offline", 0); // non inviato dalla board /....INVIA 2 ????
       status.add(Status.ONLINE,          "Online",    1);
       status.add(Status.AVAILABLE,       "Available",    3);
       status.add(Status.SUBSCRIBING,     "Subscribing to Race",4);
       status.add(Status.LEAVING,         "Leaving a Race",   5);
       status.add(Status.SUBSCRIBED,      "Subscribed to Race",   6);
       status.add(Status.CONFIGURING,     "Configuring a Race",   7);
       status.add(Status.CONFIGURING_DEV, "Configuring Local Phisical Device", 8);
       status.add(Status.CONFIGURED,      "Configured for Race", 9);
//       status.add(Status.CONFIGURING_DEV, "Configuring Local Phisical Device", 10);
       status.add(Status.CONFIG_ERROR,    "Error configuring Phisical Device", 1);
       status.add(Status.READY,           "Ready to Start", 12);
       status.add(Status.RACING,          "Racing", 13);
       status.add(Status.RACEDONE,        "Race Complete", 14);
       status.add(Status.RACEAGAIN,       "Play Race Again", 15);
       status.add(Status.STALE,           "Not Responding", 16);
       status.add(Status.UNDEFINED,       "Code value NOT FOUND", 99);
       
     }
     static class Status {
        public static final String jName = "TS";
       
       static final String OFFLINE         = "00"; // non inviato dalla board
       static final String ONLINE          = "01";
       static final String AVAILABLE       = "A0";
       static final String SUBSCRIBING     = "J1";
       static final String LEAVING         = "L0";
       static final String SUBSCRIBED      = "R0";
       static final String CONFIGURING     = "R1";
       static final String CONFIGURING_DEV = "R2";
       static final String CONFIGURED      = "R3";
       static final String READY           = "R4";
       static final String RACING          = "R5";
       static final String RACEDONE        = "R6";
       static final String RACEAGAIN       = "R7";
       static final String CONFIG_ERROR    = "R9";
       static final String STALE           = "GN";
       static final String UNDEFINED       = "-";  
     }
     
    static class Id       { public static final String jName = "TI"; } // Device Id   
    static class SName    { public static final String jName = "TM"; } // Device Short Name
    static class Name     { public static final String jName = "TN"; } // Device Name
    
  } // Device

  
  /**
   * Race related fields - Network
   */
  static class PRace {
  
     public static CodeList  status  = new CodeList();
     static { 
       status.add(Status.ACCEPTING,          "Acceping Participants", 0); 
       status.add(Status.CONFIGURING,        "Configuring",    1);
       status.add(Status.CONFIGURED,         "Configured",    3);
       status.add(Status.CONFIGURING_DEVICE, "Waiting for Participant's Phisical Device Configuration",4);
       status.add(Status.CONFIGDEV_ERROR,    "Error configuring one of the OLR Participants",   5);
       status.add(Status.CONFIGDEV_OK,       "Participans Configured",   6);
       status.add(Status.READY_TO_START,     "Ready to Start",   7);
       status.add(Status.COUNTDOWN,          "Countdown", 8);
       status.add(Status.RACING,             "Racing", 9);
       status.add(Status.PAUSED,             "Paused", 10);
       status.add(Status.RESUMED,            "Resumed", 11);
       status.add(Status.COMPLETE,           "Comlete", 12);
       status.add(Status.DELETED,            "Deleted", 14);
     }
     static class Status {
       public static final String jName = "RS";
       
       static final String ACCEPTING          = "A0"; 
       static final String CONFIGURING        = "C1";
       static final String CONFIGURED         = "C2";
       static final String CONFIGURING_DEVICE = "C3";
       static final String CONFIGDEV_ERROR    = "C4";
       static final String CONFIGDEV_OK       = "R0";
       static final String READY_TO_START     = "R3";
       static final String COUNTDOWN          = "R4";
       static final String RACING             = "R5";
       static final String PAUSED             = "R6";
       static final String RESUMED            = "R7";
       static final String COMPLETE           = "R8";
//       static final String START_OVER         = "R9";
       static final String DELETED            = "DD";
     }
    
    static class Id           { public static final String jName     = "RI";  } // Race Id
    static class Name         { public static final String jName     = "RN";  } // Race Name
    static class UpdatedBy    { public static final String jName     = "RU";  } // Id of the Device sending the Message

    
  } // class Race
  

  /**
   *  Car related fields - Network 
   *  Used in Topics:
   *    - OLR/basePool/race/<RaceId>/Cars/<CarId> 
   *    - OLR/basePool/race/<RaceId>/Send/<TrackId>
   */
  static class PCar {
     
     public static CodeList  status  = new CodeList();
     static { 
       status.add(Status.STOP,    "Stopped", 0); 
       status.add(Status.RACING,  "Racing",  1);
       status.add(Status.LEAVING, "Leaving", 2);
       status.add(Status.LEAVE,   "Gone",    3);
       status.add(Status.WINNER,  "Winner",  8);
     }
     static class Status {
       public static final String jName = "CS";
 
       static final String STOP    = "0"; 
       static final String RACING  = "1";
       static final String LEAVING = "2";
       static final String LEAVE   = "3";
       static final String WINNER  = "8";
     }

    static class Id     { public static final String jName     = "CI"; } // Car Id
    static class Name   { public static final String jName     = "CN"; } // car Name
    static class CurDev { public static final String jName     = "CD"; } // Where the car currently is (OLRDevice ID)
    static class Speed  { public static final String jName     = "CP"; } // Car Speed
     
  } 

  /**
   *  Car Telemetry fields - Network 
   *  Used in Topics:
   *    - OLR/basePool/race/<RaceId>/Cars/<CarId> 
   *    - OLR/basePool/race/<RaceId>/Send/<TrackId>
   */
  static class PCarTelemetry {

//    public static final String jName = "p";

    static class OlrId { public static final String jName   = Protocol.PDevice.Id.jName; } // Uses Jname of Device Id 
    static class Id    { public static final String jName     = "C"; } // Car Num
    static class Track { public static final String jName     = "T"; } // Track Id
    static class Lap   { public static final String jName     = "L"; } // Lap Number
    static class RPos  { public static final String jName     = "R"; } // relative position in current Lap
    static class Batt  { public static final String jName     = "B"; } // Battery level
     
  } 
  
  /**
   *  Race Configuration fields - Network 
   */  
  static class PRaceConfig {

      public static final String jName = "RC";
  
      static class OlrId    { public static final String jName   = Protocol.PDevice.Id.jName; } // Uses Jname of Device Id 
      static class Position { public static final String jName = "PO"; } // "Position" of the Device in the Race 
      static class Laps     { public static final String jName = "LO"; } // "Loops" for the Device in the Race 
      static class Repeat   { public static final String jName = "RE"; } // "Repeat" for the Device in the Race 
  } 
  




  /**
   *  Network Messages
   */
  static class Network {
  
      /**
       *  Protocol Data Unit (PDU)
       *
       *  In the current implementation it 
       *  corresponds to a MQTTMessage   
       */
      static class PDU {
        String  channel; // MQTT Topic
        String  payload;
        JSONObject json;
        boolean isRetained;
        int     qos; 
        
        PDU(){
          this.channel=null;
          this.payload=null;
          this.json=null;
          this.isRetained=false;
          this.qos = -1;
        } 
        PDU(String c, String p, int q, boolean r) {
          this.channel=c;
          this.payload=p;
          this.json=null;
          this.qos = q;
          this.isRetained=r;
        }
        PDU(String c, JSONObject j, int q, boolean r) {
          this.channel=c;
          this.payload=null;
          this.json=j;
          this.qos = q;
          this.isRetained=r;
        }

      } 
      
      // Error String identifying Protocol Errors
      public static final String PERROR = "PROTOCOL ERROR";
      
      /**
       *  Channel class
       *  Contains ONE inner class for each communication channel (MQTT Topic)
       *  that DEFINES the FORMAT of PDU.payload (Message Payload) for the CHANNEL
       *
       *  Each inner class implements:
       *  - encodePDUPayload()
       *  - decodePDU()
       */
      static class Channel {
          
          /**
           *  Channel: Status of OLR NetworkDevice
           *    [OLR/basePool/device/status/<DeviceId>] Topic 
           */ 
          static class DeviceStatus {
              /**
               * Prepare PDU PayLoad for messages to be Published
               * @param dev Device Object
               * @param pdu PDU Object
               * @return null: ok
               * @return err String: String containing the error description
               */  
              static String encodePDUPayload(Device dev, PDU pdu) {
                String code=Protocol.PDevice.status.validateCode(dev._status);
                if(code == null) {
                  return(String.format("%s invalid Status Code:[%s]\n",PERROR, dev._status ));
                }

                pdu.json = new JSONObject()  // Build JSON Payload
                                    .put(Protocol.OLRNVersion.jName, Protocol.OLRNVersion.ver)
                                    .put(Protocol.PDevice.Id.jName, dev._id)
                                    .put(Protocol.PDevice.Status.jName, dev._status)
                                    .put(Protocol.PDevice.SName.jName,dev._sName)
                                    .put(Protocol.PDevice.Name.jName,dev._name);
                                    
                pdu.payload = pdu.json.toString();  // Build Payload String                    
                return(null);  
              }
              
              /**
               * Decode PDU PayLoad for messages received from Network
               * @param pdu PDU Object
               * @param dev Empty Device Object to be filled 
               * @return null: ok
               * @return err String: String containing the error description
               */  
              static String decodePDU(PDU pdu, Device dev) {

                String id = trim(pdu.json.getString(Protocol.PDevice.Id.jName , "-"));      
                String status = trim(pdu.json.getString(Protocol.PDevice.Status.jName , "-"));
                String sname = trim(pdu.json.getString(Protocol.PDevice.SName.jName , "-"));
                String name = trim(pdu.json.getString(Protocol.PDevice.Name.jName , "-"));
            
                if( id.equals("-") ){
                  return(String.format("%s Missing Device [Id] field:[%s]\n", PERROR, Protocol.PDevice.Id.jName ));
                }
                if( status.equals("-")) {
                  return(String.format("%s missing Device [Status] field:[%s]\n", PERROR, Protocol.PDevice.Status.jName));
                }        
                String code=Protocol.PDevice.status.validateCode(status);
                if(code == null){
                  return(String.format("%s invalid Status Code:[%s]\n", PERROR, status ));
                }
                if( name.equals("-")) {
                  return(String.format("%s missing Name field[%s]\n", PERROR, Protocol.PDevice.Name.jName));
                }
                if( sname.equals("-")) {
                  return(String.format("%s missing SName field[%s]\n", PERROR, Protocol.PDevice.SName.jName));
                }
                
                //Device item = new Device(id, sname, name, status );
                dev.setFields(id, sname, name, status);
                return(null);
              }
          } 
          
          
          /**
           *  Channel: Status of a Race
           *    [OLR/basePool/race/status/<RaceId>] Topic 
           */ 
          static class RaceStatus {
              /**
               * Prepare PDU PayLoad for messages to be Published
               * @param race Race Object
               * @param pdu PDU Object
               * @return null: ok
               * @return err String: String containing the error description
               */  
                static String encodePDUPayload(Race race, PDU pdu){
                
                String code=Protocol.PRace.status.validateCode(race._status);
                if(code == null) {
                  return(String.format("%s invalid Status Code:[%s]\n", PERROR, race._status ));
                }    
                pdu.payload = new JSONObject()  // Build JSON Payload
                                    .put(Protocol.OLRNVersion.jName, Protocol.OLRNVersion.ver)
                                    .put(Protocol.PRace.Id.jName, race._id)
                                    .put(Protocol.PRace.Status.jName, race._status)
                                    .put(Protocol.PRace.Name.jName, race._name)
                                    .put(Protocol.PRace.UpdatedBy.jName, race._updatedBy)
                                    .toString(); // Construct JSON Payload  
                return(null);  
              }  
              
              
              /**
               * 
               */          
              static String decodePDU(PDU pdu, Race race) {
                
                String id = trim(pdu.json.getString(Protocol.PRace.Id.jName , "-"));      
                String status = trim(pdu.json.getString(Protocol.PRace.Status.jName , "-"));
                String name = trim(pdu.json.getString(Protocol.PRace.Name.jName , "-"));
                String updatedBy = trim(pdu.json.getString(Protocol.PRace.UpdatedBy.jName , "-"));
            
                if( id.equals("-") ) {  
                  return(String.format("%s missing Name Field:[%s]\n", PERROR, Protocol.PRace.Id.jName));
                }
                if( name.equals("-") ) {  
                  return(String.format("%s missing Name Field:[%s]\n", PERROR, Protocol.PRace.Name.jName));
                }
                if( status.equals("-") ) {  
                  return(String.format("%s missing Status Field:[]\n", PERROR,Protocol.PRace.Status.jName));
                }
                
                String code=Protocol.PRace.status.validateCode(status);
                if(code == null){
                  return(String.format("%s invalid Status Field:[%s]=[%s]\n", PERROR, Protocol.PRace.Status.jName, status));
                }
            
                // Check UpdatedBy
                if( updatedBy.equals("-") ) {  
                  return(String.format("%s missing Field:[%s]\n", PERROR, Protocol.PRace.UpdatedBy.jName));
                }
    
                //Race item = new Race(id, name, status, updatedBy );
                race.setFields(id, name, status, updatedBy);
                return(null);
                
              }
          }
        
          /**
          *  Channel: Status of a Device partecipating to a Race
          *    [OLR/basePool/race/<RaceId>/Participants/<DeviceId>] Topic
          */ 
          static class RaceParticipantStatus {
              /**
               * RaceParticipantStatus Channel uses same PDU as 
               * DeviceStatus Channel (PDevice PDU)
               */
              static String encodePDUPayload(Device dev, PDU pdu){
                return(DeviceStatus.encodePDUPayload(dev,pdu));
              }
              static String decodePDU(PDU pdu, Device dev) {
                return(DeviceStatus.decodePDU(pdu,dev));
              }
          }
    
          /**
          *  Channel: Configuration data for every device partecipating to a race
          *    [OLR/basePool/race/<RaceId>/Config] Topic
          */ 
          static class RaceConfiguration {
              /**
               * Prepare PDU PayLoad for messages to be Published
               * @param cfgList RaceConfigList Object
               * @param pdu PDU Object
               * @return null: ok
               * @return err String: String containing the error description
               *
               * RaceConfiguration message JSON format example:
               *     {
               *       "VV": "0.2"            -> Protocol Version
               *       "RC": [                -> Array of Participants
               *        {
               *          "TI": "HAM-A",      -> Participant 1
               *          "PO": 1
               *          "LO": 5
               *          "RE": 2
               *        },
               *        {
               *          "TI": "HAM-B",      -> Participant 2
               *          "PO": 1
               *          "LO": 5
               *          "RE": 2
               *        }
               *       ]
               *     }
               */
              static String encodePDUPayload(RaceConfigList cfgList, PDU pdu){
                JSONObject jsonCmd = new JSONObject();
                encodeJSONPayload(cfgList ,jsonCmd);
                pdu.json=jsonCmd; // set json field
               pdu.payload=jsonCmd.toString();
                return(null);   
              }     
              
              static String encodeJSONPayload(RaceConfigList cfgList, JSONObject jsonCmd){
                JSONArray values = new JSONArray();
                for (int i = 0; i < cfgList.size(); i++) {
                  JSONObject dev = new JSONObject();
                  RaceConfig rec = cfgList.getRaceConfig(i);
                  dev.setString(Protocol.PRaceConfig.OlrId.jName, rec.olrId);
                  dev.setInt(Protocol.PRaceConfig.Position.jName, rec.pos);
                  dev.setInt(Protocol.PRaceConfig.Laps.jName, rec.laps); 
                  dev.setInt(Protocol.PRaceConfig.Repeat.jName, rec.repeat);
                  values.setJSONObject(i, dev);
                }
                jsonCmd.setString(Protocol.OLRNVersion.jName, Protocol.OLRNVersion.ver);
                jsonCmd.setJSONArray( Protocol.PRaceConfig.jName, values);
                return(null);  
              }
              
              /**
               *
               */
              static String decodePDU(PDU pdu, RaceConfigList rCfgList) {
                
                JSONArray cfgData = pdu.json.getJSONArray(Protocol.PRaceConfig.jName);
                if(cfgData == null) {
                  return(String.format("%s ConfigValues Array=[%s] Not found \n",PERROR, Protocol.PRaceConfig.jName));
                }
                int cliNum = cfgData.size();
                rCfgList.initSize(cliNum);

                // Get each Config object in the array
                for (int i = 0; i < cliNum; i++) {
                   JSONObject cfg = cfgData.getJSONObject(i);
                   String deviceId  = cfg.getString(Protocol.PRaceConfig.OlrId.jName);
                   int pos    = cfg.getInt(Protocol.PRaceConfig.Position.jName);
                   int laps   = cfg.getInt(Protocol.PRaceConfig.Laps.jName);
                   int repeat = cfg.getInt(Protocol.PRaceConfig.Repeat.jName);
                   
                   rCfgList.add(deviceId, pos, laps,repeat) ;
                 }
                return(null);
              }

              
          }
        
          /**
          *  Channel: Status of a Car partecipating to a Race
          *    [OLR/basePool/race/<RaceId>/Cars/<CarId>] Topic
          */ 
          static class CarStatus {
              /**
               *  
               */
              static String encodeTextPayload(int id, String name, String cdev, String status, int speed){
                String code=Protocol.PCar.status.validateCode(status);
                if(code == null) {
                    System.err.printf("%s Protocol.Network.Channel.CarStatus.encodeTextPayload(): invalid status code:[%s]\n",PERROR,status);
                    return(null);
                }    
                String payload =  String.format("%s%d,%s,%s,%s,%d",Protocol.TEXTPAYLOAD_HEADER,id,name,cdev,status,speed);
                return(payload);  
              }   
              /**
               * 
               */          
              static String decodeTextPayload(String txt, Car car) {

                String[] fields = txt.substring(TEXTPAYLOAD_HEADER.length()).split(",");
                if(fields.length != 5){
                  return(String.format("%s found [%d] params - expected [5]\n",PERROR,fields.length));
                }
                int carId=Integer.valueOf(fields[0]);
                int speed=Integer.valueOf(fields[4]);
                
                car.setFields(carId, fields[1],fields[2],fields[3],speed);
                return(null);
              }           

            
              /**
               * Prepare PDU PayLoad for messages to be Published
               * @param car Car Object
               * @param pdu PDU Object
               * @return null: ok
               * @return err String: String containing the error description
               */  
               static String encodePDUPayload(Car car, PDU pdu){  
                String code=Protocol.PCar.status.validateCode(car.status);
                if(code == null) {
                  return(String.format("%s invalid Status Code:[%s]\n", PERROR, car.status ));
                }    

                pdu.payload = new JSONObject()  // Build JSON Payload
                                    .put(Protocol.OLRNVersion.jName, Protocol.OLRNVersion.ver)
                                    .put(Protocol.PCar.Id.jName, car.id)
                                    .put(Protocol.PCar.Name.jName, car.name)
                                    .put(Protocol.PCar.CurDev.jName, car.curDev)
                                    .put(Protocol.PCar.Status.jName, car.status)
                                    .put(Protocol.PCar.Speed.jName, car.speed)
                                    .toString(); // Construct JSON Payload  
                return(null);  
              }   

              /**
               * 
               */          
              static String decodePDU(PDU pdu, Car car) {
                
                int id = pdu.json.getInt(Protocol.PCar.Id.jName , -1);      
                String name = trim(pdu.json.getString(Protocol.PCar.Name.jName , "-"));
                String curOLR = trim(pdu.json.getString(Protocol.PCar.CurDev.jName , "-"));
                String status = trim(pdu.json.getString(Protocol.PCar.Status.jName , "-"));
                int speed = pdu.json.getInt(Protocol.PCar.Speed.jName , -1);
                if(id== -1) {
                  return(String.format("%s missing [Id] field:[%s]\n", PERROR, Protocol.PCar.Id.jName ));
                }
                if( name.equals("-")) {
                  return(String.format("%s missing [name] field[%s]\n", PERROR, Protocol.PCar.Name.jName));
                }
                if( curOLR.equals("-")) {
                  return(String.format("%s missing [curDev] field:[%s]\n", PERROR, Protocol.PCar.CurDev.jName));
                }
                if( status.equals("-")) {
                  return(String.format("%s missing [status] field:[%s]\n", PERROR,  Protocol.PCar.Status.jName));
                }  
                
                car.setFields(id, name,curOLR,status,speed);
                return(null);
               
              }           
               
    
          }
          /**
          *  Channel: Telemetry data for racing cars (used in race visualization)
          *    [OLR/basePool/race/<RaceId>/Telemetry] Topic
          */ 
          static class RaceTelemetry {
            /**
             * Prepare JSON PayLoad for messages to be Published
             *
             *  - Accept PLAIN parameters and SET JSONObject (not CarTelemetry obj + PDU)
             *    to avoid object creation overhead on every method invocation
             *
             *  - The Payload DOES NOT contains OLRVersion
             *
             * @param id Car Id           
             * @param trackId  Id of the current track (Main, Box, etc)
             * @param lap current lap number
             * @param rPos current normalized position in lap (%)  
             * @param pdu
             * @return null: ok
             * @return err String: String containing the error descriptio
             */                
            static String encodeJSONPayload(String devId, int id, String trackId, int lap, int rPos, int batt, JSONObject json) {
              /**
              JSONObject carPos = new JSONObject()
                .put(Protocol.PCarTelemetry.OlrId.jName, devId)
                .put(Protocol.PCarTelemetry.Id.jName, id)
                .put(Protocol.PCarTelemetry.Track.jName, trackId)
                .put(Protocol.PCarTelemetry.Lap.jName, lap)
                .put(Protocol.PCarTelemetry.RPos.jName, rPos);

                json.put(Protocol.PCarTelemetry.jName, carPos);
              **/  
             
              json.put(Protocol.PCarTelemetry.OlrId.jName, devId)
                  .put(Protocol.PCarTelemetry.Id.jName, id)
                  .put(Protocol.PCarTelemetry.Track.jName, trackId)
                  .put(Protocol.PCarTelemetry.Lap.jName, lap)
                  .put(Protocol.PCarTelemetry.RPos.jName, rPos)
                  .put(Protocol.PCarTelemetry.RPos.jName, batt);
                
              return(null);  
            }    
          }
          
    } // channels  
  } // network

} // Protocol
