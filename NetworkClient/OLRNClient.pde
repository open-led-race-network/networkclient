
public class OLRNClient extends MQTTClient {
  
  Device dev  = null;
  Race   race = null;
  Car    car  = null;
    
  // Inner Class: Channel Set
  //   Topics where the OLRNClient will publish PDUs
  class ChannelSet {
    public final String UNDEFINED = "-";

    String deviceStatus          = UNDEFINED ;
    String raceStatus            = UNDEFINED;
    String raceParticipantStatus = UNDEFINED;
    String raceConfig            = UNDEFINED;
    String raceCar               = UNDEFINED;
    String raceTelemetry         = UNDEFINED;
  }
  ChannelSet channel;

  private Trace t;
  private static final String _CLASS_ = "OLRNClient";

  /**
   * Creates a ORLNetworkDevice object
   * The object will be publishing messages for the NetworkDevice
   * whose instantiates it, identified by (id,sname,name) params
   *
   * @param parent Parent Processing PApplet 
   * @param id Device id of the caller
   * @param sname Device sName of the caller
   * @param name Device name of the caller
   */
  public OLRNClient(PApplet parent, MQTTListener listener,String id, String sname, String name) {
    super(parent,listener);
    t = new Trace(Trace.NONE);
    t.setPublisherId(id);
    t.setClassId(_CLASS_);

    channel = new ChannelSet();
    
    // Instantiates Device, Race, etc objects used in 
    // publishXxxx() methods to avoid instantiating it
    // on every call
    
    this.dev = new Device();
    this.dev._id     = id;
    this.dev._sName  = sname;
    this.dev._name   = name;
    this.dev._status = Protocol.PDevice.Status.UNDEFINED;
    
    this.race = new Race();
    this.car  = new Car();

  }
  
  // Patch:
  // when client is NOT CONNECTED and the user ends the program,
  // this would throw an exceprion.
  // The following override MQTTclient.dispose():
  //    disconnect() will be managed in NetworkDevice.exit()
  @Override
  public void dispose() {
    //disconnect();
  }

  void setDeviceStatusChannel(String str){
    this.channel.deviceStatus = str;
  }
  void setRaceStatusChannel(String str){
    this.channel.raceStatus = str;
  }
  void setRaceParticipantStatusChannel(String str){
    this.channel.raceParticipantStatus = str;
  }
  void resetRaceParticipantStatusChannel(){
    this.channel.raceParticipantStatus = channel.UNDEFINED;
  }
  
  void setRaceConfigChannel(String str){
    this.channel.raceConfig = str;
  }
  void resetRaceConfigChannel(){
    this.channel.raceConfig = channel.UNDEFINED;
  }

  void setRaceCarChannel(String str){
    this.channel.raceCar = str;
  }
  
  void setRaceTelemetryChannel(String str){
    this.channel.raceTelemetry = str;
  }
  void resetRaceTelemetryChannel(){
    this.channel.raceTelemetry = channel.UNDEFINED;
  }
  
  
  void setTraceLevel (String levStr) {
    if(levStr.equals("-") ){
      t.setLevel(Trace.NONE);
    } else {
      t.setLevel(levStr);
    }
  }
  


  /**
   * set the LWT message for this client to manage (Offline/Online) status
   *  >>> This method have to be called BEFORE connect() <<<
   */  
  private void setLWT(){
    t.log(Trace.METHOD + Trace.DETAILED, String.format("setLWT()\n"));
    
    this.dev._status = Protocol.PDevice.Status.OFFLINE ;

    Protocol.Network.PDU pdu = new Protocol.Network.PDU();
    String errStr = Protocol.Network.Channel.DeviceStatus.encodePDUPayload(dev, pdu );
    if(errStr != null) {
      t.error(String.format("setLWT() ERROR:%s", errStr));
      return;
    }  
    setWill( this.channel.deviceStatus, pdu.payload, Protocol.MQTT_QOS.AT_LEAST_ONCE,  true);
  }
                                              ///////////////////////////
                                              // Device Status Channel //
                                              ///////////////////////////
  /**
   * Publish ON the [Device Status] Channel (Topic) 
   * @param status The status to publish for the client
   */  
  private void publishDeviceStatus(String status){
    t.log(Trace.METHOD + Trace.DETAILED, String.format("publishDeviceStatus(%s:%s)\n",status, Protocol.PDevice.status.decode(status)));

    // set field in local object
    this.dev._status = status;
    
    // creates an empty PDU and calls encodePDUPayload() to fill pdu.paylead field
    Protocol.Network.PDU pdu = new Protocol.Network.PDU();
    String errStr = Protocol.Network.Channel.DeviceStatus.encodePDUPayload(dev, pdu );
    if(errStr != null) {
      t.error(String.format("publishDeviceStatus(%s):%s",status ,errStr));
      return;
    }            
    // fill others PDU fields and  Publish it to OLRBetwork 
    pdu.channel = this.channel.deviceStatus;
    pdu.qos=Protocol.MQTT_QOS.AT_LEAST_ONCE;
    pdu.isRetained=true;
    pub(pdu);
    
    return;
  }
  /**
   * REMOVE Retained Message on [Device Status] Topic
   */  
  private void publishDeviceStatusRemoveRetained(){
    t.log(Trace.METHOD, String.format("publishDeviceStatusRemoveRetained()\n")); 
    pub(this.channel.deviceStatus, "", Protocol.MQTT_QOS.AT_LEAST_ONCE, true); // Pub empty payload on topic  
    return;  
  }
                                              /////////////////////////
                                              // Race Status Channel //
                                              /////////////////////////
  /**
   * Publish ON the [Race Status] Channel (Topic)
   * @param raceId   Race id 
   * @param raceName Race name   
   * @param status   Race status code   
   */  
  private void publishRaceStatus(String raceId, String raceName, String status){
    t.log(Trace.METHOD + Trace.DETAILED, String.format("publishRaceStatus(%s,%s,%s:%s)\n",raceId,raceName,status, Protocol.PRace.status.decode(status)));

    if(this.channel.raceStatus.equals(channel.UNDEFINED)){
      t.error(String.format("publishRaceStatus(%s,%s,%s) ERROR: raceStatus channel not set - call setRaceStatusChannel()\n", raceId,raceName,status));
      return;
    }
    
    // Set race obj fields
    race.setFields(raceId, raceName, status, this.dev._id);
    
    // creates an empty PDU and calls encodePDUPayload() to fill pdu.paylead field
    Protocol.Network.PDU pdu = new Protocol.Network.PDU();
    String errStr = Protocol.Network.Channel.RaceStatus.encodePDUPayload(race, pdu);
    if(errStr != null) {
      t.error(String.format("publishRaceStatus(%s,%s,%s):%s",raceId,raceName,status, errStr));
      return;
    }     
    // fill others PDU fields and  Publish it to OLRBetwork 
    String raceStatusTopicStr = this.channel.raceStatus.replaceAll("_RaceId_", raceId); // Create Topic String 
    pdu.channel=raceStatusTopicStr;
    pdu.qos=Protocol.MQTT_QOS.AT_LEAST_ONCE;
    pdu.isRetained=true;
    pub(pdu);

    return;
  }

  /**
   * REMOVE Retained Message on [Race Status] Topic
   * @param raceId   Race id 
   */  
  private void publishRaceStatusRemoveRetained(String raceId){
    if(this.channel.raceStatus.equals(channel.UNDEFINED)){
      t.error(String.format("publishRaceStatusRemoveRetained(%s) ERROR: raceStatus channel not set - call setRaceStatusChannel()\n",raceId));
      return;
    }
    t.log(Trace.METHOD, String.format("publishRaceStatusRemoveRetained(%s)\n",raceId));
    String raceStatusTopicStr = this.channel.raceStatus.replaceAll("_RaceId_", raceId); // Create Topic String 
    pub(raceStatusTopicStr, "", Protocol.MQTT_QOS.AT_LEAST_ONCE, true); // Pub empty payload on topic  
    return;  
  }

                                              ///////////////////////////////
                                              // Race Participants Channel //
                                              ///////////////////////////////
  
  /**
   * Publish ON the [RaceParticipants] Channel (Topic)
   * @param status status code of the Participant Device    
   */
  private void publishRaceParticipantStatus(String status) {
    t.log(Trace.METHOD + Trace.DETAILED, String.format("publishRaceParticipantStatus(%s:%s)\n", status, Protocol.PDevice.status.decode(status)));

    if(this.channel.raceParticipantStatus.equals(channel.UNDEFINED)){
      t.error(String.format("publishRaceParticipantStatus(%s) ERROR: raceStatus channel not set - call setRaceParticipantStatusChannel()\n",status));
      return;
    }

    // set field in local object
    this.dev._status = status;

    Protocol.Network.PDU pdu = new Protocol.Network.PDU();
    String errStr = Protocol.Network.Channel.RaceParticipantStatus.encodePDUPayload(dev, pdu );
    if(errStr != null) {
      t.error(String.format("publishRaceParticipantStatus(%s):%s",status,errStr));
      return;
    }            
    pdu.channel=this.channel.raceParticipantStatus;
    pdu.qos=Protocol.MQTT_QOS.AT_LEAST_ONCE;
    pdu.isRetained=true;
    pub(pdu);
    
    return;   
  }
  /**
   * REMOVE Retained Message on [Race Participants] Topic
   */  
  private void publishRaceParticipantStatusRemoveRetained(){
    t.log(Trace.METHOD, String.format("publishRaceParticipantStatusRemoveRetained()\n"));
    if(this.channel.raceParticipantStatus.equals(channel.UNDEFINED)){
      t.error(String.format("publishRaceParticipantStatusRemoveRetained() ERROR: raceStatus channel not set - call setRaceParticipantStatusChannel()\n"));
      return;
    }
    pub(this.channel.raceParticipantStatus, "", Protocol.MQTT_QOS.AT_LEAST_ONCE, true); // Pub empty payload on topic  
    return;  
  }
                                              /////////////////////////
                                              // Race Config Channel //
                                              /////////////////////////


  /**
   * Publish ON the [RaceConfig] Channel (Topic)
   * @param pdu PDU with pdu.payload already set    
   */
  private void publishRaceConfigParam(Protocol.Network.PDU pdu){
    t.log(Trace.METHOD + Trace.DETAILED, String.format("publishRaceConfigParam(PDU)\n"));

    if(this.channel.raceConfig.equals(channel.UNDEFINED)){
      t.error(String.format("publishRaceConfigParam(PDU) ERROR: raceConfig channel not set - call setRaceConfigChannel()\n"));
      return;
    }
   
    pdu.channel=this.channel.raceConfig;
    pdu.qos=Protocol.MQTT_QOS.AT_LEAST_ONCE;
    pdu.isRetained=true;
    pub(pdu);
    
    return;
  }
  
  /**
   * REMOVE Retained Message on [Race Config] Topic
   * @param raceId   Race id 
   */  
  private void publishRaceConfigParamRemoveRetained(){
    t.log(Trace.METHOD + Trace.DETAILED, String.format("publishRaceConfigParamRemoveRetained()\n"));
    if(this.channel.raceConfig.equals(channel.UNDEFINED)){
      t.error(String.format("publishRaceConfigParamRemoveRetained() ERROR: raceConfig channel not set - call setRaceConfigChannel()\n"));
      return;
    }
    pub(this.channel.raceConfig, "", Protocol.MQTT_QOS.AT_LEAST_ONCE, true); // Pub empty payload on topic  
  }
  

                                              
                                              
                                              //////////////////////
                                              // Race Car Channel //
                                              //////////////////////
  /**
   * Publish ON the [RaceCar] Channel (Topic)
   * @param raceId   id of the race the car is participating to    
   * @param carId    id of the car 
   * @param carName  Name of the car
   * @param curDev   id of the OLR Device where the car is now
   * @param status   car status
   * NOTE:
   *    This method is invoked on RACE CREATION as well (i.e. BEFORE the NetworkDevice  
   *    partecipates to a race).
   *    The "channel topic string" for this channel (channel.raceCar) is the one loaded 
   *    from the  app configuration JSON file (it always contains the _RaceId_ placeholder.
   *    [this.channel.raceCar] example:
   *        "OLR/basePool/race/_RaceId_/Cars/_CarId_"
   */
  private void publishCarStatus(String raceId, int carId, String carName, String curDev, String status, int speed){
     t.log(Trace.METHOD + Trace.DETAILED, String.format("publishCarStatus(%s,%d,%s,%s,%s:%s,%d)\n", raceId,carId,carName,curDev,status,Protocol.PCar.status.decode(status),speed));
    if(this.channel.raceCar.equals(channel.UNDEFINED)){
      t.error(String.format("publishCarStatus(%s,%d,%s,%s,%s,%d) ERROR: raceCar channel not set - call setRaceCarChannel()\n", raceId, carId, carName, curDev, status,speed));
      return;
    }

    String payload = Protocol.Network.Channel.CarStatus.encodeTextPayload(carId, carName, curDev, status, speed);
    if(payload == null) {
      t.error(String.format("publishCarStatus(%s,%d,%s,%s,%s,%d): Error Encoding payload - See logfile\n",raceId,carId,carName,curDev,status,speed));
      return;
    }            

    //   set channelStr to "OLR/basePool/race/<raceId>/Cars/<carId>"
    String carChannelsStr = new String (this.channel.raceCar.replaceAll("_RaceId_", raceId).replaceAll("_CarId_", Integer.toString(carId)));
    pub(carChannelsStr, payload, Protocol.MQTT_QOS.AT_LEAST_ONCE, true); // Pub empty payload on topic
    
/***     
    car.setFields(carId, carName, curDev, status,speed);

    Protocol.Network.PDU pdu = new Protocol.Network.PDU();
    String errStr = Protocol.Network.Channel.CarStatus.encodePDUPayload(car, pdu);
    if(errStr != null) {
      t.error(String.format("publishCarStatus(%s,%d,%s,%s,%s,%d):%s",raceId,carId,carName,curDev,status,speed,errStr));
      return;
    }            

    //   set channelStr to "OLR/basePool/race/<raceId>/Cars/<carId>"
    String carChannelsStr = new String (this.channel.raceCar.replaceAll("_RaceId_", raceId).replaceAll("_CarId_", Integer.toString(carId)));
    
    pdu.channel=carChannelsStr;
    pdu.qos=Protocol.MQTT_QOS.AT_LEAST_ONCE;
    pdu.isRetained=true;
    pub(pdu);
***/    
    
  }

  /**
   * REMOVE Retained Message on [Race Car] Topic
   * @param raceId  Race id 
   * @param carId   Car id 
   */  
  private void publishCarStatusRemoveRetained(String raceId, int carId){
     t.log(Trace.METHOD + Trace.DETAILED, String.format("publishCarStatusRemoveRetained(%s,%d)\n", raceId,carId));
    if(this.channel.raceCar.equals(channel.UNDEFINED)){
      t.error(String.format("publishCarStatusRemoveRetained(%s,%d) ERROR: raceCar channel not set - call setRaceCarChannel()\n", raceId, carId));
      return;
    }
    if(raceId == null){
      return; // Patch on Progra Close after a race
    }
    //   set channelStr to "OLR/basePool/race/<raceId>/Cars/<carId>"
    String carChannelsStr = new String (this.channel.raceCar.replaceAll("_RaceId_", raceId).replaceAll("_CarId_", Integer.toString(carId)));
    pub(carChannelsStr, "", Protocol.MQTT_QOS.AT_LEAST_ONCE, true); // Pub empty payload on topic
  }
  
  
  
                                              ////////////////////////////
                                              // Race Telemetry Channel //
                                              ////////////////////////////
  /**
   * Publish on the [RaceTelemetry] Channel (Topic)
   * @param carId   id of the car 
   * @param trackId id of the current track (Main, Box, etc) in the RaceDevice
   * @param lap     lap number in the RaceDevice
   * @param rPos    relative position (%) in the current lap
   */
  private void publishCarTelemetry(int carId, String trackId, int lap, int rPos, int batt){
    t.log(Trace.METHOD + Trace.DETAILED, String.format("publishCarTelemetry(%d,%s,%d,%d)\n", carId,trackId,lap,rPos));

    JSONObject carTelemetry = new JSONObject();

    String errStr = Protocol.Network.Channel.RaceTelemetry.encodeJSONPayload(this.dev._id,carId,trackId,lap,rPos, batt, carTelemetry);
    if(errStr != null) {
      t.error(String.format("publishCarTelemetry(%d,%s,%d,%d,%d)):%s", carId,trackId,lap,rPos,batt, errStr));
      return;
    }            

    pub(this.channel.raceTelemetry, carTelemetry.toString(), Protocol.MQTT_QOS.AT_LEAST_ONCE, false);

  }
                                              /////////////////////
                                              // MQTT Pub / Sub  //
                                              /////////////////////
  /**
   *  PUBLISH PDU on topic
   */
    void pub(Protocol.Network.PDU pdu){
      t.log(Trace.LOWLEVEL, String.format("pub(PDU): PUBLISH on Topic[%s] QoS[%d] Retained[%b] Payload[%s]\n", pdu.channel,pdu.qos,pdu.isRetained,pdu.payload));
      publish(pdu.channel, pdu.payload, pdu.qos, pdu.isRetained);
    }
  /**
   *  PUBLISH on topic
   */
    void pub(String topic, String payload, int qos, boolean retained) {
      t.log(Trace.LOWLEVEL, String.format("pub(): PUBLISH on Topic[%s] QoS[%d] Retained[%b] Payload[%s]\n",topic,qos,retained,payload));
      publish(topic, payload, qos, retained);
    }

  /**
   *  SUBSCRIBE to topic
   */
    void sub(String topic) {
      t.log(Trace.DETAILED, String.format("sub(): SUBSCRIBE to Topic[%s]\n", topic));
      subscribe(topic);
    }
  
  /**
   *  SUBSCRIBE to topic with specified QoS
   */
    void sub(String topic, int qos) {
      t.log(Trace.DETAILED, String.format("sub(): SUBSCRIBE to Topic[%s] QoS[%d]\n", topic, qos));
      subscribe(topic, qos);
    }
  
  /**
   * UNSUBSCRIBe to topic
   */
    void unsub(String topic) {
      t.log(Trace.DETAILED, String.format("unsub(): UNSUBSCRIBE to Topic[%s]\n", topic));
      unsubscribe(topic);
    }


}
