class Device extends OLRNBaseItem {
  public String _sName;

  Device ()   {
    super ();
    this._sName = null ;
  }

  Device(String id,String name,String status){
    super (id, name, status);
    this._sName = "-" ;
  }  

  Device (String id, String shortname, String name, String status)   {
    super (id, name, status);
    this._sName = shortname ;
  }

  void update(String sname, String name, String status) {
    this._sName  = sname;
    this._name   = name;
    this._status = status;
    this._tstamp = LocalDateTime.now().toString(); 
  }
  
  /**
   *  used when the obj was created with empty fields (Device() constructor)
   */
  void setFields(String id, String sname, String name, String status) {
    super.setFields(id,name,status);
    this._sName  = sname;
  }
  
} 



/**
 *  List of OLRNBaseItem
 *    OLRNBaseItem.id Attribute is UNIQUE and used to access
 *    list items
 */
class DeviceList extends Device {
   
  private ArrayList<Device> itemList;
  public long lastUpdate = 0;  // Uses public static long currentTimeMillis() 
  
  // Constructor
  DeviceList() {
    super("","","");
    itemList = new ArrayList<Device>();
    setLastUpdateTime();
    
  }

  void setLastUpdateTime(){
    lastUpdate = System.currentTimeMillis(); 
  }


  // Add item 

  boolean add (String id, String sname, String name, String status){
    if( ! chkParams("DeviceList.add", id, name, status)){return false;}
    // Check if item(id) already exist
    if( exist(id)){
      System.out.printf("DeviceList.add() ERROR - id=[%s] exist\n",id);
      return false;    
    }
    if( sname == null || sname.length() == 0){
      sname = id;
    }
    Device item = new Device (id, sname, name, status);
    itemList.add(item);
    setLastUpdateTime();
    return true;
  }
  
  // Update item 
  boolean update (String id, String sname, String name, String status){
    boolean updateStatusOnly = false;
    if(! chkParams("DeviceList.update", id, status)){return false;}
    if( name.equals("-")){
      updateStatusOnly = true; 
    } 
    int idx = indexOf(id); 
    if( idx < 0){ 
      System.out.printf("DeviceList.update() ERROR - id=[%s] DOES NOT exist\n", id);
      return(false);  
    } 
    if(updateStatusOnly) {
      itemList.get(idx).updStatus(status);
    } else {
      itemList.get(idx).update(sname, name, status);
    }
    setLastUpdateTime();
    return true;
  }  
  
  boolean updateStatus (String id, String status){
    return(update (id, "-", "-", status));
  } 

  boolean updateOrAdd (String id, String status){
    return(updateOrAdd (id, "-", "-", status));
  } 

  // Add/Update item 
  boolean updateOrAdd (String id, String sname, String name, String status){
    if(! chkParams("DeviceList.updateOrAdd", id, status)){return false;}
    if( ! exist(id)) {  // ID not found ---> Create new entry
      if(name.equals("-")) {
        System.out.printf("DeviceList.updateOrAdd(%s) ERROR : Name=[-] on ADD operation\n");
        return false;
      }
      this.add(id, sname, name, status);      
    } else {
      this.update(id, sname, name, status);
    }  
    setLastUpdateTime();    
    return true;
  }

  boolean updateOrAdd_OLD (String id, String status){
    return(updateOrAdd (id, "-", "-", status));
  } 



  /**
    Remove item
  **/  
  boolean delete (String id){

    if(! chkParams("DeviceList.delete", id)){return false;}
    int idx = indexOf(id); 
    if( idx < 0){    // ID not found 
      System.out.printf("DeviceList.delete() ERROR - Race=[%s] DOES NOT exist\n", id);
      return(false);  
    } 
    itemList.remove(idx);
    setLastUpdateTime();
    return(true);
  }

  /**
   * Return timestamp of the Last Update Operation 
   */
  public long getUpdateTstamp() {
    return(this.lastUpdate); 
  }

  public void setUpdateTstamp(long l) {
    this.lastUpdate = l; 
  }
        
  public int indexOf(String id)  {
    // Returns -1 on NOT FOUND
    int n = itemList.size();
    for (int i=0; i<n ; i++) {
      if (itemList.get(i)._id.equals(id.trim())){
           return(i);
      }
    }
    return(-1); 
  }  
  
  public boolean exist(String id) {
    int idx = indexOf(id); 
    if( idx >= 0){    // ID found
      return(true);
    }
    return(false);
  }  

  /**
   * Return name attribute for item(id) 
   */
  public String getShortName(String id) {
    int idx = indexOf(id);  
    if( idx < 0){    // ID not found
        return(null);
    }  
    return(itemList.get(idx)._sName);
  }
  
  /**
   * Return name attribute for item(id) 
   */
  public String getName(String id) {
    int idx = indexOf(id);  
    if( idx < 0){    // ID not found
        return(null);
    }  
    return(itemList.get(idx)._name);
  }

  /**
  *  Get Status for item(id)
  *  
  * @return  Status: if a Device[id] exist
  * @return  "-" : On NOT FOUND
  */
  String getStatus(String id){
    
    int idx = indexOf(id); 
    if( idx < 0){    // ID not found
      return("-");
    }
    return(itemList.get(idx)._status);
  }

  /**
   * Return tstamp attribute for item(id)
   */
  public String getTStamp(String id) {
    int idx = indexOf(id);  
    if( idx < 0){    // ID not found
        return(null);
    }  
    return(itemList.get(idx)._tstamp);
  }

  /**
    * @param String id   
    *
    * @return String[]: {id, shortName, name, status, tstamp }
    * @return null:     id not found
  **/      
  public String[] getAttributes(String id)  {  
    if( this.exist(id)) {
      return(new String[] {id,getShortName(id),getName(id),getStatus(id), getTStamp(id)});    
    }  
    return(null);
  }
    
  public Device get(String id)  {
    // Returns -1 on NOT FOUND
    int n = itemList.size();
    for (int i=0; i<n ; i++) {
      Device item = itemList.get(i);
      if (item._id.equals(id.trim())){
           return(item);
      }
    }
    return(null); 
  }  


  /**
   * get List Content 
   *
   * @return String[n][5] [n]:Items / [5]:{id, shortName, name, status, tstamp}
   */
  String[][] getFullContent(){
    int n = itemList.size();
    String[][] l = new String[n][6];
    Device item;
    for (int i=0; i<n ; i++) {
      item=itemList.get(i);
      l[i][0]=item._id.trim();  
      l[i][1]=item._sName.trim();
      l[i][2]=item._name.trim();
      l[i][3]=item._status.trim();
      l[i][4]=item._tstamp.trim();
    }
    return(l);
  }
  
  /**
   *
   */
  int getActiveNum(){
    int n = itemList.size();
    int n1=0;
    for (int i=0; i<n ; i++) {
      if( ! itemList.get(i)._status.equals(Protocol.PDevice.Status.OFFLINE) ){
        n1++;
      }
    }
    return(n1);
  }
  
  
  /**
   * get List Content 
   *
   * @return String[n][5] [n]:Items / [5]:{id, shortName, name, status, tstamp}
   */
  String[][] getContent(){
    int n = itemList.size();
    int m = getActiveNum();
    String[][] l = new String[m][6];
    Device item;
    int j=0;
    for (int i=0; i<n ; i++) {
      item=itemList.get(i);
      if( ! item._status.equals(Protocol.PDevice.Status.OFFLINE) ){
        l[j][0]=item._id.trim();  
        l[j][1]=item._sName.trim();
        l[j][2]=item._name.trim();
        l[j][3]=item._status.trim();
        l[j][4]=item._tstamp.trim();
        j++;
      }   
    }
    return(l);
  }
  

  
  String toString(){
    String s="Device List:\n";
    String[][] l = getContent();
    for(int i=0 ; i<l.length;i++ ) {
      s = String.format("%s  [%s][%s][%s][%s][%s]\n", s, l[i][0], l[i][1], l[i][2], l[i][3], l[i][4]); 
    }
    return(s);
  }
  
} 
