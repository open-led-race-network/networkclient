class RaceConfig implements Comparable<RaceConfig> {
  String olrId;   // OLR Id
  int    pos;     // Position in race - as received in cfg  
  int    laps;    // Laps number - as received in cfg
  int    repeat;  // Repeat number - as received in cfg
  int    start;   // Race Starts here  
  int    finish;  // Race Finish here

    RaceConfig(){
    this.olrId=null;
    this.pos=-1;
    this.laps=-1;
    this.repeat=-1;
    this.start=0;
    this.finish=0;
  } 

  RaceConfig(String devId){
    this.olrId=devId;
    this.pos=-1;
    this.laps=-1;
    this.repeat=-1;
    this.start=0;
    this.finish=0;
  } 


  RaceConfig(String devId, int p, int l, int r){
    this.olrId=devId;
    this.pos=p;
    this.laps=l;
    this.repeat=r;
    this.start=0;
    this.finish=0;
  } 
  
  
  void setPos(int p){
    this.pos=p;
  }
  
  void setLaps(int l){
    this.laps=l;
  }
  void setRepeat(int r){
    this.repeat=r;
  }
  
  public String toString() {
     return "[olrId:" + olrId + ", pos:" + pos + ", laps:" + laps + ", repeat:" + repeat + ", start:" + start + ", finish:" + finish + "]";
  }
  @Override
  public int compareTo(RaceConfig o) {
     return Integer.valueOf(pos).compareTo(o.pos);
  }
  
}



class RaceConfigList  {
  
  RaceConfig cfgList[]; 
  int  idx;

  RaceConfigList(){
    cfgList = null;
    this.idx=0;
  }

  RaceConfigList(int size){
    cfgList = new RaceConfig[size];
    this.idx=0;
  }
  
  void initSize(int size){
    cfgList = new RaceConfig[size];
    for(int i=0; i<size;i++){
      cfgList[i]= new RaceConfig();
    }
  }
  
  
  void add(RaceConfig rec){
    if(idx >= cfgList.length){
      System.err.printf("RaceConfigList.add() ERROR: Out Of Range");
      return;
    }
    cfgList[idx]=rec;
    idx++;
  }

  void add(String devId, int p, int l, int r){
    if(idx >= cfgList.length){
      System.err.printf("RaceConfigList.add() ERROR: Out Of Range");
      return;
    }
    cfgList[idx].olrId=devId;
    cfgList[idx].pos=p;
    cfgList[idx].laps=l;
    cfgList[idx].repeat=r;

    idx++;
  }



  int size(){
    return(cfgList.length);
  }
  
  /**
   *  process the array data, previusly inserted via add():
   *    SORT Array
   *    Find start and finish OLR and set values in corresponding records 
   */
  boolean process(){
    
    int tMin=1000,tMax=0;    
    for (int i = 0; i < cfgList.length; i++) {
       if(cfgList[i].repeat < tMin){
         tMin=cfgList[i].repeat;
       }
       if(cfgList[i].repeat > tMax){
         tMax=cfgList[i].repeat;
       }
     }
     if(tMax-tMin > 1){
        System.err.printf("RaceConfigList.process() ERROR - Repeat parameters error min:[%d] max[%d] DIFFERS more than 1 unit\n",tMin,tMax);
        return(false);
     }    
    
     // Sort by Pos
     Arrays.sort(cfgList);
    
     // set startHere in the first OLR
     cfgList[0].start=1;
      
     // set finishtHere
     boolean found = false;
     if(tMax == tMin){  // every OLR have the same repeat param
       cfgList[cfgList.length -1].finish=1; // Finish Line in the Last OLR in the list
       found=true;
     } else {
       // Look for finish line:
       //   Finish line is the OLR before "repeat" param decrease
       for(int i=0; i<cfgList.length -1;i++) {
//        if(cfgList[i].repeat < cfgList[i+1].pos ){
        if(cfgList[i].repeat < cfgList[i+1].repeat ){
          System.err.printf("RaceConfigList.process() ERROR - Repeat parameters error on index:[%d]\n",i);
          return(false);
        }
        if(cfgList[i].repeat > cfgList[i+1].repeat ){
          cfgList[i].finish=1;
          found=true;
        } 
      }
     } 
     if(! found){
       System.err.print("RaceConfigList.process() - SOFTWARE ERROR: RaceConfigList.process() - Unmanaged Parameters SET Configuration\n");
       return(false);
     }
     
    return(true);
  } 
  
  /**
   * Find item into the array identified by olrId param 
   *
   * @param olrId 
   *
   * @return index of the array for the orlId
   * @return -1 on olrId not found
   */
  int getIdx(String olrId) {
    for(int i=0; i<cfgList.length;i++) {
      if(cfgList[i].olrId.equals(olrId)){
        return(i);
      } 
    }
    return(-1);
  }
  
  RaceConfig getRaceConfig(String olrId){
    int idx = getIdx(olrId);
    if(idx == -1){
      return(null);
    }
    return(cfgList[idx]);
  }
  
  RaceConfig getRaceConfig(int idx){
    if(idx <0 || idx >= cfgList.length ){
      return(null);
    }
    return(cfgList[idx]);
  }

  /**
   *  Get the OLR following the one passed as a param
   *  in the race order
   */
  RaceConfig getNext(String olrId){
    int idx = getIdx(olrId);
    if(idx == -1){
      return(null);
    }
    idx++;
    if(idx >=  (cfgList.length)){
      idx=0;
    }
    return(cfgList[idx]);
  }
  
  
  
  String toString(){
    String str="";
    for(int i=0; i<cfgList.length;i++) {
      str = String.format("%sidx[%d]: %s\n", str, i,cfgList[i].toString() ); 
    }
    return(str);
  }
  
  
}
