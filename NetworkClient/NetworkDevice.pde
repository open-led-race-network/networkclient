/**
 * Class NetworkDevice - Client for the Open Led Race Network
 * 
 *  This class implements everything is needed to "Participate" the OpenLedRace Network.
 *
 *  The NetworkDevice manage internally everything related to the OLRN Network.
 *  It "extends" Device (a NetworkDevice have it's own _id, _name, _status)
 *
 *  Class COMPOSITION: 
 *  The NetworkDevice Obj instantiate internally, on creation:
 *
 *    - An OLRClient used to Send/Receive message (extends MQTTClient) 
 *        this.mqttClientId (taken fron JSON Config: "NetworkDevice.MQTT.ClientId")
 *        is the ID used for the MQTT connection.
 *
 *    - Device List: 
 *        List of currently "Active" Devices in the OLR Network.
 *        Updated on reception of valid OLRN(MQTT) messages
 *
 *    - Race List: 
 *        List of currently "Active" Races  in the OLR Network.
 *        Updated on reception of valid OLRN(MQTT) messages
 *
 *    - RaceDevice: 
 *        An instance of the oject representing the Local OLR hardware
 *        implements Serial Communication with the OLR Phisical Device
 *        
 *  VERY IMPORTANT  -  NO NETWORK NO PARTY
 *  --------------  -  -------------------
 *      Every NetworkDevice attribute (Device List, Race List, Status, etc)
 *      is updated internally ON RECEPTION of a MQTT  message containing 
 *      a VALID OLRNetwork Payload.
 *      The NetworkDevice DOES NOT implements ANY method that allows to change
 *      directly these Attributes.
 *
 *      Examples:
 *        A LOCAL USER CREATES, via the UI, a new Race.
 *        This software calls NetworkDevice.createRace() that PUBLISH the 
 *        specific OLRNetwork Message (in a specific Channel)
 *        NO MODIFICATION IS DONE AT THIS POINT to the RaceList (no 'record' 
 *        added locally).
 *        The Race List will be UPDATED only when the message just published 
 *        is RECEIVED by the Client who sent it (and by any other client on the Network) 
 *
 *        Methods like "NetworkDevice.publishDeviceStatus()" DOES NOT store the
 *        Status the user just request to SEND.
 *      ` The new Status is SENT and, on reception of the Message, Status Attribute 
 *        will be updated.
 *
 *  Incoming Network Messages (Received MQTT messages)
 *  --------------------------------------------------
 *    Incoming messages 'arriving' in the Callback gets
 *    into a QUEUE for later processing.
 *      This is done to don't lose messages coming at a fast rate.
 *      It happens, for example for messages SENT in the same method, 
 *      one right after the other.
 *      Not using a queue, if the Software is still processing
 *      the previous 'callback', the arriving message get lost.
 *
 */ 
import mqttmod.*;

public class NetworkDevice extends Device {

  private boolean serialLastConnectionStatus;
  private boolean sendTelemetry;  

  public RaceDevice phRaceDev;  

  public boolean isConnected=false;
  public boolean isConnecting=false;
  public boolean isOnline=false;
  private double lastConnectAttempt;  // System.currentTimeMillis()
  
  private final static long TIMEOUT = 2000;
   
  public OLRNClient client;
  
  public DeviceList deviceList;
  public RaceList raceList;
  
  public Location location;  

  public RaceConfigList raceOrder = null;

  // Queue for MQTT messages received 
  public  Deque<NetworkMessage> recvMessage_Queue;

  // QUEUE for "SUBSCRIPTION to topic" request
  public  Deque<SubscriptionRequest> subRequest_Queue;
  
  // Object used in manageReceivedXxx() methods 
  // These object are Instance Variables to avoid overhead
  // of creating them each time a message is received.
  Device   recvDevice ;
  Race     recvRace   ;
  Car      recvCar    ;
  
  private String mqttServer ;
  private String mqttClientId ; 
  private String mqttUser ;
  private String mqttPass ;

  private PApplet p;
  private String MqttURI;
  
  private Trace t;
  private static final String _CLASS_ = "NetworkDevice";
  
  /**
   *  Inner class "Callback" 
   *    Implements Callbacks for MQTT client
   */
  class Callback implements MQTTListener {  
    void clientConnected() {
      System.out.printf(">>> Client[%s] connected\n", mqttClientId);
      isConnecting = false;
      isConnected = true;
    }  
    
    void messageReceived(String topic, MqttMessage message) {
        boolean ok = recvMessage_Queue.offer(new NetworkMessage(topic,message));
        if(! ok) {
          System.err.print("Callback()->messageReceived() ERROR: recvMessage_Queue FULL - offer() returns FALSE\n");
        }
        t.log(Trace.LOWLEVEL, String.format("Callback()->messageReceived() on [%s]: added to Queue\n", topic));
      
    }
    
    void connectionLost() {
      System.err.printf(">>> Client[%s] connection lost",mqttClientId);
      isConnected=false;
      isOnline=false;
      isConnecting = false;
    }
  } 
  Callback callback;
  
  /**
   *  Inner class "DeviceTopicSet" 
   *  Variables holding Device-related Topics Set
   */
  class DeviceTopicSet {
    String baseString ;
    String statusSubscribeString ;
    Topic  status;
    Topic  recv;
    Topic  broadcast;
    DeviceTopicSet(){
      this.baseString = null;
      this.statusSubscribeString = null;
      this.status = new Topic();
      this.recv = new Topic();
      this.broadcast = new Topic();
    }
  }
  DeviceTopicSet deviceTopicSet;

  /**
   *  Inner class "RaceTopicSet" 
   *  Variables holding Race-related Topics Set
   */
  class RaceTopicSet {
    String  baseString ;
    String  statusSubscribeString ;
    Topic   status;
    Topic   participants;
    Topic   telemetry;
    Topic   config; 
    Topic   cars;
    RaceTopicSet(){
      this.baseString = null;
      this.statusSubscribeString = null;
      this.status = new Topic();
      this.participants = new Topic();
      this.telemetry = new Topic();
      this.config = new Topic(); 
      this.cars = new Topic();
    }
  }
  RaceTopicSet raceTopicSet;

  /**
   *  Inner class "Topic" 
   *  Fields for DeviceTopicSet,RaceTopicSet
   */
  class Topic {  
    String topic ;
    String id;
  }

  /**
   *  Inner class "MyRace"
   *  Variables for the race this NetworkDevice Client is currently subscribed to
   */
  class MyRace {
    String id;                   // Race.id of the race where the client is subscribed    
    String name;                 // Race.name    
    String participantsTopicStr; // "OLR/basePool/race/<RaceId>/Participants/<TrackId>" 
    String telemetryTopicStr;    // "OLR/basePool/race/<RaceId>/Telemetry"
    String configTopicStr;       // "OLR/basePool/race/<RaceId>/Config" 
    String carsTopicStr;         // "OLR/basePool/race/_RaceId_/Cars/_CarId_"  
    boolean startsHere;          
    
    MyRace(){
      reset();
    }
    void reset(){
      this.id = null;                   
      this.name = null;                 
      this.participantsTopicStr = null; 
      this.telemetryTopicStr = null;    
      this.configTopicStr = null;       
      this.carsTopicStr = null; 
      this.startsHere = false; 
    }
    boolean isValid(){
      if(this.id != null){
        return(true);
      }
      return(false);
    }
  }
  MyRace myRace;

  
  /**
  * Constructor - Load cfg from JSON obj
  */
  public NetworkDevice(PApplet p, String id, String sname, String name, JSONObject json) {
    super (id, sname, name, Protocol.PDevice.Status.OFFLINE);    

    // Set logLevel for NetworkDevice object
    JSONObject logLevel = getJSONObjectOrExit(json, "LogLevel");
    String logStr   = trim(logLevel.getString("NetworkDevice" , "-"));    
    if(logStr.equals("-") ){
      t = new Trace(Trace.NONE);
    } else {
      t = new Trace(logStr);
    }
    //t.setPublisherId(this._sName);
    t.setPublisherId(id);
    t.setClassId(_CLASS_);
    t.log(Trace.METHOD, String.format("Constructor(%s, %s, %s)\n", id, sname, name ));

    StringBuilder uri = null;

    this.p = p;
    
    this.lastConnectAttempt = 0 ;  // System.currentTimeMillis()

    deviceList = new DeviceList();
    raceList  = new RaceList ();
    JSONObject pd = getJSONObjectOrExit(json, "RaceDevice");
    phRaceDev=new RaceDevice(this._sName, pd, trim(logLevel.getString("RaceDevice" , "-")));
    
    serialLastConnectionStatus=false;    

    location  = new Location();
    
    recvMessage_Queue = new ArrayDeque<NetworkMessage>();
    subRequest_Queue = new ArrayDeque<SubscriptionRequest>();

    this.raceTopicSet = new RaceTopicSet() ;
    this.deviceTopicSet = new DeviceTopicSet() ;
    
    this.recvDevice = new Device();
    this.recvRace   = new Race();
    this.recvCar    = new Car();

    procConstructorJParam(json);

    myRace = new MyRace();  
      
    if(this.mqttUser.length() == 0 ){   // No User/Pass supplied in Json cfg file
      // uri = new StringBuilder("mqtt://" + this.mqttServer);  // Connection URI
       uri = new StringBuilder("tcp://" + this.mqttServer);  // Connection URI
    } else {
      //uri = new StringBuilder("mqtt://"  + this.mqttUser + ":" + this.mqttPass + "@" + this.mqttServer);  // Connection URI
      uri = new StringBuilder("tcp://"  + this.mqttUser + ":" + this.mqttPass + "@" + this.mqttServer);  // Connection URI
    }  
    
    this.MqttURI = uri.toString();
    t.log(Trace.DETAILED, String.format("new MQTT Client URI:[%s]\n", uri ));

    // Create MQTT client - Set callbacks  for this client 
    callback = new Callback();
    client = new OLRNClient(this.p , callback, this._id , this._sName, this._name);
    client.setTraceLevel(trim(logLevel.getString("OLRNClient" , "-")));
    client.setDeviceStatusChannel(this.deviceTopicSet.status.topic);
    client.setRaceStatusChannel(this.raceTopicSet.status.topic);
    client.setRaceCarChannel(this.raceTopicSet.cars.topic);

    client.setLWT();  // Set Last Will and Testament message for this client
  }




  /*
  *  Process JSON Constructor's parameter  
  */
  //private void  procConstructorJParam(String port, String portProperties, JSONObject json) {    
  private void  procConstructorJParam(JSONObject json) {
    
    // sendTelemetry (false|true)
    this.sendTelemetry = json.getBoolean("sendTelemetry" , false);
    
    // Location Data
    JSONObject loc = getJSONObjectOrExit(json, "Location"); 
    location.address = trim(loc.getString("address" , "-"));
    location.locality = trim(loc.getString("locality" , "-"));
    location.city = trim(loc.getString("city" , "-"));
    location.state = trim(loc.getString("state" , "-"));
    location.zip = trim(loc.getString("zip" , "-"));
    location.country = trim(loc.getString("country" , "-"));
    location.countryCode = trim(loc.getString("countryCode" , "-"));
    location.lat = trim(loc.getString("lat" , "-"));
    location.lon = trim(loc.getString("lon" , "-"));

    // MQTT Broker Connection Data
    JSONObject mqtt = getJSONObjectOrExit(json, "MQTT"); 
    this.mqttServer = getJSONStringOrExit(mqtt, "server");
    this.mqttClientId = getJSONStringOrExit(mqtt, "clientId");
    this.mqttUser = getJSONStringOrExit(mqtt, "user");
    this.mqttPass = getJSONStringOrExit(mqtt, "pass");

    // Topics Data
    // - Set values for Topic.subscribeBaseString / calc once and used on every received message
    // - Read cfg values and REPLACE "_Id_" placeholder with the ID of the current Track
    //   NOTE:
    //    For RACE topics, the field still contains the "_RaceId_" placeholder.
    //    It will be substitutes on Race Creation.
    /////////////////////////////////////////////////////////////////////////////////////////
    JSONObject topic = getJSONObjectOrExit(json, "Topic");
    JSONObject tDevice = getJSONObjectOrExit(topic, "device");
    JSONObject tRace = getJSONObjectOrExit(topic, "race");

    int checkPlaceholder;
    String fileValue;
    String subTopicIdString;
    
    // "Topic.device" section
    /////////////////////////
    this.deviceTopicSet.baseString = getJSONStringOrExit(tDevice,"base"); // Ex: "base" : "OLR/basePool/device/",
    
    // Topic.device.status    
    fileValue = this.deviceTopicSet.baseString.concat(getJSONStringOrExit(tDevice,"status")); // ex: "OLR/basePool/device/status/_Id_"
    checkPlaceholder = FindStringOrExit(fileValue , "_Id_" );
    this.deviceTopicSet.status.topic = fileValue.replaceAll("_Id_", this._id) ;               // ex: "OLR/basePool/device/status/HAM-00102833922"                                            
    this.deviceTopicSet.statusSubscribeString = fileValue.replaceAll("_Id_", "");       // ex: "OLR/basePool/device/status/"
    
    // Topic.device.recv    
    fileValue = this.deviceTopicSet.baseString.concat(getJSONStringOrExit(tDevice,"recv")); // ex: "OLR/basePool/device/Recv/_Id_"
    checkPlaceholder = FindStringOrExit(fileValue , "_Id_" );
    this.deviceTopicSet.recv.topic = fileValue.replaceAll("_Id_", this._id) ;               // ex: "OLR/basePool/device/Recv/HAM-00102833922"                                            
    
    // Topic.device.broadcast    
    fileValue = this.deviceTopicSet.baseString.concat(getJSONStringOrExit(tDevice,"broadcast")); // ex: "OLR/basePool/device/broadcast"
    this.deviceTopicSet.broadcast.topic = fileValue ;    
    //String devBcastStr = getJSONStringOrExit(tDevice,"broadcast");
    
    // "Topic.race" section
    ///////////////////////
    this.raceTopicSet.baseString = getJSONStringOrExit(tRace,"base"); // Ex: "base" : "OLR/basePool/race/",
    
    // Topic.race.status
    fileValue = this.raceTopicSet.baseString.concat(getJSONStringOrExit(tRace,"status")); // ex: "OLR/basePool/race/status/_RaceId_"
    checkPlaceholder = FindStringOrExit(fileValue , "_RaceId_" );
    this.raceTopicSet.status.topic = fileValue;                                            
    this.raceTopicSet.statusSubscribeString = fileValue.replaceAll("_RaceId_", "");       // ex: "OLR/basePool/race/status/"

    // Topic.race.participants
    fileValue = this.raceTopicSet.baseString.concat(getJSONStringOrExit(tRace,"participants")); // ex: "OLR/basePool/race/_RaceId_/_SubTopicIdString_/_Id_"
    checkPlaceholder = FindStringOrExit(fileValue , "_RaceId_" );
    checkPlaceholder = FindStringOrExit(fileValue , "_Id_" );
    checkPlaceholder = FindStringOrExit(fileValue , "_SubTopicIdString_" );
    subTopicIdString = getJSONStringOrExit(getJSONObjectOrExit(tRace,"subTopicIdString"),"participants"); // ex "Participants"
    String ParticipantCfgStr = fileValue.replaceAll("_SubTopicIdString_", subTopicIdString); // ex: "OLR/basePool/race/_RaceId_/Participants/_Id_"
    this.raceTopicSet.participants.topic = ParticipantCfgStr.replaceAll("_Id_", this._id); // ex: "OLR/basePool/race/_RaceId_/Participants/HAM-00102833922"
    this.raceTopicSet.participants.id = subTopicIdString;
    
    // Topic.race.cars
    fileValue = this.raceTopicSet.baseString.concat(getJSONStringOrExit(tRace,"cars")); // ex: "OLR/basePool/race/_RaceId_/_SubTopicIdString_/_CarId_"
    checkPlaceholder = FindStringOrExit(fileValue , "_RaceId_" );
    checkPlaceholder = FindStringOrExit(fileValue , "_CarId_" );
    checkPlaceholder = FindStringOrExit(fileValue , "_SubTopicIdString_" );
    subTopicIdString = getJSONStringOrExit(getJSONObjectOrExit(tRace,"subTopicIdString"),"cars"); // ex "Cars"
    String raceCarsStr = fileValue.replaceAll("_SubTopicIdString_", subTopicIdString); // ex: "OLR/basePool/race/_RaceId_/Cars/_CarId_"
    this.raceTopicSet.cars.topic = raceCarsStr; 
    this.raceTopicSet.cars.id = subTopicIdString;
    
    // Topic.race.config
    fileValue = this.raceTopicSet.baseString.concat(getJSONStringOrExit(tRace,"config")); // ex: "OLR/basePool/race/_RaceId_/_SubTopicIdString_"
    checkPlaceholder = FindStringOrExit(fileValue , "_RaceId_" );
    checkPlaceholder = FindStringOrExit(fileValue , "_SubTopicIdString_" );
    subTopicIdString = getJSONStringOrExit(getJSONObjectOrExit(tRace,"subTopicIdString"),"config"); // ex "Config"
    String raceConfigStr = fileValue.replaceAll("_SubTopicIdString_", subTopicIdString); // ex: "OLR/basePool/race/_RaceId_/Config"
    this.raceTopicSet.config.topic = raceConfigStr; 
    this.raceTopicSet.config.id = subTopicIdString;
     
    // Topic.race.telemetry
    fileValue = this.raceTopicSet.baseString.concat(getJSONStringOrExit(tRace,"telemetry")); // ex: "OLR/basePool/race/_RaceId_/_SubTopicIdString_"
    checkPlaceholder = FindStringOrExit(fileValue , "_RaceId_" );
    checkPlaceholder = FindStringOrExit(fileValue , "_SubTopicIdString_" );
    subTopicIdString = getJSONStringOrExit(getJSONObjectOrExit(tRace,"subTopicIdString"),"telemetry"); // ex "Telemetry"
    String raceTelemetryStr = fileValue.replaceAll("_SubTopicIdString_", subTopicIdString); // ex: "OLR/basePool/race/_RaceId_/Telemetry"
    this.raceTopicSet.telemetry.topic = raceTelemetryStr; 
    this.raceTopicSet.telemetry.id = subTopicIdString;

    return;
  }

  
  boolean isOnline(){
    return(this.isOnline);
  }

  boolean isConnected(){
    return(this.isConnected);
  }

  boolean isConnecting(){
    return(this.isConnecting);
  }

  String getId(){
    return(this._id);
  }

  String getSName(){
    return(this._sName);
  }

  String getName(){
    return(this._name);
  }

/**
  * connect() - Open connection to mqttServer
  */
  void connect() {
    
    //Connect to the supplied broker by parsing the URL and setting the client id and clean session flag:
    //    void client.connect(String brokerURI, String clientId, boolean cleanSession) 
    if(lastConnectAttempt==0 || ( System.currentTimeMillis() > lastConnectAttempt + TIMEOUT)) {
      t.log(Trace.BASE, String.format("connect(): mqttClientId:[%s]->Connecting to uri:[%s]\n", this.mqttClientId, this.MqttURI ));
      try {
        this.lastConnectAttempt = System.currentTimeMillis();
        this.client.connect(this.MqttURI, this.mqttClientId, true);
      } catch (RuntimeException e) {
        ///throw new RuntimeException(e);
        t.error(String.format("connect() ERROR: mqttClientId:[%s]->connect(%s)  %s ---> Retry in [%d] mSec\n", this.mqttClientId, this.MqttURI, e.getMessage(), TIMEOUT ));    
        isConnecting = false;
        return;
      }
      isConnecting = true;
    } 
  }


 /*
  * When an OLR device connects to the Network, it advertises himself:
  *  - PUB on the device/status/TRACK_ID topic (Payload = ONLINE)
  *  - Subscribe to basic required topics.
  *  - Subscribe to extra topics (if any):
  *    This may happen when a client DISCONNECT for a network error.
  *    and the user already joined a Race (so it was connected to Race-specific topics)
  */
  public void setupOLRN () {
    t.log(Trace.METHOD, String.format("setupOLRN()\n"));

    // Subscribe to [device/status/+] to get the Connected Device List  //<>// //<>// //<>// //<>//
    String subToDeviceStatus = this.deviceTopicSet.statusSubscribeString.concat("#"); // ex: "OLR/basePool/device/status/+"
    client.sub(subToDeviceStatus, Protocol.MQTT_QOS.AT_LEAST_ONCE);

    // Subscribe to [device/Recv/_Id] to get messages sent only to this device
    client.sub(this.deviceTopicSet.recv.topic, Protocol.MQTT_QOS.AT_LEAST_ONCE);      // ex: "OLR/basePool/device/Recv/HAM-00102833922"

    // Subscribe to [race/Status/+] to get the Active Race List
    String subToRaceStatus = this.raceTopicSet.statusSubscribeString.concat("#");      // ex: "OLR/basePool/race/status/+"
    client.sub(subToRaceStatus, Protocol.MQTT_QOS.AT_LEAST_ONCE);

    isOnline=true;

    client.publishDeviceStatus(Protocol.PDevice.Status.ONLINE);

  }

  
   /**
   * Publish a Device Leave Network messages sequence: 
   *  If it's subscribed to a Race - Leave It
   *  - Pub a STATUS message with Status = OFFLINE
   *  - Pub an EMPTY Payload message in the Status Topic with RETAIN = Yes
   *  - Pub an EMPTY Payload message in the Recv Topic with RETAIN = Yes
   */  
  void exitOLRN(){
    t.log(Trace.METHOD, String.format("\n\n\t___exitOLRN() CleanUp on user Exit____\n\n"));

    if( ! this.isConnected()) {
      return;
    }

    if( this.isOnline() ) {
      if(this.myRace.isValid()) {
        // LEAVE RACE
        client.publishRaceParticipantStatus(Protocol.PDevice.Status.LEAVING);
        // Clean Up its Partecipant Topic
        client.publishRaceParticipantStatusRemoveRetained();        
      }
      
      // Publish Status OFFLINE  
      client.publishDeviceStatus(Protocol.PDevice.Status.OFFLINE);
      // CleanUp its STATUS topic 
      client.publishDeviceStatusRemoveRetained();
    }    
    
    // DOES NOT Disconnect from Broaker
    // If the user is closing the Client with the [x] in the windows
    // and is inm a race, the LEAVING started above will 
    // still need to process an UNSUBSCRIBE 
    //    this.client.disconnect();
  }



  /**
  *  Process queues
  */
  void loop() {
    if( ! this.isConnected() || ! this.isOnline ) {
      return;
    }
    
    // Received Network Messages
    if(! recvMessage_Queue.isEmpty()){
      NetworkMessage rMsg =  recvMessage_Queue.poll() ;
      manageReceivedMessage(rMsg.topic, rMsg.message);
    }
    
    // Subscription Requests
    if(! subRequest_Queue.isEmpty()){
      SubscriptionRequest subReq = subRequest_Queue.poll() ;
      if(subReq.operation.equals(SubscriptionRequest.Type.SUBSCRIBE)){
        client.sub(subReq.topic, subReq.qos);
      } else {
        client.unsub(subReq.topic);
       }
    }

    // Check Changes in connected OLRDevice Status
    boolean serialConnectionStatus = phRaceDev.isConnected();
    if(serialConnectionStatus != serialLastConnectionStatus ){
      serialLastConnectionStatus = serialConnectionStatus;
      if(phRaceDev.isConnected()){
        client.publishDeviceStatus(Protocol.PDevice.Status.AVAILABLE);
      } else {
        client.publishDeviceStatus(Protocol.PDevice.Status.ONLINE);
      }
    }
    
    // Manage Serial connect() with OLR Board (handshake)
    // When connected, calls RaceDevice.draw()
    if(phRaceDev.isConnected()) { 
      phRaceDev.loop();
    } else {
      if(phRaceDev.isPortValid()) {
        phRaceDev.connect();
      } 
    }

    // Serial "Commands" received from OLRBoard  
    if(phRaceDev.isConnected() && ! phRaceDev.fromBoardQ.isEmpty()) {
      String cmd  = phRaceDev.fromBoardQ.poll() ;
      manageBoardCommand(cmd);
    } 
  
  } 

  /**
   *  get the id of the Device currently configuring the Race
   *
   * @param raceId id of the race 
   *
   * @return Device._id of the device configuring the race
   * @return null if no device is configuring the client
   */
  public String getRaceConfiguringClient(String raceId){
   String l[] = raceList.getPartnersId(raceId);
    for (int i=0; i<l.length ; i++) {
      String status=deviceList.getStatus(l[i]);
      if (status.equals(Protocol.PDevice.Status.CONFIGURING)){
        return(l[0]);
      }
    }
    return(null); 
  }


  /**
   * Create a New Race 
   * - send a {Create a Race} message on Race Status Topic
   * - send a {Car status} message for each car
   * - send a {Join Race} message to participates to it
   * Note:
   *  The {Car Status} messages sent here will create on subtopic
   *  for each car.
   *  Clients joining the race afterward will check if their car 
   *  configuration (Number of cars) is the same (Perticipants 
   *  RaceDevices needs to have the same number of cars) 
   * @return ErrorString on Error
   * @return null  on race created ok 
   */  
  public String sendCreateRace(String raceName){    
    t.log(Trace.METHOD, String.format("sendCreateRace(%s)\n",raceName));
  
    // generate a random string for the RaceId
    String newId = new RandomString(12, new SecureRandom()).nextString();
    if( raceList.exist(newId)) { // FARE WHILE ! Exist...
      t.error(String.format("sendCreateRace(%s) SOFTWARE ERROR: Generated RaceId=[%s] ALREADY EXIST\n",raceName, newId));
      return(String.format("ERROR - Generarated RaceId=[] ALREADY EXIST"));
    }
    
    t.log(Trace.BASE, String.format("sendCreateRace() PUB Race[%s][%s] Status[%s:Acceping Participants]\n",newId,raceName,Protocol.PRace.Status.ACCEPTING));
    client.publishRaceStatus(newId, raceName, Protocol.PRace.Status.ACCEPTING);  

    // Publish on Cars topic it's own Car configuration
    //   This is mainly to Create the <RaceId>/Cars/<CarId> topics
    //   so the Broker will not CLOSE the connection when clients 
    //   will SUBSCRIBE  to  <RaceId>/Cars/+ topic set
    int n=phRaceDev.carList.length;
    for (int i=0; i<n ; i++) {
      t.log(Trace.INTERNALS, String.format("sendCreateRace() PUB Car[%d][%s] in Device[%s] -> Status [STOP]\n",phRaceDev.carList[i].id, phRaceDev.carList[i].name, phRaceDev.carList[i].curDev ));
      client.publishCarStatus(newId, phRaceDev.carList[i].id, phRaceDev.carList[i].name, phRaceDev.carList[i].curDev,Protocol.PCar.Status.STOP, 0);
    }
    
    return(null);  
  }

  /**   //<>// //<>// //<>// //<>//
   * Send a {Join a Race} message on RaceParticipants channel
   * In the current implementation a NetworkDevice can participates 
   * to one Race only.
   * @param raceId Id of the Race to Join
   * @return  ErrorString ON Error
   * @return  null ON operation ok 
   */    
  public String sendSubscribeToRace(String raceId) {
    t.log(Trace.METHOD, String.format("sendSubscribeToRace(%s)\n",raceId ));

    setMyRaceAttributes(raceId);
    
    _sendSubscribeToRace();

sendConfirmSubscriptionToRace(raceId);
    
    return(null);  //<>// //<>// //<>// //<>//
  }
  /** 
   * Send a {Join a Race} message on RaceParticipants channel
   * Usen on Race Creation (Race does not exist yet and 
   *    setRaceParticipantAttributes(raceId) 
   * would fail to find the Race and set the name.
   * In the current implementation a NetworkDevice can participates 
   * to one Race only.
   * @param raceId Id of the Race to Join
   * @param raceName Name of the Race to Join
   * @return  ErrorString ON Error
   * @return  null ON operation ok 
   */    
  public String sendSubscribeToRace(String raceId, String raceName) {
    t.log(Trace.METHOD, String.format("sendSubscribeToRace(%s,%s)\n",raceId,raceName ));

    setMyRaceAttributes(raceId,raceName); 
    
    _sendSubscribeToRace();
    
sendConfirmSubscriptionToRace(raceId);
    
    return(null);  
  }

  private String _sendSubscribeToRace() {
    // Publish to RaceParticipants
    client.publishRaceParticipantStatus(Protocol.PDevice.Status.SUBSCRIBING );
    // Change its own Status to [SUBSCRIBING]
    client.publishDeviceStatus(Protocol.PDevice.Status.SUBSCRIBING); 
    
    return(null);  
  }


  /**
   * Leave a Race as Participant
   *
   * @param raceId The Id of the Race to leave
   *
   * @return  ErrorString ON Error
   * @return  null ON operation ok 
   */  
  public String sendLeaveRace(String raceId) {
    t.log(Trace.METHOD, String.format("sendLeaveRace(%s)\n",raceId ));
    
    client.publishRaceParticipantStatus(Protocol.PDevice.Status.LEAVING );

    // Remove Retained Message from Topic (EMPTY Payload, Retained = TRUE)
    client.publishRaceParticipantStatusRemoveRetained();
    
    // change back his Status to [Available]
    client.publishDeviceStatus(Protocol.PDevice.Status.AVAILABLE);
    
    // Participant Topic String will be RESET on message reception, 
    // after unsubscribe from RACE-Specific topics  
    
    return(null);
    
  }

  /**
   * Delete a Race
   * Publish a {Delete race} message in Race Status Topic, 
   * and Remove the Retained Message
   *
   * @param raceId The Id of the Race to leave
   *
   * @return  ErrorString ON Error
   * @return  null ON operation ok 
   */  
  public String sendDeleteRace(String raceId){
    t.log(Trace.METHOD, String.format("sendDeleteRace(%s)\n", raceId ));

    int pNum = raceList.numberOfPartners(raceId);
    
    if(pNum < 0){
      t.error(String.format("sendDeleteRace() ERROR - RaceId=[%s] does NOT EXIST\n", raceId));
      return("NetworkDevice:sendDeleteRace() ERROR - RaceId does NOT EXIST");
    }
    if(pNum > 0){
      t.error(String.format("sendDeleteRace() ERROR - RaceId=[%s] have partecipants:[%d]\n", raceId, pNum));
      return("NetworkDevice:sendDeleteRace() ERROR - RaceId does NOT EXIST");
    }

    // I'm (and nobody) is subscriber to the race.
    // Even if i was subscribed, resetRaceParticipantAttributes()
    // reset race data on LEAVE request.
    // So we ned to call raceList.getName(raceId)
    client.publishRaceStatus(raceId, raceList.getName(raceId), Protocol.PRace.Status.DELETED);  

    // Remove Retained Message from RaceStatus Topic 
    client.publishRaceStatusRemoveRetained(raceId);  
    return(null);
    
  }



  /**
   * publish a {Subscribe to Race} message in Participant Status Topic
   *
   * @param raceId The Id of the Race to Confirm
   *
   * @return  String: ErrorString ON Error
   * @return  String: null ON operation ok 
   */  
  public String sendConfirmSubscriptionToRace(String raceId){
    t.log(Trace.METHOD, String.format("sendConfirmSubscriptionToRace(%s)\n",raceId ));

    client.publishRaceParticipantStatus(Protocol.PDevice.Status.SUBSCRIBED);
    
    // change his status to [SUBSCRIBING]
    client.publishDeviceStatus(Protocol.PDevice.Status.SUBSCRIBED);    
    
    return(null);    

  }


  /**
   * Play again the same race i was participating to
   *
   * @param raceId The Id of the Race to leave
   *
   * @return  ErrorString ON Error
   * @return  null ON operation ok 
   */  
  public String sendPlayAgainRace(String raceId) {
    t.log(Trace.METHOD, String.format("sendPlayAgainRace(%s)\n",raceId ));

    client.publishDeviceStatus(Protocol.PDevice.Status.RACEAGAIN);
    
    return(null);
    
  }



  /**
   * Publish a {Configure Race} message in Race Status Topic
   *
   * @param raceId The Id of the Race
   *
   * @return  ErrorString ON Error
   * @return  null ON operation ok 
   */  
  public String sendConfigureRace(String raceId){
    t.log(Trace.METHOD, String.format("sendConfigureRace(%s)\n",raceId ));    
    
    if( ! raceList.exist(raceId )) { 
      t.error(String.format("configureRace() ERROR - RaceId=[%s] Not found \n", raceId));
      return("configureRace() ERROR - RaceId=[" + raceId + "] Not found ");
    }

    client.publishRaceStatus(raceId, myRace.name, Protocol.PRace.Status.CONFIGURING);
    
    // change its status to [CONFIGURING]
    client.publishDeviceStatus(Protocol.PDevice.Status.CONFIGURING);    
      
    return(null);
    
  }


  /**
   * Send a {Configuration Parameters} message in Race Config Channel
   *
   * @param raceId id of the Race
   * @param pdu  PDU containing the Config payload
   *
   * @return  String: ErrorString ON Error
   * @return  String: null ON operation ok 
   */  
  public String sendRaceConfigParam(Protocol.Network.PDU pdu){
    t.log(Trace.METHOD, String.format("sendRaceConfigParam(PDU)\n"));

    // Pub to Config topic for this race
    client.publishRaceConfigParam(pdu);

    // change Race status to [CONFIGURING_DEVICE]
    client.publishRaceStatus(myRace.id, myRace.name, Protocol.PRace.Status.CONFIGURING_DEVICE);


    return(null);
  }

  /**
   * Set values for myRace object
   * @param rid: Race "id"
   */
  private void setMyRaceAttributes(String rid){
    t.log(Trace.METHOD, String.format("setMyRaceAttributes(%s)\n",rid ));
    String name = raceList.getName(rid);
    if( name == null){
      t.error(String.format("setMyRaceAttributes() ERROR - RaceId=[%s] does NOT EXIST\n", rid));
      return;
    }        
    setMyRaceAttributes(rid,name);
  }
  
  /**
   * Set values for myRace object
   * @param rid: Race "id" 
   * @param rName: Race "name"
   */
  private void setMyRaceAttributes(String rid,String rName){
    t.log(Trace.METHOD, String.format("setMyRaceAttributes(%s,%s)\n",rid,rName ));

    myRace.id  = rid;           
    myRace.name = rName;
    myRace.participantsTopicStr = new String( raceTopicSet.participants.topic.replaceAll("_RaceId_", rid));
    client.setRaceParticipantStatusChannel( raceTopicSet.participants.topic.replaceAll("_RaceId_", rid) );
   
    myRace.telemetryTopicStr = new String( raceTopicSet.telemetry.topic.replaceAll("_RaceId_", rid));;
    client.setRaceTelemetryChannel(myRace.telemetryTopicStr);
    
    // currently: "config":"OLR/basePool/race/_RaceId_/Config"
    //   set myRace.configTopicStr to "OLR/basePool/race/RACE_001/Config"
    myRace.configTopicStr = new String (raceTopicSet.config.topic.replaceAll("_RaceId_", rid));
    client.setRaceConfigChannel(myRace.configTopicStr);
    
    // currently: "cars":"OLR/basePool/race/_RaceId_/Cars/_CarId_"
    //   set myRace.carsTopicStr to "OLR/basePool/race/RACE_001/Cars/"
    String raceCarStr = new String (raceTopicSet.cars.topic.replaceAll("_RaceId_", rid).replaceAll("_CarId_", ""));
    myRace.carsTopicStr = raceCarStr;
  }


  /**
   * Reset values for Attributes storing raceParticipant and RaceSend topic
   * Invoked when the Client LEAVE a race  
   */
  private void resetMyRaceAttributes(){
    myRace.reset();
    t.log(Trace.METHOD, String.format("resetMyRaceAttributes()\n" ));
    
    client.resetRaceParticipantStatusChannel();
    client.resetRaceTelemetryChannel();
    client.resetRaceConfigChannel();    
  }



  /**
   * Process incoming messages -> calls the topic-specific manageXxxx() method  
   */   
  void manageReceivedMessage(String topic, MqttMessage message) {
      t.log(Trace.METHOD + Trace.DETAILED, String.format("manageReceivedMessage(): RECEIVED Topic:[%s] Payload:[%s] QoS:[%d] Retained:[%b]\n\n",topic,new String(message.getPayload()), message.getQos(), message.isRetained()  ));

      String  payload = new String(message.getPayload());

      if(payload.isEmpty()){
        // usually a RemoveRetained message
        t.log(Trace.LOWLEVEL, String.format("manageReceivedMessage(): DROP Msg on Topic[%s] - EMPTY Payload\n",topic));
        return;
      }
      
      // Channels with Text Payload messages        
      if(payload.startsWith(Protocol.TEXTPAYLOAD_HEADER)){  
        // Race Cars Channel - Process Immediately
        if(topic.startsWith(this.raceTopicSet.baseString) && topic.contains(this.raceTopicSet.cars.id)) { // ex: OLR/basePool/race/RACE9323613531/Car/1                 
          String baseRaceId = topic.substring(this.raceTopicSet.baseString.length());   // ex: RACE9323613531/Car/1
          String raceId = baseRaceId.substring(0, baseRaceId.indexOf('/')) ;            // ex: RACE9323613531
          boolean ok=manageReceivedRaceCar(raceId, payload,message.isRetained()); 
          if(! ok){
            t.error(String.format("manageReceivedMessage(): - DROP: \n[%s] [%s]\n", topic, payload));
          }
          return;
        } else {
          t.error(String.format("manageReceivedMessage() ERROR: - Unknown Plain Text Payload Channel:[%s] - DROP:[%s]\n", topic,payload));
          return;
        }
      }
      
      // Channels with JSON Payload messages 
      JSONObject json = new JSONObject();
      try {
        json = JSONObject.parse(payload); 
      } catch (RuntimeException jse) {
        t.error(String.format("manageReceivedMessage() ERROR: DROPPED - Not a valid Json String:[%s] received on [%s] - %s\n",payload,topic,jse.getMessage()));
        return;
      }
      // Check OLRN Version
      String ver = trim(json.getString(Protocol.OLRNVersion.jName , "-"));
      if(! ver.equals(Protocol.OLRNVersion.ver)) {
        t.log(Trace.DETAILED, String.format("manageReceivedMessage(): OLRNVersion[%s] != required[%s] - DROP: \n%s\n", ver, Protocol.OLRNVersion.ver,json.toString()));
        return;
      }      
      
      // new PDU containing JSON Object
      Protocol.Network.PDU pdu = new Protocol.Network.PDU(topic, json, message.getQos(), message.isRetained());

      boolean ok = false;

      if(topic.startsWith(this.deviceTopicSet.statusSubscribeString)) {          // Device Status Topic, ex: "OLR/basePool/device/status/HAM-00102833922"
        ok=manageReceivedDeviceStatus(topic.substring((topic.lastIndexOf('/'))+1).trim(), pdu);
        
      } else if(topic.equals(this.deviceTopicSet.recv.topic)) {                  // Device Recv Topic, ex: "OLR/basePool/device/Recv/HAM-00102833922
        ok=processDeviceRecv(topic.substring((topic.lastIndexOf('/'))+1).trim(), pdu);
      
      } else if(topic.equals(this.deviceTopicSet.broadcast.topic)) {              // Device broadcast message, ex: OLR/basePool/device/broadcast
        ok=processDeviceBroadcast(pdu);

      } else if(topic.startsWith(this.raceTopicSet.statusSubscribeString)) {      // Race Status update
        ok=manageReceivedRaceStatus(topic.substring((topic.lastIndexOf('/'))+1), pdu);

      } else if(topic.startsWith(this.raceTopicSet.baseString)) {                 // Ex: "OLR/basePool/race/" 
         
        // Topics example: OLR/basePool/race/RACE9323613531/Participants/HAM021412471274        
        String baseRaceId = topic.substring(this.raceTopicSet.baseString.length());   // ex: RACE9323613531/Participants/HAM021412471274
        String raceId = baseRaceId.substring(0, baseRaceId.indexOf('/')) ;            // ex: RACE9323613531
        
        if(topic.contains(this.raceTopicSet.participants.id)) {                    // - Race Participants Channel
          String trackId = topic.substring((topic.lastIndexOf('/')) +1) ;
          ok=manageReceivedRaceParticipants(raceId.trim(), trackId.trim(), pdu);
   
        } else if (topic.contains(this.raceTopicSet.config.id)) {                  // - Race Config Channel
          ok=manageReceivedRaceConfig(raceId, pdu);            

        } else {
          t.error(String.format("manageReceivedMessage() ERROR: Race Specific Channel with no match found - DROP: \n[%s] \n%s \n", topic,payload));
        }

      } else {
        t.error(String.format("manageReceivedMessage() ERROR: - Unknown Channel:[%s] - DROP:\n%s \n", topic,payload));
      }

      if(! ok){
        t.error(String.format("manageReceivedMessage(): - DROP: \n[%s] \n%s \n", topic, payload));
      }
    }

  
  
  /**
    * Manage messages received on Device Status Topic
    * 
    * Note: Device status = [Configuring a Race]  
    *   If two clients click CONFIGURE a Race at almost the same time
    *   the system will:
    *     1) send [Race Status]=[Configuring] on RaceStatus Topic 
    *          -> received and update twice, no prob
    *     2) send [Client Status]=[Configuring] on deviceStatus Topic 
    *          -> received twice - FIRST ONE WIN!
    *             Only the first Device sending the new status will
    *             change it's state to [Configuring]
    * 
    * @param id: The Device id who sent it's status update - Taken from the last part of the topic
    * @param json: The received message Payload (in JSON format)
    */
  boolean manageReceivedDeviceStatus(String cid, Protocol.Network.PDU pdu){
    t.log(Trace.METHOD, String.format("manageReceivedDeviceStatus(%s, PDU)\n" , cid));

    // Decode: get Device Obj from PDU
    String errStr=Protocol.Network.Channel.DeviceStatus.decodePDU(pdu, this.recvDevice);
    if(errStr != null){
      t.error(String.format("manageReceivedDeviceStatus():%s", errStr));
      return(false);    
    }

    // message SENT by This NetworkDevice (It's ME)
    if(this._id.equals(this.recvDevice._id)){      
    
      // Discharge RETAINED MESSAGES for My Status
      //   In the current implementation in a client gets offline
      //   no action are taken to restore the previous situation
      if(pdu.isRetained && !(this.recvDevice._status.equals(Protocol.PDevice.Status.OFFLINE)) ){ 
        t.error(String.format("manageReceivedDeviceStatus(): RETAINED message for MY Status:[%s:%s].\n",recvDevice._status,Protocol.PDevice.status.decode(recvDevice._status)));
      }
      
      // if it's ME sending a Device Status=[configuring] -> manage conflicts (two NetworkDevices push the button almost at the same time)
      if(this.recvDevice._status.equals(Protocol.PDevice.Status.CONFIGURING)) {
        if(! this.myRace.isValid()){
          t.error("manageReceivedDeviceStatus(): IT's ME Configuring a Race BUT no Valid this.myRace \n");
        }
        String cfgClient = this.getRaceConfiguringClient(this.myRace.id);
        if( cfgClient != null) {
          t.error(String.format("manageReceivedDeviceStatus() TOO LATE: Race=[%s] is already being configured by Client=[%s]\n", this.myRace.name, cfgClient));
          t.error(String.format("       -> reset my Status to [SUBSCRIBED]\n"));
            client.publishDeviceStatus(Protocol.PDevice.Status.SUBSCRIBED); // ...should be[SUBSCRIBED]
          return(false);
        }
      }

      this._status=this.recvDevice._status; // updates internal var holding status for this client
      t.log(Trace.DETAILED, String.format("manageReceivedDeviceStatus(): ITS ME...\n"));
    }

    // Update/Add Device 
    deviceList.updateOrAdd(this.recvDevice._id, this.recvDevice._sName, this.recvDevice._name, this.recvDevice._status); 

    // it's ME sending a DeviceStatus CONFIGURED message
    // If everybody else in the race is already configured:
    //  - Publish "Ready" in RACE STATUS topic
    //  - Publish on Cars topics "Car in in the first circuit of the race"
    if( this._id.equals(this.recvDevice._id) && this.recvDevice._status.equals(Protocol.PDevice.Status.CONFIGURED)) {
      t.log(Trace.DETAILED, String.format("manageReceivedDeviceStatus() Recv CONFIGURED msg I JUST SENT -> check if everybody else is already configured...\n"));
      boolean everybodyConfigured = true;
      String p[] = raceList.getPartnersId(myRace.id);
      for(int j = 0 ; j < p.length ; j++) {
        String devId = p[j].trim();
        String status = deviceList.getStatus(devId);
        if (! status.equals(Protocol.PDevice.Status.CONFIGURED)){
          everybodyConfigured = false;
          break;
        }      
      }
      if(everybodyConfigured){
        t.log(Trace.DETAILED, String.format("manageReceivedDeviceStatus(): Everybody configured -> PUB READY_TO_START in Race Status Channel\n"));
        client.publishRaceStatus(myRace.id, myRace.name, Protocol.PRace.Status.READY_TO_START);
        
        RaceConfig lCfg = raceOrder.getRaceConfig(0);
        if(lCfg == null){
          t.error(String.format("manageReceivedDeviceStatus() SOFTWARE ERROR - EMPTY raceOrder.getRaceConfig(0)\n"));
          return(false);
        }
        int n=phRaceDev.carList.length;
        for (int i=0; i<n ; i++) {
          t.log(Trace.DETAILED, String.format("manageReceivedDeviceStatus(): PUB Car[%d][%s] in [%s] Status[STOP] in Car Status Channel\n",phRaceDev.carList[i].id, phRaceDev.carList[i].name, lCfg.olrId));
          client.publishCarStatus(myRace.id, phRaceDev.carList[i].id, phRaceDev.carList[i].name, lCfg.olrId, Protocol.PCar.Status.STOP,0);
        }
      }
    }
    
    // it's ME sending PLAY RACE AGAIN 
    if( this._id.equals(this.recvDevice._id) && this.recvDevice._status.equals(Protocol.PDevice.Status.RACEAGAIN)) {

      RaceConfig lCfg = raceOrder.getRaceConfig(this._id);
      if(lCfg == null){
        t.error(String.format("manageReceivedDeviceStatus() SOFTWARE ERROR: this._id=[%s] Not found in raceOrder list. \n",this._id));
        return(false);
      }
      t.log(Trace.DETAILED, String.format("manageReceivedDeviceStatus() Recv:RACE AGAIN--->Resend Config to Board:[%d][%d][%d][%d]\n",lCfg.start,lCfg.laps,lCfg.repeat,lCfg.finish ));
      phRaceDev.setRaceParams(lCfg.start,lCfg.laps,lCfg.repeat,lCfg.finish);
      phRaceDev.sendConfig();
      // Back to Configuring Dev Phase
      t.log(Trace.DETAILED, String.format("manageReceivedDeviceStatus() Recv:RACE AGAIN---> PUB my status = CONFIGURING_DEV\n"));
      client.publishDeviceStatus(Protocol.PDevice.Status.CONFIGURING_DEV);
    }


// TODO
//  Manage CFG_ERROR ricevuto da uno dei partecipanti (o da me)
// Adesso cambia solamente lo stato (compare in UI con stato = cfg_error)

    // Force the reload of RaceList in the UI (needed for {Leave} and {Configuring} messages
    raceList.setLastUpdateTime();

    return(true);
  }



  /**
   * Method invoked on Reception of a Message on the 
   *   [race/<RaceId>/Status] topic (Authoritative for Race List)
   *
   * @param raceId Race Id
   * @param json   message Payload in JSON format 
   * @param isRetained Message send automatically by the broker on Subscribe to the Topic Y|N
   *
   * @return Operations OK [Y|N] 
   *
   * Implementation  
   *
   *  Race Exist ?
   *  NO:
   *    Create Race
   *    Subscribe to Race's Participants Topic
   *  YES:
   *    Update Race Status
   *
   *  IF it's a Delete Operation:
   *    Delete Race
   *    Unsubscribe to Race's [Participants] Topic
   *    Unsubscribe to other's partecipants [Send] Topic  
   *
   * Note:
   *   Method invoked by the messageArrived() Callback.
   *     - It's ok to publish a message here (paho documentation)
   *     - It's NOT OK to invoke a SUBSCRIBE method from here (tested):
   *         -> Use the SubscribeToTopic QUEUE instead         
   */
  boolean manageReceivedRaceStatus(String raceId, Protocol.Network.PDU pdu) {
    t.log(Trace.METHOD, String.format("manageReceivedRaceStatus(%s, PDU)\n",raceId));

    // Decode: get Race Obj from PDU
    String errStr=Protocol.Network.Channel.RaceStatus.decodePDU(pdu, this.recvRace);
    if(errStr != null){
      t.error(String.format("manageReceivedRaceStatus(%s,PDU):%s", raceId, errStr));
      return(false);    
    }    
    
    // Check if Device sending the message exists
    int n = deviceList.indexOf(this.recvRace._updatedBy);
    if( n < 0) { 
      if(pdu.isRetained){
        // this happens on Client startup, when a RaceStatus msg arrive before the DeviceStatus of the device creating/updating the race
        // or when the Device creating the race is gone
        // The raceStatus msg is NOT ignored so the race appears in the UI and can be used (or deleted if status > accepting participants)
        t.log(Trace.INTERNALS,String.format("manageReceivedRaceStatus() : RETAINED MSG - Race[%s] updatedBy[%s] NOT FOUND in Device List !\n", raceId,this.recvRace._updatedBy));
      } else {
        t.error(String.format("manageReceivedRaceStatus() ERROR: Race[%s] updatedBy[%s] NOT FOUND in Device List\n", raceId, this.recvRace._updatedBy));
        return(false);
      }
    }
    if( ! raceId.equals(this.recvRace._id)){
      t.error(String.format("manageReceivedRaceStatus() SOFTWARE ERROR: RaceId param[%s] differs from PDU(this.recvRace._id)[%s]\n", raceId, this.recvRace._id));
      return(false);
    }

    boolean ok = false;    
    // Race NOT found in Racelist -> Creates it
    ///////////////////////////////////////////
    if( ! raceList.exist(this.recvRace._id)) {
      t.log(Trace.BASE, String.format("manageReceivedRaceStatus(): create Race[%s][%s] Status:[%s] (retained=%b)\n",this.recvRace._id,this.recvRace._name,this.recvRace._status,pdu.isRetained));
      if( ! this.recvRace._status.equals(Protocol.PRace.Status.ACCEPTING) && ! pdu.isRetained) {
        // creation of a New Race in a status > ACCEPTING from a not retained message -> protocol error or so
        t.error(String.format("manageReceivedRaceStatus() ERROR: create a Race in a Status > ACCEPTING from a Message NOT Retained - Status:[%s]\n", raceId,this.recvRace._status));
        return(false);
      }
      ok=raceList.add(this.recvRace._id, this.recvRace._name, this.recvRace._status, this.recvRace._updatedBy); // ADD the Race in RaceList
      if(! ok) {
        return(false);  
      }
      // SUBSCRIBE to the [Participants] Topic of the newly created race to get the Participants List 
      String subToRaceParticipants = raceTopicSet.participants.topic.replaceAll(this._id, "#").replaceAll("_RaceId_", this.recvRace._id); // Replace BEFORE [id] with [#] and AFTER [_RaceId_] with [raceId], in case RaceId contains id
      SubscriptionRequest subReq = new SubscriptionRequest ( subToRaceParticipants, Protocol.MQTT_QOS.AT_LEAST_ONCE ); 
      ok = subRequest_Queue.offer(subReq);
      if(! ok) {
        t.error(String.format("manageReceivedRaceStatus() ERROR:  SUBSCRIBE subRequest_Queue FULL -> offer() returns FALSE\n"));
      }
      t.log(Trace.INTERNALS, String.format("manageReceivedRaceStatus(): SUBSCRIBE to [%s] ENQUEUED\n",subToRaceParticipants));
    } else {
      // Update Race Status
      t.log(Trace.INTERNALS, String.format("manageReceivedRaceStatus(): UPDATE [%s][%s]\n",this.recvRace._id,this.recvRace._status));
      if(! raceList.updateStatus(this.recvRace._id, this.recvRace._status, this.recvRace._updatedBy)) {
        return(false);
      } 
    }

    // Status==DELETED ---> DELETE RACE operation
    /////////////////////////////////////////////
    if( this.recvRace._status.equals(Protocol.PRace.Status.DELETED)) {
      t.log(Trace.BASE, String.format("manageReceivedRaceStatus(): delete Race[%s][%s]\n",this.recvRace._id,this.recvRace._name));
      ok=raceList.delete(this.recvRace._id);
      if(! ok) {
        return(false);  // Errorlog in raceList.delete()
      }
      // UNSUBSCRIBE request for Participants topic
      //String subToRaceParticipants = raceParticipants.topicString.replaceAll("_RaceId_", raceId).replaceAll(this._id, "#");
      String subToRaceParticipants = raceTopicSet.participants.topic.replaceAll("_RaceId_", raceId).replaceAll(this._id, "#");
      SubscriptionRequest subReq = new SubscriptionRequest ( subToRaceParticipants, SubscriptionRequest.Type.UNSUBSCRIBE ); 
      ok = subRequest_Queue.offer(subReq);
      if(! ok) {
        t.error(String.format("manageReceivedRaceStatus() ERROR: UNSUBSCRIBE subRequest_Queue FULL -> offer() returns FALSE\n"));
      }
      t.log(Trace.INTERNALS, String.format("manageReceivedDeviceStatus(): UNSUBSCRIBE to [%s] ENQUEUED\n",subToRaceParticipants));
      return(true);
    } 
    
//LUCA4

    // Status Operations for MY race 
    ////////////////////////////////
    if(myRace.id !=null && this.recvRace._id.equals(myRace.id)) {
  
      if( this.recvRace._status.equals(Protocol.PRace.Status.READY_TO_START)) {
        // the race Owner sent a READY: Send to Board
        t.log(Trace.BASE, String.format("manageReceivedRaceStatus(): [%s] Recv:READY--->Send to board\n",this.recvRace._id));
        phRaceDev.sendRacePhase(ProtocolSerial.Cmd.RacePhase.Phase.READY);
        // Change my NetworkDevice status to "Racing"
        t.log(Trace.BASE, String.format("manageReceivedRaceStatus(): [%s] Publish my new Status=RACING\n",this.recvRace._id));
        client.publishDeviceStatus(Protocol.PDevice.Status.RACING);
      } 
      
      if( this.recvRace._status.equals(Protocol.PRace.Status.COMPLETE)) {
        // Change my NetworkDevice status to "Race Complete"
        client.publishDeviceStatus(Protocol.PDevice.Status.RACEDONE);
        t.log(Trace.DETAILED, String.format("manageReceivedRaceStatus(): [%s] Publish my new Status=Race Complete\n",this.recvRace._id));    
        if( ! this.recvRace._updatedBy.equals(this._id)) { // if the command was NOT originated from this Board
           // Sent to Board
           t.log(Trace.BASE, String.format("manageReceivedRaceStatus(): [%s] Recv:COMPLETE--->Send to board\n",this.recvRace._id));
           phRaceDev.sendRacePhase(ProtocolSerial.Cmd.RacePhase.Phase.COMPLETE);
        }      
      }


      // Only for Race Status command NOT originated from this Board 
      if( ! this.recvRace._updatedBy.equals(this._id)) {
        
        if( this.recvRace._status.equals(Protocol.PRace.Status.COUNTDOWN)) {
          t.log(Trace.BASE, String.format("manageReceivedRaceStatus(): [%s] Recv:COUNTDOWN--->Send to board\n", this.recvRace._id));
          phRaceDev.sendRacePhase(ProtocolSerial.Cmd.RacePhase.Phase.COUNTDOWN);
        }
        if( this.recvRace._status.equals(Protocol.PRace.Status.RACING)) {
          t.log(Trace.BASE, String.format("manageReceivedRaceStatus(): [%s] Recv:RACING--->Send to board\n", this.recvRace._id));
          phRaceDev.sendRacePhase(ProtocolSerial.Cmd.RacePhase.Phase.RACING);
        }
      }
    }

// TODO
//  Manage CFG_ERROR per la Race inviato da uno dei partecipanti (o da me)

    return(true);
  }
  

  /**
   * Method invoked on Reception of a Message on the Race Participants topic 
   *
   * @param raceId Race Id
   * &param id     Device id
   * @param pdu    Received message 
   *
   * @return Operations OK [Y|N] 
   */
  boolean manageReceivedRaceParticipants(String raceId, String deviceId, Protocol.Network.PDU pdu ) {
    t.log(Trace.METHOD, String.format("manageReceivedRaceParticipants(%s, %s, PDU)\n", raceId,deviceId));

    // Decode: get Device Obj from PDU
    String errStr=Protocol.Network.Channel.RaceParticipantStatus.decodePDU(pdu, this.recvDevice);
    if(errStr != null){
      t.error(String.format("manageReceivedRaceParticipants():%s", errStr));
      return(false);    
    }
    if( ! deviceId.equals(this.recvDevice._id)){
      t.error(String.format("manageReceivedRaceParticipants() SOFTWARE ERROR: deviceId param[%s] differs from PDU(dev._id)[%s]\n", deviceId, this.recvDevice._id));
      return(false);
    }

    // RETAINED MESSAGE for ME Partecipating to a Race
    //   In the current implementation in a client gets offline
    //   no action are taken to restore the previous real situation
    //   for races > accepting participants
    if(pdu.isRetained && this._id.equals(deviceId)) {
      t.error(String.format("manageReceivedRaceParticipants(): RETAINED - I WAS PARTECIPATING to Race [%s]\n", raceId));
    }

    // Participant already in the Race's Participants List ?
    ////////////////////////////////////////////////////////
    boolean createdNow = false;
    if( ! raceList.partnerExist(raceId, deviceId)) {
      // Participant not found in Race - ADD 
      createdNow = true;
      t.log(Trace.BASE, String.format("manageReceivedRaceParticipants():ADD Participant[%s] in Status[%s] to Race[%s]\n" ,this.recvDevice._sName, this.recvDevice._status ,raceId));

      if( ! this.recvDevice._status.equals(Protocol.PDevice.Status.SUBSCRIBING) && ! pdu.isRetained) {
        // creation of a New Participant in a Race in a status > SUBSCRIBING from a not retained message -> protocol error or so
        t.error(String.format("manageReceivedRaceParticipants()ERROR: New Participant:[%s] Status:[%s] SUBSCRIBING in a message Not Retained - Race:[%s]\n", this.recvDevice._sName, this.recvDevice._status, raceId));
        return(false);
      }
      // Add the [deviceId] to the Race Participants List  
      int err = raceList.addPartner(raceId,deviceId);
      if( err < 0){    // should never happen - Software error 
        t.error(String.format("manageReceivedRaceParticipants() ERROR: Unexpected Fail on ADD CREATING Participant=[%s] to Race=[%s]\n", deviceId,raceId));
        return(false);
      }
    } 
    
 // TODO LUCA .... VERIFICARE STATO REALE DEL RaceDevice collegato in seriale
 
    // It's me  
    boolean ok;
    if(this._id.equals(deviceId)) {      
      this._status=this.recvDevice._status; 
      if(createdNow) {   
        // It's me Subscribing to the Race    
        t.log(Trace.INTERNALS, String.format("manageReceivedRaceParticipants():It's me Subscribing to the Race[%s]\n", raceId));
        setMyRaceAttributes(raceId);  // - Set Client's Attributes Strings for this race
        
        // - Sub to CONFIG topic for this Race
        // currently this.raceConfigTopicStr - "OLR/basePool/race/RACE_001/Config"
        SubscriptionRequest cfgSubReq = new SubscriptionRequest (myRace.configTopicStr, Protocol.MQTT_QOS.AT_LEAST_ONCE );
        t.log(Trace.INTERNALS, String.format("manageReceivedRaceParticipants():Enqueue Subscription to RaceCfg[%s]\n", myRace.configTopicStr));
        ok = subRequest_Queue.offer(cfgSubReq);
        if(! ok) { t.error(String.format("manageReceivedRaceParticipants() ERROR:[RaceCfg] subRequest_Queue FULL - offer() returns FALSE\n"));  }
        
        // - Sub to CARS topics for this Race
        // currently this.raceCarsTopicStr - "OLR/basePool/race/RACE_001/Cars/"
        SubscriptionRequest carSubReq = new SubscriptionRequest (myRace.carsTopicStr.concat("#"), Protocol.MQTT_QOS.AT_LEAST_ONCE );
        t.log(Trace.INTERNALS, String.format("manageReceivedRaceParticipants():Enqueue Subscription  to RaceCar[%s]\n", myRace.carsTopicStr));
        ok = subRequest_Queue.offer(carSubReq);
        if(! ok) { t.error(String.format("manageReceivedRaceParticipants() ERROR:[RaceCar] subRequest_Queue FULL - offer() returns FALSE\n"));  }
      } 
    } 

    // LEAVE Race
    /////////////
    if( this.recvDevice._status.equals(Protocol.PDevice.Status.LEAVING)) {
      if(this._id.equals(deviceId)) {        
        // It's ME LEAVING THE RACE
        t.log(Trace.INTERNALS, String.format("manageReceivedRaceParticipants(): IT'S ME Leaving the race [%s]\n", raceId));
        this._status=this.recvDevice._status; // updates internal var holding status for this client
        
        // - UNSUBSCRIBE to CONFIG topic for this Race - Currently this.raceConfigTopicStr = "OLR/basePool/race/RACE_001/Config"
        SubscriptionRequest cfgSubReq = new SubscriptionRequest (myRace.configTopicStr, SubscriptionRequest.Type.UNSUBSCRIBE );
        ok = subRequest_Queue.offer(cfgSubReq);
        if(! ok) { t.error(String.format("manageReceivedRaceParticipants() ERROR:UNSUBSCRIBE to RaceCfg[%s] subRequest_Queue FULL - offer() returns FALSE\n", myRace.configTopicStr));  }
        t.log(Trace.INTERNALS, String.format("manageReceivedRaceParticipants():UNSUBSCRIBE to RaceCfg[%s] ENQUEUED\n", myRace.configTopicStr));
        
        // - UNSUBSCRIBE to CARS topics for this Race - currently this.raceCarsTopicStr - "OLR/basePool/race/RACE_001/Cars/"
        SubscriptionRequest carSubReq = new SubscriptionRequest (myRace.carsTopicStr.concat("#"), SubscriptionRequest.Type.UNSUBSCRIBE );
        ok = subRequest_Queue.offer(carSubReq);
        if(! ok) { t.error(String.format("manageReceivedRaceParticipants() ERROR:UNSUBSCRIBE to [Car] subRequest_Queue FULL - offer() returns FALSE\n"));  }
        t.log(Trace.INTERNALS, String.format("manageReceivedRaceParticipants():UNSUBSCRIBE to [Car] ENQUEUED\n"));
                
        // If the race i'm leaving was COMPLETED (LEAVE and not RACE_AGAIN) 
        //   The race, and it's configuration,  have no sense anymore ---> Cleanup
        String rStatus = raceList.getStatus(raceId);
        if(rStatus != null && rStatus.equals( Protocol.PRace.Status.COMPLETE   )) {  
          t.log(Trace.INTERNALS, String.format("manageReceivedRaceParticipants(): I'm Leaving a COMPLETED Race -> cleanup...\n"));
          client.publishRaceConfigParamRemoveRetained();                                      // Remove Config channel retained content
          int n=phRaceDev.carList.length;
          for (int i=0; i<n ; i++) {
            client.publishCarStatusRemoveRetained(myRace.id, phRaceDev.carList[i].id);        // Remove Car channels retained content
          }
          // I AM THE LAST DEVICE LEAVING the race -> RESET the race to "Accepting Participants" status
          String p[] = raceList.getPartnersId(myRace.id);
          if(p.length == 1 && p[0].trim().equals(this._id) ){
            t.log(Trace.INTERNALS, String.format("manageReceivedRaceParticipants(): I'm the last one Leaving -> PUB ACCEPTING in Race Status Channel\n"));
            client.publishRaceStatus(myRace.id, myRace.name, Protocol.PRace.Status.ACCEPTING);
          }          
        }
        // - RESET Client's Attributes Strings for this race (forget myRace) 
        resetMyRaceAttributes();
        
      } else {
        // Is it SOMEBODY ELSE LEAVING MY Race
        if(myRace.id !=null && raceId.equals(myRace.id)) {  
          String rStatus = raceList.getStatus(raceId);
          if(rStatus != null && rStatus.equals( Protocol.PRace.Status.COMPLETE   )) {  
            // Somebody is leaving MY SAME COMPLETED Race (it chooseed LEAVE and not RACE_AGAIN) 
            // The race, and it's configuration,  have no sense anymore ---> Leave
            t.log(Trace.INTERNALS, String.format("manageReceivedRaceParticipants(): Somebody is leaving MY SAME COMPLETED Race -> I LEAVE as well\n"));
            sendLeaveRace(raceId); // Auto-Push my Leave Race button...             
          }
        }
      }
      
      // LAST OPERATION for LEAVE ---> Remove deviceId from race's Participants list 
      t.log(Trace.BASE, String.format("manageReceivedRaceParticipants():REMOVE Participant[%s] from Race[%s]\n", this.recvDevice._id,raceId));
      int removedN = raceList.removePartner(raceId,deviceId)  ;
      if( removedN != 1){  // error on removing TrackId from Race Participants 
        t.error(String.format("manageReceivedRaceParticipants() ERROR: (LEAVING) - Status for [deviceId]=[%s] will not be updated\n", deviceId));
        return(false);
      }
    } // Leave race message

  return(true);
  
  }

  
  /**
   * Message received on <RaceId>/Config
   */  
  boolean manageReceivedRaceConfig(String raceId, Protocol.Network.PDU pdu){
    t.log(Trace.METHOD, String.format("manageReceivedRaceConfig(%s, PDU)\n", raceId));
    
    // Discharge RETAINED MESSAGES
    // In the current implementation in a client gets offline
    // while it was already configured for a race no action
    // is taken to restore the situation
    if(pdu.isRetained) {
      t.error("manageReceivedRaceConfig(): RETAINED [Race Configuration] Message discharged\n");
      return(false);    
    }    
    
    // (1) Decode: get RaceConfigList Obj from PDU
    raceOrder = new RaceConfigList();
    String errStr=Protocol.Network.Channel.RaceConfiguration.decodePDU(pdu, raceOrder);
    if(errStr != null){
      t.error(String.format("manageReceivedRaceConfig():%s", errStr));
      return(false);    
    }

     // (2) process received data (fill start, finish fields)
     boolean ok=raceOrder.process();
     if(!ok){
       return(false);
     }
     t.log(Trace.DETAILED, String.format("manageReceivedRaceConfig(): Processed Configuration:\n%s\n",raceOrder.toString()));

    /***
      At this poit  raceOrder looks like:
       idx  olrId  pos  laps  repeat  start   finish
       ---  -----  ---  ----  ------  -----   ------ 
       [0]  ROM-B   1    2     3       1       1
       [1]  SVQ-A   2    1     2       0       0
       [2]  SVQ-B   3    1     2       0       0
    ***/
    
     // Find my RaceConfig in RaceConfigList
     RaceConfig lCfg = raceOrder.getRaceConfig(this._id);
     if(lCfg == null){
       t.error(String.format("manageReceivedRaceConfig() SOFTWARE ERROR: this._id=[%s] Not found in raceOrder list. \n", this._id));
       return(false);
     }
     if(lCfg.start == 1){
       myRace.startsHere=true;
     }
     
     t.log(Trace.DETAILED, String.format("manageReceivedRaceConfig(): Send Configuration to Device:[%d][%d][%d][%d]\n",lCfg.start,lCfg.laps,lCfg.repeat,lCfg.finish ));
     phRaceDev.setRaceParams(lCfg.start,lCfg.laps,lCfg.repeat,lCfg.finish);
     phRaceDev.sendConfig();

     t.log(Trace.DETAILED, String.format("manageReceivedRaceConfig(): Publish my new Status=CONFIGURING_DEV\n"));    
     client.publishDeviceStatus(Protocol.PDevice.Status.CONFIGURING_DEV);
     
    return true;
  }
  

  
  /**
   * Message received on <RaceId>/Cars/<CarId>
    
     Funzionamento topic per Car quando Car cambia circuito:
       Network:  
         Riceve su questo topic Car.LEAVE, POS=XXX
           UPD Car position 
           Next OLR(XXX) == ME ?
             - Send to Board "Car Arrived"
             - PUB:
                 Car.RACING
                 pos = ME
       
       Serial:  
         Riceve da Board Car.LEAVE
             - PUB:
                 Car.LEAVE
                 pos = ME
    
                 
       
   */  
    boolean manageReceivedRaceCar(String raceId, String payload, boolean isRetained){
    t.log(Trace.METHOD + Trace.DETAILED, String.format("manageReceivedRaceCar():[%s][%s]\n", raceId, payload));

    // check->get [Car] fields from payload
    String errStr=Protocol.Network.Channel.CarStatus.decodeTextPayload(payload, this.recvCar);
    if(errStr != null){
      t.error(String.format("manageReceivedRaceCar(%s,%s):%s", raceId, payload, errStr));
      return(false);    
    }    

    // RETAINED MESSAGE 
    //   In the current implementation in a client gets offline
    //   no action are taken to restore the previous real situation
    //   for races > accepting participants
    if(isRetained ) {
      if(this.recvCar.curDevIsValid()) { // do not log error for messages received on subscribe to a new race (createRace publish on Car/x channels before client subscribe)
        t.error(String.format("manageReceivedRaceCar(): Ignore RETAINED [%s]\n", this.recvCar.toString()));
        return(false);
      }
      return(true);
    }


    // Only for Cars in MY race
    if(myRace.id !=null && raceId.equals(myRace.id)) {
      t.log(Trace.INTERNALS, String.format("manageReceivedRaceCar(): Set car[%d] position=[%s][%s]\n", this.recvCar.id, this.recvCar.curDev, this.recvCar.status));
      
      // Update carPos    
      String err = phRaceDev.setCarCurrentDevice(this.recvCar.id,this.recvCar.curDev,this.recvCar.status);
      if(err != null){
        t.error(String.format("manageReceivedRaceCar(): ERROR %s\n", err));
        return(false);
      }

      // Car.LEAVE ?
      if(this.recvCar.status.equals(Protocol.PCar.Status.LEAVE)) {       
        RaceConfig nextOLR = raceOrder.getNext(this.recvCar.curDev);
        if(nextOLR.olrId.equals(this._id)){  // car is coming here
          t.log(Trace.BASE, String.format("manageReceivedRaceCar(): car[%d] come here at speed[%d] from[%s]--->SEND Cmd CarEnter to Board\n", this.recvCar.id, this.recvCar.speed, this.recvCar.curDev));
          phRaceDev.sendCarEnter(this.recvCar.id, this.recvCar.speed);          
          t.log(Trace.BASE, String.format("manageReceivedRaceCar(): car[%d] is here --->PUB curDev[%s], Status[RACING]\n", this.recvCar.id, this.recvCar.curDev));
          client.publishCarStatus(myRace.id, this.recvCar.id, this.recvCar.name, this._id,Protocol.PCar.Status.RACING,0);
        }
      }

      // Car.WINNER ?
      if(this.recvCar.status.equals(Protocol.PCar.Status.WINNER)) {
        if( ! this.recvCar.curDev.equals(this._id)){  // win message was NOT sent by this OLR  
          phRaceDev.sendCarWin(this.recvCar.id);          
          t.log(Trace.BASE, String.format("manageReceivedRaceCar(): car[%d] Won the race in [%s] OLR--->Send Cmd CarWin to Board\n", this.recvCar.id, this.recvCar.curDev));
        }
      }      
    } else {
      // subscribe to Car topic only for myRace, so this is not suppose to happen
      t.error(String.format("manageReceivedRaceCar(): SOFTWARE ERROR - Received Car from another race (?) Car:[%s]\n", this.recvCar.toString()));    
    }
    return true;
  }
  
         


  /**
   *  Process messages received on Device/Recv topic
   *
   */
  boolean processDeviceRecv(String devId, Protocol.Network.PDU pdu) {
    t.error("[%s] OPSSSSS...: processDeviceRecv() n") ;
    return(false);
  }

  /**
   *  Process messages received on Device/Broadcast topic
   *
   */
  boolean processDeviceBroadcast(Protocol.Network.PDU pdu ) {
    t.error("[%s] OPSSSSS...: processDeviceBroadcast() n") ;
    return(false);
  }



  //////////////////////////////////////////////////////////
  // Methods for Commands received from OLRBoard (Serial) //
  //////////////////////////////////////////////////////////
  
  
  /**
   *  Called on Reception of commands from Board
   *
   */
  private void manageBoardCommand(String cmdStr){
    t.log(Trace.METHOD, String.format("manageBoardCommand(%s)\n", cmdStr));
    
    char cmdId=cmdStr.charAt(0);  // first char = "Command.ID"
    String pars = cmdStr.substring(1); // Command parameter string

    switch(cmdId) {
      case ProtocolSerial.Cmd.SendLog.ID :
        {
          String p[]=ProtocolSerial.Cmd.SendLog.getParam(pars);
          if(p != null) {
            if(ProtocolSerial.Cmd.SendLog.isError(p[0])){
              t.error(String.format("manageBoardCommand(): Error message from OLRBoard:[%s] \n",p[1]));
            } else {
              int correspondigLogLevel = ProtocolSerial.Cmd.SendLog.getLogLevel(p[0]);
              t.log(correspondigLogLevel, String.format("manageBoardCommand(): Log message from OLRBoard:[%s] \n",p[1]));
            }
          } else {
            t.error(String.format("manageBoardCommand():[%s] Error decoding LOG message from OLRBoard:[%s] \n", this._id,pars));
          }
        }
        break;
        
      case ProtocolSerial.Cmd.SetConfig.ID :  // Answer to a SetConfig sent from Host
        if(pars.equals(ProtocolSerial.Cmd.OK)){
          client.publishDeviceStatus(Protocol.PDevice.Status.CONFIGURED);
        } else {
          // Pub error on Device Status
          client.publishDeviceStatus(Protocol.PDevice.Status.CONFIG_ERROR);
          // Pub error on Race Status
          client.publishRaceStatus(myRace.id, myRace.name, Protocol.PRace.Status.CONFIGDEV_ERROR);
        }
        break;      
      
      case ProtocolSerial.Cmd.RacePhase.ID :  // Race Phase Command  
        manageBoardRacePhase(pars);
        break;
        
      case ProtocolSerial.Cmd.CarTelemetry.ID :
        {
          String p[]=ProtocolSerial.Cmd.CarTelemetry.getParam(pars);
          if(p != null){
            int carId=Integer.valueOf(p[0]);
            int lap=Integer.valueOf(p[2]);
            int rPos=Integer.valueOf(p[3]);
            int batt=Integer.valueOf(p[4]);
            if(this.sendTelemetry) {  
              client.publishCarTelemetry(carId, p[1], lap, rPos, batt);
            }
          }
        }
        break;

      case ProtocolSerial.Cmd.CarLiving.ID :
        {
          int carId=Integer.valueOf(pars);  
          Car car = phRaceDev.getCar(carId);
          if(car!=null)  {
            client.publishCarStatus(myRace.id, car.id, car.name, this._id,Protocol.PCar.Status.LEAVING,0);
            t.log(Trace.BASE, String.format("manageBoardCommand(): PUB Car[%d][%s] LEAVING\n", car.id, car.name));
          } else {
            t.error(String.format("manageBoardCommand() ERROR: Invalid carId:[%s] LEAVING\n", pars));
          }
        }          
        break;
 
      case ProtocolSerial.Cmd.CarLeave.ID :
        {
          int p[]=ProtocolSerial.Cmd.CarLeave.getParam(pars);
          if(p != null){
            int carId=Integer.valueOf(p[0]);
            int speed=Integer.valueOf(p[1]);
            Car car = phRaceDev.getCar(carId);
            if(car!=null)  {
              t.log(Trace.BASE, String.format("manageBoardCommand(): PUB Car[%d][%s] LEAVE at Speed[%d]\n", car.id, car.name, speed));
              client.publishCarStatus(myRace.id, car.id, car.name, this._id,Protocol.PCar.Status.LEAVE,speed);
            } else {
              t.error(String.format("manageBoardCommand() ERROR: Invalid carId:[%s] LEAVE\n", pars));
            }
          } else {
            t.error(String.format("manageBoardCommand() ERROR: ProtocolSerial.Cmd.CarLeave.getParam() returns NULL\n"));
          }
        }  
        break;

      case ProtocolSerial.Cmd.CarWin.ID :
        {
          int carId=Integer.valueOf(pars);  
          Car car = phRaceDev.getCar(carId);
          if(car!=null)  {
            client.publishCarStatus(myRace.id, car.id, car.name, this._id,Protocol.PCar.Status.WINNER, 0); // speed = 0
            t.log(Trace.BASE, String.format("manageBoardCommand(): PUB Car[%d][%s] WIN\n", car.id, car.name));
          } else {
            t.error(String.format("manageBoardCommand() ERROR: Invalid carId:[%s] WIN\n",pars));
          }
        }
        break;

      case ProtocolSerial.Cmd.CarEntering.ID :    
      case ProtocolSerial.Cmd.CarEnter.ID :    
        t.log(Trace.BASE, String.format("manageBoardCommand() ERROR: Unexpected command car[%s] Entering/Enter received\n",pars));
        break;
        
      case ProtocolSerial.Cmd.Handshake.ID :
        // Ignore extra handshake answers received (in connect Host may send more than one before Board get ready and answer)
        break;
        
      default :    
        t.log(Trace.BASE, String.format("manageBoardCommand() ERROR: Unexpected command [%c][%s] received\n",(char)cmdId,pars));
        break;
    }
  }
  
  /**
   *  manage Race Phase Command 
   *
   */
  void  manageBoardRacePhase(String phase){
    t.log(Trace.METHOD, String.format("manageBoardRacePhase():->phase[%s]\n", phase));
    
    
    if(phase.equals(ProtocolSerial.Cmd.OK)){  
      t.log(Trace.DETAILED, String.format("manageBoardRacePhase():Race acknowledge:[%s] discharged\n", phase)); 
      return;
    }
    
    String code=ProtocolSerial.Cmd.RacePhase.phase.validateCode(phase);
    if(code == null){
      t.error(String.format("manageBoardRacePhase(): invalid phase code:[%s]\n", phase));
      return;
    }
  
    switch(code) {
      case  ProtocolSerial.Cmd.RacePhase.Phase.CFGCOMPLETE: 
        // Ignored. The 
        //     publishDeviceStatus(Protocol.PDevice.Status.JValue.CONFIGURED.val);
        // is sent on reception of the "COK" (ACK Answer from the board to a C (config) command)
        break;
        
      case  ProtocolSerial.Cmd.RacePhase.Phase.READY: // From Host
        // Sent by the Host when every participant reach the CFGOK status 
        // Should not receive a READY command from Board
        t.error(String.format("manageBoardRacePhase(): SOFTWARE ERROR: received an unexpected READY code[%s] from Board\n", code)); 
        break;
        
      case ProtocolSerial.Cmd.RacePhase.Phase.COUNTDOWN: // From Both
        // the connected Board is the one starting the race 
        // and it just sent the COUNTDOWN phase message
        if(myRace.startsHere) {
          t.log(Trace.BASE, String.format("manageBoardRacePhase(): Recv COUNTDOWN code[%s]\n", code));
          client.publishRaceStatus(myRace.id, myRace.name, Protocol.PRace.Status.COUNTDOWN);
        } else {
          t.error(String.format("manageBoardRacePhase(): BOARD SENT a COUNTDOWN code[%s] but race does not starts here - Discharged\n", code));
        }
        break;
        
      case  ProtocolSerial.Cmd.RacePhase.Phase.RACING:
        // same as above
        t.log(Trace.BASE, String.format("manageBoardRacePhase(): Recv RACING code[%s]\n", code));
        client.publishRaceStatus(myRace.id, myRace.name, Protocol.PRace.Status.RACING);
        break;
        
      case  ProtocolSerial.Cmd.RacePhase.Phase.PAUSED:
        t.error(String.format("manageBoardRacePhase() SOFTWARE ERROR: unmanaged PAUSED code[%s] received \n", code));
        break;

      case  ProtocolSerial.Cmd.RacePhase.Phase.RESUME:
        t.error(String.format("manageBoardRacePhase() SOFTWARE ERROR: unmanaged RESUME code[%s] received \n", code));
        break;

      case  ProtocolSerial.Cmd.RacePhase.Phase.COMPLETE:
        t.log(Trace.BASE, String.format("manageBoardRacePhase():Recv COMPLETE code[%s]\n", code));
        client.publishRaceStatus(myRace.id, myRace.name, Protocol.PRace.Status.COMPLETE);
        break;
        
      default:
        t.error(String.format("manageBoardRacePhase() SOFTWARE ERROR: unmanaged valid code:[%s]\n", code));
        break;
    }

    // Set new Phase in RaceDevice
    phRaceDev.setRacePhase(code);
    
    return;
  }
 
} // NetworkDevice

  //<>// //<>// //<>// //<>//
class SubscriptionRequest {

  class Type {
    public static final String SUBSCRIBE   = "S";
    public static final String UNSUBSCRIBE = "U";
  }
  
  String topic;
  int qos;
  String operation;
  
  SubscriptionRequest(String _topic) {
    this.qos = Protocol.MQTT_QOS.AT_MOST_ONCE; 
    this.topic = _topic;
    this.operation = Type.SUBSCRIBE;
  }
  
  SubscriptionRequest(String _topic, int _qos) {
    this.qos = _qos; 
    this.topic = _topic;
    this.operation = Type.SUBSCRIBE;
  }
  
  SubscriptionRequest(String _topic, String type) {
    this.qos = 0; 
    this.topic = _topic;
    this.operation = type;
  }
  
} 

class Location {

    String address = null;
    String locality = null;
    String city = null;
    String state = null;
    String zip = null;
    String country = null;
    String countryCode = null;
    String lat = null;
    String lon = null;

    Location () {;}

}
