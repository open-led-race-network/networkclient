/*
*  PhysicalDevice: Store device properties (name, index, CommSpeed, ...) 
*    and the Car LIST contained in the device.
*  Car is defined as an INNER CLASS
*/

class PhysicalDevice {

  private String boardId = null;
  private String name = null;
  private String description = null;
  private int    boardBrate = 0;
  private int    handshakeTimeout=2000; // default val
  private int    lookupTimeout=1400; // default val
  private int    openToSendDelay=1200; 

  SerialPort     serialPort = null;  // Serial Port of the OLR Board
  SerialPortList serialPortList; // Class used to get list of Available (Valid) OS serial ports 
  SerialPort     ports[];        // List of available serial ports 

  String  port=null;
  String  portProperties=null;

  public  boolean isSearching=false; // SET in LocalClient.draw() and RESET here after a complete unsuccesful scan of any available ports
  
  /* 
   * Vars managing the "connect to device" phase 
   */
  // deviceFound: true after the Configured "Device ID" is found on a Port (step 1)
  private boolean deviceFound=false;  
  
  // isPortValid: true after checking if the configured DeviceId found 
  // on step 1, runs the correct firmware version (step 2)
  public  boolean isPortValid=false;    
  
  // true after succesfull handshake on the Port where the configured 
  // device was found (step 1+2)
  public  boolean isConnected=false;  

  
  private double lastHandshakeAttempt;  // System.currentTimeMillis()

  private double lastPortLookupTime;  // System.currentTimeMillis()
  private int lastLookupIdx; 
  private SerialPort lastLookupPort;
  private int lastLookupListLength;
  private char cmdBuf[];

  private double lastPortOpenTime;  // System.currentTimeMillis()
  private boolean querySent;


  // used in readCmd()
  protected final static int READBUFFERSIZE = 120;
  private char    serialBuffer[];
  private int     sbufIdx;
  private boolean sbufBitEncoded;
  
  private Trace t;
  private static final String _CLASS_ = "PhysicalDevice";

  public  Car[] carList;

  /**
   * Constructor
   */
  PhysicalDevice (String sname, JSONObject pd, String logStr)   {   
    
    this.boardId     = getJSONStringOrExit(pd,"id");
    this.name        = getJSONStringOrExit(pd,"name");
    this.description = trim(pd.getString("description" , "-"));
    this.boardBrate  = pd.getInt("baudRate");  // fare util ..OrExit() per Int
    
    this.handshakeTimeout = pd.getInt("HandshakeTimeout"); 
    this.lookupTimeout  = pd.getInt("LookupTimeout");  
    this.openToSendDelay =  pd.getInt("OpenToSendDelay");  
    
    if(logStr.equals("-") ){
      t = new Trace(Trace.NONE);
    } else {
      t = new Trace(logStr);
    }    
    t.setPublisherId(this.boardId);
    t.setClassId(_CLASS_);
    t.log(Trace.METHOD, String.format("Constructor(%s, %s, %s, %d)\n",this.boardId,this.name,this.description,this.boardBrate));

    // Create Cars Array       
    JSONArray cars = getJSONArrayOrExit(pd, "Cars");
    int carsNum = cars.size();
    carList = new Car[carsNum];
    for (int j = 0; j < carsNum; j++) {
       // Get each Car object in the array
       JSONObject car = cars.getJSONObject(j);
       // get name, id
       int carid = car.getInt("id");       // fare util ..OrExit() per Int
       String carname = getJSONStringOrExit(car,"name"); 
       carList[j] = new Car (carid, carname);
    }

    serialBuffer = new char[READBUFFERSIZE];
    sbufIdx=0;
    cmdBuf = new char[READBUFFERSIZE];
    
    lastPortLookupTime = 0;  
    lastLookupIdx  = -1; 
    lastLookupPort = null;
    lastLookupListLength = 0;

    this.lastPortOpenTime = 0;
    this.querySent = false;

    // used in findBoard() 
    serialPortList = new SerialPortList (this.boardId,logStr);
    ports = serialPortList.getPortList();  // Get list of VALID Ports

  }

  /**
   *  Search for OLR Board on Available serials
   *  @param logStr :  LogLevel for SerialList Obj
   *
   *  Please Note:
   *    LocalClients.draw() calls this findBoard() method in a loop.
   *    This calls are Coordinates between NetworkDevice.RaceDevice
   *    configured in the JSON cfg file, TO HAVE ONLY ONE RaceDevice
   *    at a time doing a COMPLETE scan of any available port.
   *    The attribute [this.isSearching] is used to communicate to the
   *    caller that a complete port scan has been done.
   *   
   *    NOTE: Serial Ports on Windows
   *     On Windows, a call to openPort() make the 
   *     Arduino Board to RESET, even without s.clearDTR()
   *     AND (or maybe cause of this) openPort() takes 
   *     up to 600 mSec to complete 
   *   
   *     This means we need to adjust JSON CFG PARAMETERS:
   *       LookupTimeout   = 2000  or more 
   *       OpenToSendDelay = 1200 or more
   *     To let Arduino reset and than send
   *     the query commands (get "BoardID" + get "Board Software Version")
   */
  void findBoard() {

    if(lastPortLookupTime==0 || ( System.currentTimeMillis() > lastPortLookupTime + this.lookupTimeout)) {  
      this.lastPortLookupTime = System.currentTimeMillis();

      // in case the device was found but with a wrong software version
      this.deviceFound=false; 

      // first call or reset after unsuccesful check on any available port
      if(lastLookupIdx == -1) { 
        if(lastLookupPort != null && lastLookupPort.isOpen()) { 
          t.error(String.format("findBoard(): SHOULD NEVER HAPPEN... - CLOSING PORT [%s]\n", lastLookupPort.getSystemPortName() ));
          lastLookupPort.closePort();
        }
        lastLookupListLength = serialPortList.reloadSerialPortList(); // (re)Load current serial port list
        ports = serialPortList.getPortList();  // Get list of VALID Ports
        t.log(Trace.LOWLEVEL, String.format("findBoard(): (re)Load port list:\n%s", serialPortList.toString()));
        lastLookupIdx = 0;
      }  else {
        t.log(Trace.LOWLEVEL, String.format("findBoard(): Timeout -> Check next port\n"));
        lastLookupIdx ++;
      }
      
      // all of the available ports checked
      if(lastLookupIdx >= lastLookupListLength ) { 
        t.log(Trace.LOWLEVEL, String.format("findBoard(): Any available port checked\n"));
        if(lastLookupPort != null && lastLookupPort.isOpen()) { 
          t.log(Trace.LOWLEVEL, String.format("findBoard(): DONE - CLOSING PORT [%s]\n", lastLookupPort.getSystemPortName() ));
          lastLookupPort.closePort();
        }
        lastLookupIdx = -1; // reset - reload list on next call
        this.isSearching = false; // DONE with a complete scan of any available ports
        return;
      }

      // Close previous port
      if(lastLookupPort != null && lastLookupPort.isOpen()) { 
        t.log(Trace.LOWLEVEL, String.format("findBoard(): 2 - CLOSING PORT [%s]\n", lastLookupPort.getSystemPortName() ));
        lastLookupPort.closePort();
      }
      
      // Open Serial Port
      lastLookupPort = ports[lastLookupIdx];      
      String err = openSerialPort (lastLookupPort, this.boardBrate);  
      if(err != null){  
        t.log(Trace.LOWLEVEL, err);
        t.log(Trace.LOWLEVEL, String.format("   findBoard(): Port Skipped\n"));
        lastPortLookupTime=0; // Next available port 
        return;  
      }
      t.log(Trace.INTERNALS + Trace.DETAILED + Trace.LOWLEVEL, String.format("findBoard(): Check Port [%s]\n", lastLookupPort.getSystemPortName() ));
      this.lastPortLookupTime = System.currentTimeMillis(); // On Windows OPEN a Serial port takes more than 500 mSec
      this.lastPortOpenTime = System.currentTimeMillis(); 
      this.querySent = false;                               // will send Get UID command to board on the selected port
    } 

    // If there is currently NO port open looking for this device  => returns
    //   This happens when, for example, there is another configured PhysicalDevice 
    //   doing the same (looking for its port) and only one device phisically connected
    if(lastLookupPort == null){
      return;
    }
    
    if (! this.querySent && (System.currentTimeMillis() > lastPortOpenTime + this.openToSendDelay) ) {
      this.querySent = true;

// 2023-08-14 - manda ENTER CFG alla board prima del comando (interrompe race active...)     
byte cmdcfg[]={ProtocolSerial.Cmd.EnterCfgMode.ID,  ProtocolSerial.EOC};   
t.log(Trace.BASE, String.format("findBoard(): >>> >>> >>> SEND command [ENTER CFG][%s] to BOARD on Port [%s]n", new String(cmdcfg) ,this.port));
sendCmdToBoard(lastLookupPort,cmdcfg);


      byte cmd[]={ProtocolSerial.Cmd.GetUID.ID,  ProtocolSerial.EOC};      // Send "Get Board ID" command to board
      sendCmdToBoard(lastLookupPort,cmd);
    }
    
    /* look for answers from the port currently checked
     * ------------------------------------------------
     */
    int bytesRead = readCmd(cmdBuf,lastLookupPort);

    // Cleanup commands sent from board not already in CONFIG mode 
    // Board starts always in standalone mode and may have autostart mode active
    // or be in a race (demo mode), etc...
    if(! this.querySent && bytesRead != 0) {
        char abuf[] = new char[bytesRead];
          for(int i=0;i<bytesRead;i++){
            abuf[i]=cmdBuf[i];
          } 
        t.log(Trace.LOWLEVEL, String.format("findBoard(): QUERY NOT SENT - DISCHARGED [%d]bites:[%s] from board on Port [%s]\n",bytesRead,new String(abuf), this.port));
        bytesRead = 0;
    }
    // Cleanup ENTER CONFIG OK sent from board 
    if(bytesRead != 0) {
      if(discardEnterConfigOK(cmdBuf,bytesRead)) {
        bytesRead = 0;
        t.log(Trace.BASE, String.format("findBoard():DISCHARGED [@OK] from board on Port [%s]n", this.port));
      }
    }
     
      
    if(bytesRead != 0) {
      // step 2: check if the found configured board runs the correct firmware version
      if(this.deviceFound) {

        char verBuf[] = new char[bytesRead -1];
        for(int i=0;i<(bytesRead-1);i++){
          verBuf[i]=cmdBuf[i+1];
        } 
        if(isFirmwareVersionOK(cmdBuf,bytesRead)){
          // Firmware OK -> DONE
          t.log(Trace.BASE, String.format("findBoard(): BOARD on Port [%s] RUNS Firmware [%s] -> OK\n", this.port, new String(verBuf)));
          this.isSearching = false; // Free Scan Port token
          this.isPortValid=true;

// 2023-08-14 - Firmware unificato passa in modo "Network" inviando un comando ('n')     
byte cmd[]={ProtocolSerial.Cmd.EnterNetMode.ID,  ProtocolSerial.EOC};   
t.log(Trace.BASE, String.format("findBoard(): >>> >>> >>> SEND command [n] to BOARD on Port [%s]n", this.port));
sendCmdToBoard(lastLookupPort,cmd);
          
          
        } else {
          // Incorrect Software version - free the Port for other PhysicalDevices looking for the board
          t.error(String.format("findBoard(): INCOMPATIBLE Board FIRMWARE VERSION=[%s] - Required=[%d.%d.%d] - Closing Port [%s]\n",
                                                  new String(verBuf),
                                                  req_firmware_version[0],
                                                  req_firmware_version[1],
                                                  req_firmware_version[2],
                                                  lastLookupPort.getSystemPortName()));
          lastLookupPort.closePort();
          lastLookupPort = null;
        }
        
      // Step 1: check if board on lookupPort is the one configured for this device
      } else {
        if (isThisBoard(cmdBuf,bytesRead) ){  
          // Board FOUND on lastLookupPort port -> next phase (check board software ver)
          serialPort = lastLookupPort;
          this.port = serialPort.getSystemPortName();
          this.portProperties = serialPort.getPortDescription();
          t.log(Trace.BASE, String.format("findBoard(): BOARD FOUND ON PORT [%s]\n", this.port));

          this.deviceFound=true;
          // Send "Get Software version" command to board
          byte cmd[]={ProtocolSerial.Cmd.GetSoftwareVersion.ID,  ProtocolSerial.EOC};   
          sendCmdToBoard(lastLookupPort,cmd);
          return;
        } else {
          // Not my board - free the Port for other PhysicalDevices looking for the board
          t.log(Trace.LOWLEVEL, String.format("findBoard(): NOT My Board, Closing Port [%s]\n", lastLookupPort.getSystemPortName()));
          lastLookupPort.closePort();
          lastLookupPort = null;
        }
      }
      
    }
    return;
  }


  /**
   *
   */
  void sendCmdToBoard (SerialPort sp, byte cmd0[]) {  
    try {
      // Read buffered bytes and discard them
      if(sp.bytesAvailable() > 0) {
        byte[] readBuffer = new byte[sp.bytesAvailable()];
        int numRead = sp.readBytes(readBuffer, readBuffer.length);
        t.log(Trace.INTERNALS, String.format("sendCmdToBoard(): discharged [%d] input bytes:[%s]\n", numRead, new String(readBuffer)));
      }
      // Send command  
      sp.writeBytes(cmd0,cmd0.length);
      t.log(Trace.LOWLEVEL, String.format("sendCmdToBoard(): WRITE [%s] on [%s]\n", new String(cmd0),sp.getSystemPortName()  ));

    } catch (Exception e) { e.printStackTrace(); }
    return;
  }
  
  
  
  /**
   *
   *
   */
   boolean isThisBoard(char cmdBuf[], int bytesRead) {
      char buf[] = new char[bytesRead];
      for(int i=0;i<bytesRead;i++){
        buf[i]=cmdBuf[i];
      } 
      t.log(Trace.LOWLEVEL, String.format("isThisBoard(): got cmdBuf:[%s]\n", new String(buf)));

      if(ProtocolSerial.Cmd.GetUID.ID == (buf[0])) {
        String getId = String.valueOf(buf).substring(1);
        t.log(Trace.LOWLEVEL, String.format("findBoard(): got BoardID:[%s]\n", getId));
         
        // Check if received string is the id we're looking for      
        if(getId != null && getId.equals(this.boardId)){
          return(true); // is this board
        } 
      } else {
        t.log(Trace.LOWLEVEL, String.format("isThisBoard(): NOT a GetUID command:[%s]\n", (char) (buf[0])));
      }
      return(false);
  }  
  
 
  /**
   *
   *
   */
   boolean isFirmwareVersionOK(char cmdBuf[], int bytesRead) {

    char buf[] = new char[bytesRead];
      for(int i=0;i<bytesRead;i++){
        buf[i]=cmdBuf[i];
      } 
      t.log(Trace.LOWLEVEL, String.format("isFirmwareVersionOK(): got cmdBuf:[%s]\n", new String(buf)));

      if(ProtocolSerial.Cmd.GetSoftwareVersion.ID == (buf[0])) {
        String getVer = String.valueOf(buf).substring(1);
        t.log(Trace.LOWLEVEL, String.format("isFirmwareVersionOK(): got Version:[%s]\n", getVer));
         
        // Check if received Software Version is right 
        int boardVer[]=ProtocolSerial.Cmd.GetSoftwareVersion.getParam(getVer);
        if( req_firmware_version[0] == boardVer[0] &&
              req_firmware_version[1] <= boardVer[1]) {
          return(true);
        }
        return(false);
      } else {
        t.log(Trace.LOWLEVEL, String.format("isFirmwareVersionOK(): NOT a GetSoftwareVersion command:[%s]\n", (char) (buf[0])));
      }
      return(false);
      
  }  
 
  
  /**
   *
   *
   */
   boolean discardEnterConfigOK(char cmdBuf[], int bytesRead) {

    char buf[] = new char[bytesRead];
      for(int i=0;i<bytesRead;i++){
        buf[i]=cmdBuf[i];
      } 
      t.log(Trace.LOWLEVEL, String.format("discardEnterConfigOK(): got cmdBuf:[%s]\n", new String(buf)));

      if(ProtocolSerial.Cmd.EnterCfgMode.ID == (buf[0])) {
        String answ = String.valueOf(buf).substring(1);
        t.log(Trace.LOWLEVEL, String.format("discardEnterConfigOK(): got [%s]\n", answ));
          return(true);
      }
      return(false);
  }  
 
 
 
  /**
   *  Opens the Phisical Device's Serial port
   *
   *  PLASE NOTE:
   *    On Windows, openPort() take up to 600 mSec 
   *    AND the Arduino Board get RESET even without
   *    call s.clearDTR().
   *  
   *    This means we'll need to adjust:
   *      lookupTimeout=1400;
   *      openToSendDelay=1200;
   *    To let Arduino reset and than send
   *    the query string.
   *
   *  @param port :  OS device port name ("COM[*]" on Windows - "/dev/tty[*]" on Linux)
   *  @param properties : OS port description 
   *  @param baudRate 
   */
    String openSerialPort (SerialPort s, int bRate) {
      String pName = s.getSystemPortName();
      t.log(Trace.LOWLEVEL, String.format("openSerialPort(%s,%d) AT MILLIS:[%d]\n", pName, bRate, System.currentTimeMillis()));
      try {
        boolean ok=s.openPort(ProtocolSerial.SAFETYSLEEPTIME, ProtocolSerial.DEVICESENDQUEUESIZE, ProtocolSerial.DEVICERECVQUEUESIZE);
        //boolean ok=s.openPort();
        if(! ok){
          return(String.format("openSerialPort() Error opening port:[%s] - Already in use\n", pName));
        }
        s.setComPortTimeouts(SerialPort.TIMEOUT_NONBLOCKING, 0, 0);
        s.setBaudRate(bRate);
        
        s.clearDTR(); // !!! RESET ARDUINO BOARD -- ON WINDOWS THE BOARD RESET ANYWAY on openPort() !!!=
        
    } catch (SerialPortInvalidPortException ex) {
      return(String.format("openSerialPort() Error opening:[%s]\n\t%s", pName, ex));
    }           
    this.lastHandshakeAttempt = 0 ;  // System.currentTimeMillis()    
    sbufIdx=0;
    t.log(Trace.LOWLEVEL, String.format("openSerialPort(%s,%d) DONE AT MILLIS:[%d]\n", pName, bRate, System.currentTimeMillis()));
    return(null);
  }

  boolean isConnected(){
    return(this.isConnected);
  }

  boolean isPortValid(){
    return(this.isPortValid);
  }

  /**
   * connect() - Handshake with OLR Board 
   */
  void connect() {

    if(lastHandshakeAttempt==0 || ( System.currentTimeMillis() > lastHandshakeAttempt + this.handshakeTimeout)) {  
      this.lastHandshakeAttempt = System.currentTimeMillis();
      try { // Send HANDSHAKE to Board
      //byte cmd[] = {ProtocolSerial.Cmd.Reset.ID,ProtocolSerial.EOC,ProtocolSerial.Cmd.Handshake.ID,ProtocolSerial.EOC};
      byte cmd[] = {ProtocolSerial.Cmd.Handshake.ID,ProtocolSerial.EOC};
        serialPort.writeBytes(cmd,cmd.length);
        t.log(Trace.DETAILED, String.format("connect(): Handshake sent to port:[%s]\n", this.port));
      } catch (Exception e) { e.printStackTrace(); }
    }
    
    if(serialPort.bytesAvailable() > 0) {
      byte[] in=new byte[1];
      int nb = serialPort.readBytes(in,1); // read 1 byte
      if(nb > 0 && in[0] == ProtocolSerial.Cmd.Handshake.ID) {
        t.log(Trace.LOWLEVEL, String.format("connect(): Port[%s]->Received HANDSHAKE [%c]\n", this.port, (char)in[0]));
        byte[] in2=new byte[1];
        nb = serialPort.readBytes(in2,1); // read 1 byte
        t.log(Trace.LOWLEVEL, String.format("connect(): Port:[%s]->Received char [%c]\n", this.port, (char)in2[0]));
        if(nb > 0 && in2[0] == ProtocolSerial.EOC) {
          // ??
          byte cmd[] = {ProtocolSerial.Cmd.Handshake.ID,ProtocolSerial.EOC};
          serialPort.writeBytes(cmd,cmd.length);
          
          // Provvisorio per vedere se rientra in cfg al riaccendere il prg processing e non arduino...
          byte cmd2[] = {ProtocolSerial.Cmd.RacePhase.ID,(byte)ProtocolSerial.Cmd.RacePhase.Phase.ENTERCFG.charAt(0),ProtocolSerial.EOC};
          serialPort.writeBytes(cmd2,cmd2.length);
          
          isConnected=true;
          t.log(Trace.BASE, String.format("connect(): CONNECTED TO BOARD on port:[%s]\n", this.port));
        }
      } else {
        ;t.log(Trace.LOWLEVEL, String.format("connect(): Port:[%s]->Received unexpected char [%c] in Handshake phase\n", this.port, (char)in[0]));
      }
    }
  }
  
  /**
   *  Write a string in the Serial Port 
   */
  protected void sendString(String str) {
    if(  ! isConnected){
      t.error(String.format("sendString() SOFTWARE ERROR: NOT connected to a port\n" ));
      return;
    }
    byte b[] = str.getBytes();
    try {
    //  byte cmd[] = cmdStr.toString().getBytes();
      serialPort.writeBytes(b,b.length);
      t.log(Trace.LOWLEVEL, String.format("sendString(): [%s] sent to port [%s] \n", new String(b), this.port));
    } catch (Exception e) { e.printStackTrace(); }
  }
 
 
  /**
   * Read a "Command" from the Serial Port and
   * write it into buf[]
   * Commands are defined in ProtocolSerial and are always
   * strings where the FIRST char is the CommandId and
   * the following chars, UP TO "End Of Command" char
   * represents the parameters (if any)
   *
   * @param   buf[] buffer where command characters get stored
   * @param   sp SerialPort to read from
   *
   * @return  the Number of valid chars written into buf[]
   * @return  0 if no complete commad is available at the moment
   * @return -1 on buffer sire error
   */
  protected int readCmd(char buf[], SerialPort sp) {
    
    if(sp.bytesAvailable() > 0) {
      if(sbufIdx < READBUFFERSIZE - 2) {
         byte[] in=new byte[1];
         int nb = sp.readBytes(in,1); // read 1 byte
         
         if(sbufIdx==0) { // first byte: Command Id
           sbufBitEncoded=false;
           if(in[0] == ProtocolSerial.Cmd.CarLeave.ID) { // Car Leave: 2nd byte is bit-encoded
              sbufBitEncoded=true;
           }
         }
         
         if(nb > 0 && in[0] == ProtocolSerial.EOC) {
             int readCmdSize=sbufIdx;
             sbufIdx=0;
             for(int i=0;i<readCmdSize;i++){
               buf[i]=serialBuffer[i];
             }
             if(sbufBitEncoded) {
               t.log(Trace.LOWLEVEL, String.format("readCmd(): port:[%s]->Received [EOC]-> BitEncoded buf[%c][%s][EOC] (size:%d)\n\n", sp.getSystemPortName(), serialBuffer[0], Integer.toBinaryString(Integer.valueOf((int)serialBuffer[1])) ,readCmdSize));
             } else {
               t.log(Trace.LOWLEVEL, String.format("readCmd(): port:[%s]->Received [EOC]->buf[%s] (size:%d)\n\n", sp.getSystemPortName(), new String(serialBuffer,0,readCmdSize),readCmdSize));               
             }
             return(readCmdSize);
         } else {
             if(sbufBitEncoded) {
               t.log(Trace.LOWLEVEL, String.format("readCmd(): port:[%s]->Received [%s] add to buffer[%d]\n", sp.getSystemPortName(), Integer.toBinaryString(Integer.valueOf((char) in[0])), sbufIdx));
             } else {
               t.log(Trace.LOWLEVEL, String.format("readCmd(): port:[%s]->Received [%c] add to buffer[%d]\n", sp.getSystemPortName(), (char) in[0],sbufIdx));   
             }
             serialBuffer[sbufIdx++] = (char) in[0];
             return(0);
         }
      }else {
        t.error(String.format("readCmd():Command String length=[%d] > READBUFFERSIZE=[%d]\n", sbufIdx, READBUFFERSIZE));
        return(-1);
      }
    }
    return(0);
  }  
  
  /**
   * calls readCmd on the PhysicalDevice serial port   
   */
  protected int readCmd(char buf[]){
    return(readCmd(buf, this.serialPort));
  } 


  /**
   *  get Car [idx] from carList 
   */
  int getCarIdx(int carId) {
    for (int i=0; i<carList.length ; i++) {
      if( carList[i].id == carId){
        return(i);
      }
    }
    return(-1);
  }
  
  /**
   *  set Current Device for Car [idx] in carList
   *    (which OLRDevice contains the car now)  
   */  
  String setCarCurrentDevice(int carId, String dev, String s) {
    int idx=getCarIdx(carId);
    if(idx==-1){
      return(String.format("setCarCurrentDevice(): unknown carId:[%d]",carId));
    }
    carList[idx].curDev=dev;
    carList[idx].status=s;
    return(null);
  }

  /**
   *  get Current Device for Car [idx] in carList
   *    (which OLRDevice contains the car now)  
   */  
  String getCarCurrentDevice(int carId) {
    int idx=getCarIdx(carId);
    if(idx==-1){
      return(null);
    }
    return(carList[idx].curDev);
  }
  
  /**
   *  get Car Object at carList[idx] 
   */    
  Car getCar(int carId){
    int idx=getCarIdx(carId);
    if(idx==-1){
      return(null);
    }
    return(carList[idx]);
  }
 
} // PhysicalDevice
