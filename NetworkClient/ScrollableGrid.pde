/***
  Based on "Chrisir" post on
    https://forum.processing.org/two/discussion/12462/how-to-view-table-in-processing\
***/
/**************************************************************************************************************

  Class ScrollableGrid
  ====================
  Display data in Tabular format (Rows/Cols) in a user defined screen area.
  Manage scroll if data does'nt fit into the area.
  A cell may contain simple TEXT or a G4P Button
  It magage dataset where each Row may contains a different number of columns 
  It allows to change the dataset dinamically (not only in setup() )
  
  Tipical Usage:
  --------------
  <code>
    String lines[] = { 
      "1;11;12;Test 1",
      "2;22;23;Test Test 2", 
      "3;32;33;Test 3", 
      "4;42;43;Test 4", 
      "5;52;53;Test Test Test Test Test",
    };
    
    void setup() {
        // Creates the ScrollableGrid OBJECT
        sGrid = new ScrollableGrid(this,20,20,650,280);
        // Create the Table (Cells matrix)
        sGrid.fillGrid( String lines[] );   
    }
    
    void draw () {
      // Show the grid 
      sArea.showGrid();
    } 
    
    // manage SCROLL via KEYS (UP,DOWN,LEFT,RIGHT)
    void keyPressed() {
      sGrid.defaultKeyEvent(keyCode); 
    }
    // Intercept Mouse wheel and calls the ScrollableGrid.scroll()method 
    void mouseWheel(MouseEvent event) {
      sGrid.scroll(event.getCount());
    }
  </code>
  


  Constructor & Destructor
  ========================
  
     ScrollableGrid(PApplet p, float x1, float y1, int w, int h)
     -----------------------------------------------------------
       p  = PApplet of the calling Sketch
       x1 = x-coordinate of the View Area  
       y1 = y-coordinate of the View Area 
       w  = width of the View Area
       h  = height of the View Area
       
    void dispose() 
    --------------
        This will remove all references to this object and contained ones
        The user is responsible for nullifying all references to this object in their sketch code.
        
        For example if you want to dispose a ScrollableGrid created with:
          sGrid = new ScrollableGrid(this,20,20,650,280);
  
        you need to:
          sGrid.dispose(); 
          sGrid = null;
          
          
  Public Member Functions:
  ========================
 
    void fillGrid(String lines[])
    -----------------------------

         Fills the grid with values from String array passed as a parameter
         Each string in the array (row) is expected to contain a field 
         seaparator (ex: ";") used to split the string in columns

         This function may be called more than once when the dataset to 
         display changes.
         When this happens fillGrid() automatically dispose() the previous dataset. 
         
         Please note:
          The X,Y OFFSET values for the visualization DON'T get reset by 
          this function.
          This is useful if the dataset changes when the user was scrolling it.
          This way the Visualization does not get back to 0,0 automatically.
      
          We can always call resetView() right after fillGrid() if we want
          to reset the visualization to 0,0.  
      
         String lines[] FORMAT:
            - Fields in a single row have to be separated by ";"
            - A field Containing the keyword "BUTTON" (uppercase) contains a button definition.
              - Buttons are defined by 6 subfields, separated by ","
                  BUTTON, ButtonText, ButtonTag, ButtanTagNum, width, height
                  Example:
                    list[4] = "Text Field 1 ; Text Field 2 ; BUTTON, ButtonText, ButtonTag, 11, 80, 20 "
                    
               The main Sketch will get the BUTTON event via:
                 public void handleButtonEvents(GButton button, GEvent event) {
                    println("GButton: " + button.getText() +  "  tag:" +  button.tag + "  tagNo:" +  button.tagNo + " >> GEvent." + event );
                 } 

            - A field Containing the keyword "TEXTFIELD" (uppercase) contains a Textfield definition.
              - Textfields are defined by 7 subfields, separated by ","
                  "TEXTFIELD, Prompt Text, Default Value, TextfieldTag, TagNum, width, height"

               The main Sketch will get the BUTTON event via:
                 public void handleTextEvents(GEditableTextControl textcontrol, GEvent event) { 
                    println("GTextfield: " + textcontrol.getText() +  "  tag:" +  textcontrol.tag + "  tagNo:" +  textcontrol.tagNo + " >> GEvent." + event);
                 }
       
 
    void showGrid() 
    ---------------
         DISPLAY the grid
         
  
    void resetView()
    ----------------
         Reset the View to for the dataset.
         Cell (0,0) will be displayed in the top left corner
         of the view area
         
         
    void setTitle(String t)
    void setTitle(String t, float size)
    -----------------------------------
         SET the Title for the View Area 
         String text:
             will be displayed on the first row
         float size: 
             Text size for the Title
         
    
    void removeTitle()
    ------------------
        Remove title visualization

    
    void setFooter(String t)
    void setFooter(String t, float size)
    void removeFooter()
    ------------------------------------
        Same behaviour as for Title but the Text for the footer
        gets displayed on the last row of the View Area
        
    
    
    void setFirstRowAsHeader()
    void removeFirstRowAsHeader()
    -----------------------------
        Set/Remove the display of the first row in the
        dataset at a fixed position (always display as the 
        first row in the visible dataset)
        Useful if the first row in the dataset contains Column Headers
        
        
    void displayLineNumber()
    void hideLineNumber()
    -----------------------------
        Display/Hide the display of the line numbers 
  
  
    void setBackground(color fill)
    void setLineNumColor(color fill) 
    void setCellColors(color fill, color stroke, color text) 
    void setTitleColors(color fill, color stroke, color text) 
    void setFooterColors(color fill, color stroke, color text) 
    void setHeaderColors(color fill, color stroke, color text) 
    ----------------------------------------------------------
        Set the color attributes for Background, LineNumbers, Cells, Title, Footer and Header

        
    void scrollUP()
    void scrollDown()
    void scrollLeft()
    void scrollRight()
    ------------------    
        If the user wants to use KEYS other than (left,up,right,down) to 
        scroll, he/she may calls these scroll methods inside the keyPressed() 
        function of the calling Sketch 
        

    void defaultKeyEvent(int code)
    ------------------------------
        If the user is ok with the default scrolling keys (left,up,right,down),
        he/she may just calls this method in the keyPressed() function of the 
        calling Sketch 
        Example:
          
          void keyPressed() {
           sGrid.defaultKeyEvent(keyCode); 
          } 
    
    
    void scroll(int count)
    ----------------------
        Manage Mouse Wheel Scroll.
        If the user wants the View scroll on Mouse Wheel movements, 
        have to calls scroll() on mouse wheel events 
        in the main Sketch:

            void mouseWheel(MouseEvent event) {
              sGrid.scroll(event.getCount());
            }
        
  
  
  Classes in this source file
  ===========================
    Class ScrollableGrid {
        Cell[][] grid; // Cells matrix
    }
    Class Cell {
      CellBtn cellBtn; // reference to a Button Obj
      
      Inner Class CellBtn {
        GButton btn; // reference to a G4P Button
      }
    }
  
  
  Objects creation at runtime
  ===========================
    sGrid = new ScrollableGrid(this,20,20,650,280);
      ---> Creates a ScrollableGrid OBJ, no cells created yet
    sGrid.fillGrid( String lines[] );  
      ---> Creates 'n' Cell OBJ in "Cell[][] grid;"
      ---> Creates, if found in lines[], CellBtn OBJ inside Cell "grid[i][j]"
  
  
 ***************************************************************************************************************/

class Frame  {
  int x,y,width,height;
  
  Frame(int x1, int y1, int w, int h){
    this.x = x1;
    this.y = y1;
    this.width = w;
    this.height = h;
  }
  
  void setBackground(color bg){
    fill(bg);
    rect(x, y, width, height);
    ;
  }
}



public class ScrollableGrid {


  // const 
  int topBorder = 10;
  final int cellHeight = 20;

  int spaceBetweenRows = 5;  

  // the space between area left border and first colum (empty area used for line numbers)
  int leftBorder = 0;  

  // the minimal Width for all Column
  final int minWidthColumn = 83; 

  // the space between the columns (the distance/gap between the columns)
  final int spaceBetweenColumns = 5;  

  // Background color
  color col_Background  = color(#f7f7f7);   

  // Background color
  color col_LineNum  = color(#81bd4d);   

  // Title area
  int    titleHeight = 0;
  float  titleSize   = 16;
  String titleStr    = null;

  color col_TitleFillRect  = color(#d2ded6);    // Title: color rect fill  
  color col_TitleStroke    = color(#d2e7e7);    // Title: color rect outline
  color col_TitleFillText  = color( #005310);  // Title:  color text

  // Footer Area
  int  footerHeight      = 20;
  float  footerSize = 16;
  String footerStr     = null;

  color col_FooterFillRect  = color(#f4f4f4);    // Footer: color rect fill  
  color col_FooterStroke    = color(#f1f1f1);    // Footer: color rect outline
  color col_FooterFillText  = color(#444444);  // Footer:  color text


  // First Row as Header
  int headerHeight = 0;
  color col_HeaderFillRect  = color(#e5e9ed);      
  color col_HeaderStroke    = color (#d5d9dd);  
  color col_HeaderFillText  = color(#111111);    

  // Default Cell Color
  color col_CellFillRect  = color(#f7f7f7);   // Cells: color rect fill  
  color col_CellStroke    = color (#202020);  // Cells: color rect outline
  color col_CellFillText  = color(#222222);   // Cells: color text

  // Hilight Color
  color col_CellHighlightFillRect  = color(#face01);   // Cells: color rect fill  
  color col_CellHighlightStroke    = color (#faceff);  // Cells: color rect outline
  color col_CellHighlightFillText  = color(#2508d4);   // Cells: color text


  // Grid dimensions (cols/rows number) 
  public int gridX;
  public int gridY;

  // for scrolling 
  int startX = 0; // startx and starty represent the scrolling
  int startY = 0;
   
  // the grid is made of cells 
  Cell[][]grid = null;

  //boolean lockRowZero=false;
  boolean headerOn=false;
  //boolean displayLineNum=false;
  boolean lineNumOn=false;
  boolean titleOn=false;
  boolean footerOn=false;

  PApplet p = null;

  Point2D gviewOrigin;
  private Frame _frame = null;

  // Scrollbar 
  GSlider scrollbar = null; 
  float lastScrollbarCursorPos ;

  // Constructor  
  ScrollableGrid(PApplet _p, float x1, float y1, int x2, int y2) {
    this.p = _p;    
    _frame = new Frame((int)x1, (int)y1, x2, y2);

    scrollbar =  new GSlider(_p, x1+x2, y1, y2, 15, 10);
    //scrollbar.setOpaque(true); 
    scrollbar.setRotation(1.5707963268, GControlMode.CORNER);
    scrollbar.tag   = "Vscroll";
    scrollbar.setValue(0.0); 
    lastScrollbarCursorPos=-1.0;
  }    

  private void showFrame() {
    noFill();
    strokeWeight(1);
    stroke(color(240, 240, 255));
    rect(_frame.x, _frame.y, _frame.width, _frame.height);
    stroke(color(0));
    rect(_frame.x-1, _frame.y-1, _frame.width, _frame.height);
  }

   
  /*******************************************************************
   Fills the grid with values from String array passed as a parameter
   Each string in the array (row) is expected to contain a field 
   seaparator (ex: ";") used to split the string in columns
   
   Please note:
    This function may be called more than once when the dataset to 
    display changes.
    The X,Y OFFSET values for the visualization DON'T get reset by 
    this function.
    This is useful if the dataset changes when the user was scrolling it.
    This way the Visualization does not get back to 0,0 automatically.

    We can always call resetView() right after fillGrid() if we want
    to reset the visualization to 0,0.  
  *******************************************************************/   
  void fillGrid(String lines[]) {

    G4P.setCtrlMode(GControlMode.CORNER);      

    // If there is an "OLD" grid active --->
    //   Clean UP objects contained in the active one
    /////////////////////////////////////////////////
    if(grid != null) {
      this.dispose();  
    } // If previous grid exist 
      
    ///////////////////////
    // make the new grid //
    ///////////////////////

    // Count Rows and Cols 
    this.gridY = lines.length;
    String [] listTemp = split ( lines[0], ";" );
    this.gridX = listTemp.length;
    listTemp = null;  // not in use anymore
    for (int i = 0; i < gridY; i++) {     
      String [] list2 = split ( lines[i], ";" );
      if (list2.length > gridX ) {
        //println ("ScrollableGrid: Warning - different numbers of columns encountered");
        gridX=list2.length;
      }     
    }   
    
    // Allocate space for the cell matrix
    grid = new Cell[gridX+1][gridY+1];  
    
    // max column width
    float [] widthOfColumns = new float [gridX+1];
   
    // loop
    for (int i = 0; i < gridY; i++) {
      String [] list2 = split ( lines[i], ";" );
      for (int i2 = 0; i2 < list2.length; i2++) {
        // Make the new Cell
        grid[i2] [i] = new Cell (list2[i2], 
//                           i2 * (v.width/gridX) + 12, i *  (cellHeight+spaceBetweenRows),
                            i2 * (_frame.width/gridX) + 12, i *  (cellHeight+spaceBetweenRows),
                            col_CellFillRect, col_CellStroke, col_CellFillText );
        
        // If it's a BUTTON, manage it
        if( list2[i2].indexOf("BUTTON") != -1 ) {
        String [] buttonDef = split ( list2[i2], "," );
          if(buttonDef.length != 6) {
            println("fillGrid:Button Definition Error - Expectiong [6] fields, found [" + buttonDef.length + "]");
          } else {
            int  tagNo = parseInt(buttonDef[3].trim());
            int  ww    = parseInt(buttonDef[4].trim());
            int  hh    = parseInt(buttonDef[5].trim());
            //println("fillGrid: New Button at [" + i2 + "," + i + ") - Text:" + buttonDef[1] + " - Button Tag:" + buttonDef[2] + " - TagNo:" + tagNo + " - Width:" + ww + " - Height:" + hh);
            grid[i2][i].addBtn(buttonDef[1], buttonDef[2], tagNo, ww, hh);
            
            // If it's a button - use button width and not length of textual definition stored in "text" field
            if (widthOfColumns[i2] < ww+14) { 
              widthOfColumns[i2] = ww+14;
            }
          }
        } // it'a a button

        // If it's a TEXTFIELD, manage it
        if( list2[i2].indexOf("TEXTFIELD") != -1 ) {
        String [] textfieldDef = split ( list2[i2], "," );
          if(textfieldDef.length != 7) {
            println("fillGrid:TextField Definition Error - Expectiong [7] fields, found [" + textfieldDef.length + "]");
          } else {
            int  tagNo = parseInt(textfieldDef[4].trim());
            int  ww    = parseInt(textfieldDef[5].trim());
            int  hh    = parseInt(textfieldDef[6].trim());
//println("fillGrid: New Textfield at [" + i2 + "," + i + ") - Prompt:" + textfieldDef[1] + " - Default Value:" + textfieldDef[2] + " - Button Tag:" + textfieldDef[3] + " - TagNo:" + tagNo + " - Width:" + ww + " - Height:" + hh);
            //grid[i2][i].addBtn(buttonDef[1], buttonDef[2], tagNo, ww, hh);
            grid[i2][i].addTextfield(textfieldDef[1], textfieldDef[2], textfieldDef[3], tagNo, ww, hh);
            
            // If it's a textfield - use width and not length of textual definition stored in "text" field
            if (widthOfColumns[i2] < ww+14) { 
              widthOfColumns[i2] = ww+14;
            }
          }
        } // it'a a Textfield
        
        // find the longest text for column [i2] - FOR button already set width before
        if( list2[i2].indexOf("BUTTON") == -1  && list2[i2].indexOf("TEXTFIELD") == -1 ) {
          if (widthOfColumns[i2] < textWidth(list2[i2])+14) { 
            widthOfColumns[i2] = textWidth(list2[i2])+14;
          }
        }
        
      } // for each col in the row
    } // for each row
    
    // assign the width from above to the columns cells
    for (int j = 0; j<gridY; j++) {
      for (int i = 0; i<gridX; i++) {
        // make sure all columns are at least minWidthColumn wide
        widthOfColumns[i] = max (widthOfColumns[i], minWidthColumn);
        if(grid[i][j] != null){ // Allows rows with a different num of columns        
          grid[i][j].w = widthOfColumns[i];
          grid[i][j].h = cellHeight;
        }        
      }
    }
    
    // assign the x values to cells. 
    //int xCount = 21;
    int xCount = spaceBetweenColumns;
    for (int i = 0; i<gridX; i++) {
      for (int j = 0; j<gridY; j++) {
        if(grid[i][j] != null){ // Allows rows with a different num of columns               
          grid[i][j].x = xCount;
        }        
      }
      xCount += widthOfColumns[i] + spaceBetweenColumns;
    }
  }

   
   
      
  /*******************************************************************
     Clean UP objects contained in the active grid
  *******************************************************************/   
  void dispose() {  
    if(grid != null) { 
      for (int i = 0 ; i < gridY; i++) {
        for (int i2 = 0; i2 < gridX; i2++) {
          if(grid[i2][i] != null) { // Allows rows with a different num of columns               
            if(grid[i2][i].isButton) {
              // Dispose G4P BUTTON contained in Cell Button
              grid[i2][i].cellBtn.btn.dispose();
              grid[i2][i].cellBtn.btn = null;
              // Dispose Cell Button 
              grid[i2][i].cellBtn = null;
            } // is a Button
            if(grid[i2][i].isTextfield) {
              // Dispose G4P TextField contained in Cell Textfield
              grid[i2][i].cellTextfield.txtfld.dispose();
              grid[i2][i].cellTextfield.txtfld = null;
              // Dispose Cell Textfield
              grid[i2][i].cellTextfield = null;
            } // is a isTextfield

            // Dispose Cell
            grid[i2][i] = null;
          } // rows with a different num of columns
        }
      }      
      // Dispose Grid
      grid = null;
    } // If previous grid exist    
  }
  
  
  /*******************************************************************
   DISPLAY the grid  
     - Turn OFF visualization for OFF-SCREEN buttons
     - Show Title (if available)
     - Show Cells
     - Overwrite first row with column header (if available)
     - Show Footer (if available)
  *******************************************************************/
  void showGrid() {
  
    _frame.setBackground(255);
    //println("frame:" + _frame.x + "," + _frame.y+ "," + _frame.width+ "," + _frame.height);   

    // Max X,Y of the display area
    float maxX = _frame.width + _frame.x;
    float minY = topBorder + _frame.y + titleHeight + headerHeight ;
    float maxY = _frame.height + _frame.y;
    if(this.footerStr != null){
       maxY = _frame.height  + _frame.y - this.footerHeight - this.footerSize;  
    }

    // Manage Scrollbar
    ///////////////////
    int visibleRowsNum = (int) ( maxY - minY  + spaceBetweenRows) / ((cellHeight + spaceBetweenRows ) ); // Number of rows that fit into the View
    float hiddenRows =  gridY -  visibleRowsNum; // Total numer of rown not fitting the view
    float topHiddenRows = startY; // Number of rows not shown in the Top part 
    
    if(hiddenRows > 0.0) {
      scrollbar.setEnabled(true);
      scrollbar.setVisible(true);
      // Scrollbar Cursor position
      float getScrollbarCur = scrollbar.getValueF();   
      float newScrCurPos = 1 - ((hiddenRows - topHiddenRows) / hiddenRows) ; // (0<pos<1)
      if(getScrollbarCur == lastScrollbarCursorPos || lastScrollbarCursorPos == -1.0 ) {  // keyboard has been used (arrows keys) 
        // Update "Scrollbar cursor position" following startY
        scrollbar.setValue(newScrCurPos); 
        lastScrollbarCursorPos = newScrCurPos;
      } else {                                          // scrollbar has been used
        // Update startY following "Scrollbar cursor position"    
        float newStartY = hiddenRows - (hiddenRows - (getScrollbarCur * hiddenRows )) ;
        startY=(int)newStartY;
      }
      lastScrollbarCursorPos = newScrCurPos;
    } else {
      scrollbar.setEnabled(false);
      scrollbar.setVisible(false);
    }
    
    // End Scrollbar management
    ///////////////////////////
  
  
    int lastDispRow = startY + visibleRowsNum ;
    if(lastDispRow > gridY) {
      lastDispRow = gridY; // it happens when scrolling leave empty lines below the dataset
    }

    // Turn OFF offscreen buttons 
    // for rows BEFORE startY and AFTER lastDisopRow
    // and for columns BERFORE startX
    // Please Note:
    //   Check for buttons ouside the right border of the windows
    //   will be done in Cell.display() method.
    //   (last cell in a row may be partially visible
    //    and a visible part may contain a button)
    for (int i = 0 ; i < gridY; i++) {
      for (int i2 = 0; i2 < gridX; i2++) {
        if(grid[i2][i] != null){ // Allows rows with a different num of columns      
          if( i == startY && headerOn) {
            if(grid[i2][i].isButton ) {
               grid[i2][i].cellBtn.btn.setVisible(false);
            }
            if(grid[i2][i].isTextfield ) {
               grid[i2][i].cellTextfield.txtfld.setVisible(false);
            }
          }
          if( i < startY) {
            if(grid[i2][i].isButton) {
               grid[i2][i].cellBtn.btn.setVisible(false);
            }
            if(grid[i2][i].isTextfield) {
               grid[i2][i].cellTextfield.txtfld.setVisible(false);
            }
          }
          if( i >= lastDispRow ) {
            if(grid[i2][i].isButton) {
               grid[i2][i].cellBtn.btn.setVisible(false);
            }
            if(grid[i2][i].isTextfield) {
               grid[i2][i].cellTextfield.txtfld.setVisible(false);
            }
          }          
          if( i2 < startX) {
            if(grid[i2][i].isButton) {
               grid[i2][i].cellBtn.btn.setVisible(false);
            }
            if(grid[i2][i].isTextfield) {
               grid[i2][i].cellTextfield.txtfld.setVisible(false);
            }
          }
        } // rows with a different num of columns        
      }
    }

    // Display Title (if available)
    if(this.titleStr != null){
      showTitle();
    }
        
    int offsetY =  -startY*(cellHeight  +spaceBetweenRows )+topBorder + titleHeight ;
    int offsetX = 0; // SET later for each row
    
    int firtsRow = startY;
    // Overwrite first display row with the content of Row[0] 
    if(headerOn) {
      firtsRow = startY + 1;
    }    
      
    // Display ACTIVE Table rows/cols
    int k=0;
    if(headerOn) k++;
    // outer for loop (y, rows)
    //for (int i = startY; i < lastDispRow; i++) {
    for (int i = firtsRow; i < lastDispRow; i++) {
      
      if(lineNumOn) {
        //v.fill(col_LineNum);    
        //v.text(i, 3, k*(cellHeight+spaceBetweenRows) + topBorder + titleHeight + (cellHeight/2)); // Line number
        fill(col_LineNum);    
        text(i, _frame.x + 3,_frame.y + (k*(cellHeight+spaceBetweenRows) + topBorder + titleHeight + (cellHeight/2))); // Line number
      }
      // Display ROW 
      if(grid[startX][i] != null){ // Allows rows with a different num of columns
        offsetX =  -1 * int(grid[startX][i].x) + leftBorder; 
        showRow(i, maxX, offsetX, offsetY  );
      }    
      k++;
    } // y


    // Overwrite first display row with the content of Row[0] 
    if(headerOn) {
      /***
      v.stroke(col_Background);
      v.fill(col_Background);    
      v.rect(2, topBorder + titleHeight, leftBorder, cellHeight); // Black rectangle around the outside.
      ***/
      stroke(col_Background);
      fill(col_Background);    
      rect(_frame.x + 2, _frame.y + topBorder + titleHeight, leftBorder, cellHeight); // Black rectangle around the outside.
     
      if(grid[startX][0] != null){ // Allows rows with a different num of columns
        offsetY = topBorder + titleHeight ;
        offsetX = -1 * int(grid[startX][0].x) + leftBorder; // SET later for each row 
        showRow(0, maxX, offsetX, offsetY  );
      }    
    }

    // Display Footer (if available)
    if(this.footerStr != null){
      showFooter();
    }
    
  } // showGrid() 
 

  private void showRow(int rowN, float maxX, int offsetX, int offsetY  ) {        
    
    for (int i2 = startX; i2 < gridX; i2++) {
      if(grid[i2][rowN] != null){ // Allows rows with a different num of columns               
        // displaying the cell with an offsetx and offsety 
        grid[i2][rowN].display( maxX, offsetX, offsetY );
      } // rows with a different num of columns      
    } 
  } // showRow()
  

  private void showTitle() {            
      float prevSize = g.textSize;      
      textSize(this.titleSize);
  
      fill(col_TitleFillRect);
      stroke(col_TitleStroke);
      rect(_frame.x , _frame.y+ 1, _frame.width-1, this.titleSize + topBorder );     
      fill(col_TitleFillText);     
      
      text(titleStr, _frame.x + 3, _frame.y + topBorder + (this.titleSize / 2) );
      textSize(prevSize);
  }

  private void showFooter() {
      float prevSize = g.textSize;
      textSize(this.footerSize);
      
      fill(col_FooterFillRect);
      stroke(col_FooterStroke);
      rect(_frame.x ,  _frame.y + _frame.height - this.footerHeight - this.footerSize, _frame.width-1, this.footerHeight + this.footerSize - 1);     
      fill(col_FooterFillText);    
      
      text(footerStr, _frame.x + 2, _frame.y + _frame.height - this.footerHeight - this.footerSize, _frame.width-1, 30 );
      textSize(prevSize);
  }
  

  /*
   *  return a GTextField[] containing every existing
   *  GTextField in the Grid (created in fillArray()) 
   */
  GTextField [] getGTextfields(){
    ArrayList<GTextField> f = new ArrayList<GTextField>(10);;
    for (int i = 0; i<gridX; i++) {
      for (int j = 0; j<gridY; j++) {
        if(grid[i][j] != null){ // Allows rows with a different num of columns               
          if(grid[i][j].isTextfield ) {
               f.add(grid[i][j].cellTextfield.txtfld);
           }  
        }        
      }
    }
    return(f.toArray(new GTextField[0]));
  }
    
  
 /***************************
   Reset the View to 0,0
  ***************************/
  void resetView(){
    startX = 0;
    startY = 0;
  }


 /*************************************
   SET the Title for the Area 
     text to display on the first row
  *************************************/
  void setTitle(String t){
    this.titleStr = t;
    titleOn = true;
    
  }
  void setTitle(String t, float size){
    this.titleStr = t;
    this.titleHeight = (int) size + 5;
    this.titleSize = size;
    titleOn = true;
  }

  void removeTitle(){
    this.titleStr = null;
    this.titleHeight = 0;
    titleOn = false;
  }



 /*************************************
   SET footer for the Area 
     text to display on the last row
  *************************************/
  void setFooter(String t){
    this.footerStr = t;
    footerOn = true;
  }
  void setFooter(String t, float size){
    this.footerStr = t;
    this.footerHeight = (int) size - 5;
    this.footerSize = size;
    footerOn = true;
  }

  void removeFooter(){
    this.footerStr = null;
    footerOn = false;
  }


  /********************************
   ********************************/
  void setFirstRowAsHeader() {
    headerOn = true;
    headerHeight = cellHeight  + spaceBetweenRows ;
    setRowColors(0, col_HeaderFillRect, col_HeaderStroke, col_HeaderFillText);         
  }

  void removeFirstRowAsHeader() {
    headerOn = false;
    headerHeight = 0;
    setRowColors(0, col_CellFillRect, col_CellStroke, col_CellFillText);         
  }

  private void setRowColors(int rowN, color fill, color stroke, color text) {        
    for (int i = 0; i < gridX; i++) {
      if(grid[i][rowN] != null){ // Allows rows with a different num of columns
           grid[i][rowN].colCellFillRect = fill;    // color rect fill  
           grid[i][rowN].colCellStroke   = stroke;  // color rect outline    
           grid[i][rowN].colCellFillText = text;    // color text  
      } // rows with a different num of columns      
    } 
  }
  
  
  
  /********************************
   ********************************/
  void displayLineNumber() {
    lineNumOn = true;
    leftBorder = 35;
  }

  void hideLineNumber() {
    lineNumOn = false;
    leftBorder = 0;

  }


  /*********************************************************************
    Set color attributes for Cells, Title, Footer
    ********************************************************************/
  void setBackground(color fill) {
    col_Background  = fill;      
  }   
  void setLineNumColor(color fill) {
    col_LineNum  = fill;      
  }       

  void setCellColors(color fill, color stroke, color text) {
    col_CellFillRect  = fill;      
    col_CellStroke    = stroke;  
    col_CellFillText  = text;    
  }
  
  void setTitleColors(color fill, color stroke, color text) {
    col_TitleFillRect  = fill;      
    col_TitleStroke    = stroke;  
    col_TitleFillText  = text;    
  }
  
  void setFooterColors(color fill, color stroke, color text) {
    col_FooterFillRect  = fill;      
    col_FooterStroke    = stroke;  
    col_FooterFillText  = text;    
  }

  void setHeaderColors(color fill, color stroke, color text) {
    col_HeaderFillRect  = fill;      
    col_HeaderStroke    = stroke;  
    col_HeaderFillText  = text;    
  }
  
  
  /*********************************************************************
    If the user wants to use KEYS other than (left,up,right,down) to 
    scroll, calls the following scroll methods inside the keyPressed() 
    function of the calling Sketch 
    ********************************************************************/
  void scrollUP(){
    startY --;
    checkScroll();
  }
  void scrollDown(){
    startY ++;
    checkScroll();
  }
  void scrollLeft(){
    startX --;
    checkScroll();
  }
  void scrollRight(){
    startX ++;
    checkScroll();
  }

  /*********************************************************************
    If the user is ok with the default scrolling keys (left,up,right,down),
    just calls defaultKeyEvent() in the keyPressed() function of the 
    calling Sketch 
    ********************************************************************/
  void defaultKeyEvent(int code) {
    if (code==UP) {
      startY --;
    } else if (code==DOWN) {
      startY ++;
      //---
    } else if (code==LEFT) {
      startX --;
    } else if (code==RIGHT) {
      startX ++;
    } 

    //System.out.printf("ScrollableGrid() - Key codeS:[%d]\n",code);

    checkScroll();  
  } 
 
  /***************************************************
    Manage Mouse Wheel Scroll.
    The user calls scroll() on mouse wheel events:
        
        sGrid = new ScrollableGrid(this,20,20,650,280);
        .....  
        void mouseWheel(MouseEvent event) {
          sGrid.scroll(event.getCount());
        }
        
   ***************************************************/
  void scroll(int count){
    this.startY += count;
    checkScroll();
  }
 
  /****************************************************
    Set limits for scroll vars
   ****************************************************/
  void checkScroll() {
    // At leas one column to display   
    if (this.startX >= gridX) {
      this.startX--;   
    }
    // At leas one row to display   
    if (this.startY >= gridY -1) {
      this.startY--;           
    }
    if (this.startX<0)   this.startX=0;
    if (this.startY<0)   this.startY=0;

  } 
 
  //////////////////////////////////////
  // INNER CLASS ScrollableGrid::Cell //
  //////////////////////////////////////
  class Cell {
    //
    float x;  // pos 
    float y;
    float w;  // size
    float h;
    
    color colCellFillRect;  // color rect fill  
    color colCellStroke;    // color rect outline
    color colCellFillText;  // color text
  
    String textCell = "";   // content 
   
    boolean isButton = false;
    CellBtn cellBtn=null; // 
 
    CellTextfield  cellTextfield = null; 
    boolean isTextfield = false;
    
    // Constructor 
    Cell ( String text_, 
    float x_, float y_, 
    color cf_, color cs_, color ct_ ) {
      textCell = text_;
      x        = x_;
      y        = y_;
      colCellFillRect = cf_;
      colCellStroke   = cs_;
      colCellFillText = ct_;
      
    }  
   
    /**********************************************************
      Display the Cell at the position (this.x, this.y) with 
      the OFFSET specified by the (offsetX, offsetY) parameters
      
      Turn OFF screen Buttons OFF
      This is needed cause [ScrollableGrid.showGrid()] calls
      Cell.display() for rows where the Y coordinate 
      is BELOW the bottom of the view and the X coordinate is 
      AFTER the right border of the view (offscreen)
      
    **********************************************************/
    void display (float maxX, int offsetX, int offsetY  ) {
   
      if( (! this.isButton) && (! this.isTextfield) ) {
        // Normal TEXT Cell
        ///////////////////
        if( ! textCell.trim().isEmpty()) { // does not fill nor draw rectangle for emply cells
          if(_frame.x + x+offsetX + w < maxX) { // display only cells that fit completly in _frame
            displayCellText(offsetX, offsetY);
          }
        }
      } else if(this.isButton ) {
        // Cell contains a BUTTON
        /////////////////////////
  
        float moveToX =  _frame.x + x + offsetX + 5 ;
        float moveToY =  _frame.y + y + offsetY ;        
        // println( "MoveTo X:" + moveToX + "  MoveTo Y:" + moveToY + "  x:" + x + "  y:" + x + "  offsetX:" + offsetX + "  offsetY:" + offsetY + "  MaxX:" + maxX + "  MinY:" + minY + "  MaxY:" + maxY );
        this.cellBtn.btn.moveTo( moveToX, moveToY);
  
        // TURNS OFF Buttons outside the right border 
        // It may happens when the last cell in a row
        // is partially displayed
        if(moveToX + this.cellBtn.w < maxX ) {
          this.cellBtn.btn.setVisible(true);
        } else {
          this.cellBtn.btn.setVisible(false);
        }
      } else if(this.isTextfield ) {
        // Cell contains a TextField
        ////////////////////////////
  
        float moveToX =  _frame.x + x + offsetX + 5 ;
        float moveToY =  _frame.y + y + offsetY ;
        // println( "MoveTo X:" + moveToX + "  MoveTo Y:" + moveToY + "  x:" + x + "  y:" + x + "  offsetX:" + offsetX + "  offsetY:" + offsetY + "  MaxX:" + maxX );
        this.cellTextfield.txtfld.moveTo( moveToX, moveToY);
 
        // TURNS OFF TextFields outside the right border 
        // It may happens when the last cell in a row
        // is partially displayed
        if(moveToX + this.cellTextfield.w < maxX ) {
          this.cellTextfield.txtfld.setVisible(true);
        } else {
          this.cellTextfield.txtfld.setVisible(false);
        }
      } 
  
    }


    /**
     * Check for inline color information for cell text
     * and set Backgroud, Border and Text color accordingly
     */    
    private void displayCellText(int offsetX, int offsetY ){     
      color bgColor = colCellFillRect;
      color borderColor = colCellStroke;
      color textColor = colCellFillText;
      String txt = textCell;
      String fmtTag = "";
      
      if(textCell.indexOf("<R>") != -1) { // Reverse       
        bgColor = colCellFillText ;
        borderColor = colCellStroke;
        textColor = colCellFillRect;
        fmtTag="<R>";
      }
      else if(textCell.indexOf("<H>") != -1) {  // Use Header Color
        bgColor = col_HeaderFillRect ;
        borderColor = col_HeaderStroke;
        textColor = col_HeaderFillText;
        fmtTag="<H>";
      }
      else if(textCell.indexOf("<L>") != -1) {  // Use Hilight Color
        bgColor = col_CellHighlightFillRect ;
        borderColor = col_CellHighlightStroke;
        textColor = col_CellHighlightFillText;
        fmtTag="<L>";
      }
 
      if( ! fmtTag.isEmpty()){
       txt = txt.replaceAll(fmtTag,"");
      }
      fill(bgColor);
      stroke(borderColor);
      rect(_frame.x + x+offsetX, _frame.y + y+offsetY, w, h);
      fill(textColor);
      text (txt, 
      _frame.x + x+5+offsetX, _frame.y + y+3+offsetY, 
      w, h );
      
      return;
      
    }
    
  
    /***********************************************************
      ADD a New CellBtn OBJ to the Cell
      The CellBtn Object will instantiate a new GButton
     ***********************************************************/
    void addBtn(String text, String t, int tn, int ww, int hh) {
      this.cellBtn = new CellBtn(text, t, tn , ww, hh); 
      this.isButton = true;
    }
    
    ///////////////////////////////////////////////
    // INNER CLASS ScrollableGrid::Cell::CellBtn //
    ///////////////////////////////////////////////
    class CellBtn {
      GButton btn;
      String tag;
      int tagNo;
      int w;
      int h;
      
      CellBtn (String text, String tagId, int tagN,  int ww, int hh ){
      //  tag (String) and tagNo (int). 
        this.tag = tagId;
        this.tagNo = tagN;
        this.w = ww;
        this.h = hh;
    
        btn = new GButton(p, x , y , ww, hh, text );
        btn.tag   = this.tag;
        btn.tagNo = this.tagNo;
        btn.setVisible(true);
      }
    }  // INNER class CellBtn
 

    /***********************************************************
      ADD a New CellTextfield OBJ to the Cell
      The CellTextfield Object will instantiate a new GTextField
     ***********************************************************/
    void addTextfield(String prompt, String dflt, String t, int tn, int ww, int hh) {
      this.cellTextfield = new CellTextfield(prompt,dflt, t, tn , ww, hh); 
      this.isTextfield = true;
    }
    
    ///////////////////////////////////////////////
    // INNER CLASS ScrollableGrid::Cell::CellBtn //
    ///////////////////////////////////////////////
    class CellTextfield {
      GTextField txtfld;
      String tag;
      int tagNo;
      int w;
      int h;
      
      CellTextfield (String prompt, String dflt, String tagId, int tagN,  int ww, int hh ){
        //  tag (String) and tagNo (int). 
        this.tag = tagId;
        this.tagNo = tagN;
        this.w = ww;
        this.h = hh;

        //System.out.printf(" new() CellTextfield(%s,%s,%s,%f,%f,%d,%d) \n", prompt, dflt,tagId,x , y , ww, hh);
        txtfld = new GTextField(p, x , y , ww, hh, G4P.SCROLLBARS_NONE);
        txtfld.tag   = this.tag;
        txtfld.tagNo = this.tagNo;
        
        txtfld.setText(dflt);
//        txtfld.setPromptText(prompt);
        txtfld.setVisible(true);
        txtfld.setOpaque(true);
//        txtfld.setTextEditEnabled(true);


      }
    }  // INNER class CellTextfield
 
    
  } // INNER class Cell
  

  /////////////////////////////////////////
  // INNER CLASS ScrollableGrid::Point2D //
  /////////////////////////////////////////  
  public class Point2D {
    float x;
    float y;
    Point2D( float _x, float _y ) {
      x=_x;
      y=_y;
    }
  } 
  
} // class ScrollableGrid
 
