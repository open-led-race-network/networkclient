
class Race extends OLRNBaseItem {

  private Hashtable<String,String> _pList; 
  public String _updatedBy;
  
  String partecipantsChannelStr;   // Currently OLR/basePool/race/<RaceId>/Participants/<TrackId> 
  String raceSendTopicStr;         // Currently OLR/basePool/race/<RaceId>/Send/<TrackId> 
  String raceConfigTopicStr;       // Currently OLR/basePool/race/<RaceId>/Config 
  String raceCarsTopicStr;         // Currently "OLR/basePool/race/_RaceId_/Cars/_CarId_" 

  
  Race ()   {
    super ();
    _pList = new Hashtable<String,String>();
    this._updatedBy = "-";
  }
    
  Race (String id,String name,String status ){
    super(id,name,status);
    _pList = new Hashtable<String,String>();
    this._updatedBy = "-";
  }

  Race (String id,String name,String status, String updBy ){
    super(id,name,status);
    _pList = new Hashtable<String,String>();
    this._updatedBy = updBy;
  }

  void update(String name, String status, String updBy) {
    this._name   = name;
    this._status = status;
    this._updatedBy   = updBy;
    this._tstamp = LocalDateTime.now().toString(); 
  }

//  void updateStatus(String status, String updBy) {
  void updStatus(String status, String updBy) {  
    this._status = status;
    this._updatedBy   = updBy;
    this._tstamp = LocalDateTime.now().toString(); 
  }

  void setFields(String id, String name, String status, String updBy) {
    super.setFields(id,name,status);
    this._updatedBy = updBy;
    this._tstamp = LocalDateTime.now().toString(); 
  }
  
  public boolean partnerExist(String id)  {
    if(_pList.get(id)==null){
      return(false);
    }
    return(true);
  }

  public int numberOfPartners()  {    
    return(_pList.size()); 
  }
  
  public String[] getPartnersId()  {    
    String[] p = new String[_pList.size()];
    Enumeration ids = _pList.keys();
    int i=0;
    while(ids.hasMoreElements()) {
      p[i++] = new String((String) ids.nextElement()).trim();
    } 
    return(p); 
  }  

  // Add a Partner to Partners List
  void  addPartner (String id)  {
    _pList.put(id,"");
    return;
  }

  public int removePartner(String id)  {
    String s = _pList.remove(id);
    if(s==null){
      return(0);
    }
    return(1);
  }

} 



/**
 *  List of OLRNBaseItem
 *    OLRNBaseItem.id Attribute is UNIQUE and used to access
 *    list items
 */
class RaceList extends Race {
   
  private ArrayList<Race> itemList;
  public long lastUpdate = 0;  // Uses public static long currentTimeMillis()
  
  // Constructor
  RaceList() {
    super("","","");
    itemList = new ArrayList<Race>();
    setLastUpdateTime();
    
    
  }

  void setLastUpdateTime(){
    lastUpdate = System.currentTimeMillis(); 
  }

  // Add item 
  boolean add (String id, String name, String status, String updatedBy){
    if( ! chkParams("RaceList.add", id, name, status)){return false;}
    // Check if item(id) already exist
    if( exist(id)){
      System.out.printf("RaceList.add() ERROR - id=[%s] exist\n",id);
      return false;    
    }
    Race item = new Race (id, name, status, updatedBy);
    itemList.add(item);
    setLastUpdateTime();
    return true;
  }
  
  // Update item 
  boolean update (String id, String name, String status, String updBy){
    if( ! chkParams("RaceList.update", id, status)){return false;}
    int idx = indexOf(id); 
    if( idx < 0){ 
      System.out.printf("RaceList.update() ERROR - id=[%s] DOES NOT exist\n", id);
      return(false);  
    } 
    itemList.get(idx).update(name, status, updBy);
    setLastUpdateTime();
    return true;
  }  
  
  boolean updateStatus (String id, String status, String updBy){
    if( ! chkParams("RaceList.update", id, status)){return false;}
    int idx = indexOf(id); 
    if( idx < 0){ 
      System.out.printf("RaceList.update() ERROR - id=[%s] DOES NOT exist\n", id);
      return(false);  
    } 
    itemList.get(idx).updStatus(status, updBy);
    setLastUpdateTime();
    return true;
  } 



  /**
    Remove item
  **/  
  boolean delete (String id){

    if( ! chkParams("RaceList.delete", id)){return false;}
    int idx = indexOf(id); 
    if( idx < 0){    // ID not found 
      System.out.printf("RaceList.delete() ERROR - Race=[%s] DOES NOT exist\n", id);
      return(false);  
    } 
    itemList.remove(idx);
    setLastUpdateTime();
    return(true);
  }

  /**
   * Return timestamp of the Last Update Operation 
   */
  public long getUpdateTstamp() {
    return(this.lastUpdate); 
  }

  public void setUpdateTstamp(long l) {
    this.lastUpdate = l; 
  }
        
  public int indexOf(String id)  {
    // Returns -1 on NOT FOUND
    int n = itemList.size();
    for (int i=0; i<n ; i++) {
      if (itemList.get(i)._id.equals(id.trim())){
           return(i);
      }
    }
    return(-1); 
  }  
  
  public boolean exist(String id) {
    int idx = indexOf(id); 
    if( idx >= 0){    // ID found
      return(true);
    }
    return(false);
  }  

  /**
   * Return name attribute for item(id) 
   */
  public String getName(String id) {
    int idx = indexOf(id);  
    if( idx < 0){    // ID not found
        return(null);
    }  
    return(itemList.get(idx)._name);
  }

  /**
  *  Get Status for item(id)
  *  
  * @return  Status: if a Device[id] exist
  * @return  "-" : On NOT FOUND
  */
  String getStatus(String id){
    
    int idx = indexOf(id); 
    if( idx < 0){    // ID not found
      return("-");
    }
    return(itemList.get(idx)._status);
  }

  /**
   * Return tstamp attribute for item(id)
   */
  public String getTStamp(String id) {
    int idx = indexOf(id);  
    if( idx < 0){    // ID not found
        return(null);
    }  
    return(itemList.get(idx)._tstamp);
  }

  /**
    * @param String id   
    *
    * @return String[]: {id, name, status, tstamp }
    * @return null:     id not found
  **/      
  public String[] getAttributes(String id)  {  
    if( this.exist(id)) {
      return(new String[] {id,getName(id),getStatus(id), getTStamp(id)});    
    }  
    return(null);
  }
    
  public Race get(String id)  {
    // Returns -1 on NOT FOUND
    int n = itemList.size();
    for (int i=0; i<n ; i++) {
      Race item = itemList.get(i);
      if (item._id.equals(id.trim())){
           return(item);
      }
    }
    return(null); 
  }  



  /**
    Add Partner to the [OLRNBaseItem] 
  **/  
  int addPartner (String id, String pId)  {
    int idx = indexOf(id);  
    if( idx < 0){    
        System.out.printf("RaceList.addPartner() ERROR - id=[%s] NOT FOUND in itemList\n", id);
        return(-1);
    }   
    // Check if the Partner(id) already exist
    if( itemList.get(idx).partnerExist(pId)) {  // Partner ID found
      System.out.printf("RaceList.addPartner() ERROR - addPartner [%s] Exist\n", id);
      return(-1);    
    }
    itemList.get(idx).addPartner(pId);

    // lastUpdate time = now 
    setLastUpdateTime();
    
    return(0);
  }


  /**
    Remove Partner to the race at position idx (raceArrayList[idx])
    
    returns: 
      int: Number of removed Partners
  **/  
  int removePartner (String id, String pId)  {
    
    // lastUpdate time = now 
    setLastUpdateTime();
            
    int listIdx = indexOf(id);
    if( listIdx < 0){    
        System.out.printf("RaceList.removePartner() ERROR - raceId=[%s] NOT FOUND in Race list\n",  id);
        return(-1);
    } 
    
    int removed = itemList.get(listIdx).removePartner(pId);
    if( removed != 1){  
      System.out.printf("RaceList.removePartner() ERROR - Removed [%d] Partner(s) instead of [1]\n", removed);
    }
    return(removed);
  }
 

  public boolean partnerExist(String id, String pId)  { 
    int listIdx = indexOf(id);
    if( listIdx < 0){   
        System.out.printf("RaceList.partnerExist() ERROR - id=[%s] NOT FOUND in itemList\n", id);
        return(false);
    } 
    return(itemList.get(listIdx).partnerExist(pId));
  }  

  
  public int numberOfPartners(String id)  { 
    Race r = this.get(id);
    if( r != null){  
      return(r.numberOfPartners());
    }
    return(-1);
  }



  /**
    * Find the race where the Device=id is subscribed as a Partner 
    *
    * @param id   
    *
    * @return String[]: {id, name, status }
    * @return null:      on Partner not found in any Race
  **/      
  public String[] getWherePartner(String id)  {
    int n = itemList.size();
    for (int i=0; i<n ; i++) {
      Race r = itemList.get(i);
      if(r.partnerExist(id)){
        return(new String[] {r._id,r._name,r._status});    
      }     
    }
    return(null);
  } 


  /**
    * Return an array containing the ids of partners for item(id) 
    *
    * @param id  item(id)
    *
    * @return String[]: Array with ids of partners  
    * @return String[]: null on item(id) not found
  **/
  public String[] getPartnersId(String id) {
    int listIdx = indexOf(id);
    if( listIdx < 0){   
        System.out.printf("RaceList.getPartnersId() ERROR - id=[%s] NOT FOUND in Race list\n", id);
        return(null);
    } 
    return( itemList.get(listIdx).getPartnersId());
  }
  
  
  /**
   * get List Content 
   *
   * @return String[n][5] [n]:Items / [6]:{id, name, status, tstamp, numberOfPartners}
   */
  String[][] getContent(){
    int n = itemList.size();
    String[][] l = new String[n][5];
    Race item;
    for (int i=0; i<n ; i++) {
      item=itemList.get(i);
      l[i][0]=item._id.trim();  
      l[i][1]=item._name.trim();
      l[i][2]=item._status.trim();
      l[i][3]=item._tstamp.trim();
      l[i][4]=String.valueOf(itemList.get(i).numberOfPartners());
    }
    return(l);
  }
  
  
  String toString(){
    String s="Race List:\n";
    String[][] l = getContent();
    for(int i=0 ; i<l.length;i++ ) {
      s = String.format("%s  [%s][%s][%s][%s][%s]\n", s, l[i][0], l[i][1], l[i][2], l[i][3], l[i][4]); 
    }
    return(s);
  }

} 
