﻿

# Open Led Race Network - OLRNetwork


## What is it
    A software connecting together more OpenLedRace devices, allowing
    to create Relay Races.
    The basic idea is to create a "Network-Enabler Software" for OLR Devices.

The video below shows the Software in this repository connecting two OLR Devices, both running in DEBUG mode
(no players, the firmware installed on the two OLR Devices simulates Red and Green player)<BR>
When a car terminates the configured laps in a circuit is 'sent' to the next one,
where the race continue.
<BR>
<!-- blank line -->
<figure class="video_container">
  <iframe src="http://www.openrelayrace.org/storage/app/media/RelayRace_1.mp4" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

See: http://www.openrelayrace.org/storage/app/media/RelayRace_1.mp4

Please Note:
- There are two Network Clients managing one OLR Device each.
They exchange MQTT messages via an "external" MQTT broker (the Broker is **not** running on a server in the local network).
Messages travel through the Internet to reach the broker and come back. 
This means the ‘delay’ you see between the moment when a car ‘leave’ a circuit and ‘enter’ the other 
would be the same if one of the circuit was in a different place (city, state, etc).

## Current Implementation - Proof of Concept 0.9.8

The current implementation is intended to be a "proof of concept" to test
the proposed implementation:
- MQTT Broker as the only Server in the system
- OLRNetwork Software connecting a Physical OLR Device to the Network

The software is written in "Processing 4 / Java" (https://processing.org/) and works on Linux/MacOSX/Windows PC.<BR>
It implements a very basic User Interface (UX) **not for general public usage**
(not user friendly: expose low level software parameters to test the system)

## Current implementation Tests

Test Races done so far, using the current implementation, have shown
that the basic idea works well enough to implements Relay Races between
internet-connected OLRNetwork Devices.

A very interesting and fundamental thing in the system is the Time taken for a
car to "move" from one Circuit to the next one (**"Change Device"** delay).<BR>
This time have to be short and "consistent" (have to be almost the same for
each  car).<BR>

### "Change Device" delay
Delay for a Car moving from one OLR Device to the Next in a Relay Race

During a Relay Race, when a car finished the assigned Laps , is sent to the next OLRDevice<BR>
This process involves:
- (a) Board currently hosting the car SENDs a Serial Message to the NetworkDevice Software (Arduino->SBC)
- (b) The Software sends/receives some messages to manage the change of Board for the car (SBC<->Broker)
- (c) The Software connected to the Board where the car is coming, SENDs a Serial Message to its Board (SBC->Arduino)

The total time, sum of the above, is called **"Change Device" delay** and is what a user "see" when the two boards are physically in the same place.


#### Results

- BROKER: **moquitto 1.6.5 on Ethernet-connceted RPI3**
  - Total approx Change Device delay: **[41]-[97] mSec**

- BROKER: **shiftr.io**
  - Total approx Change Device delay: **[216]-[343] mSec**

**Please Note:**
If you compile the Arduino code in "Simulated" mode (DEBUG) and with the two cars running at the same speed,
you will just see **one** car (the other one is 'underneat" so not visible).
So, afters the first set of loops in the first circuit, **both cars LEAVE the circuit at the same time**.

What happens in this case is the FIRST CAR LEAVING take advantage of that
and appears FIRST in the next Device. So now you'll see the two cars, the last
that left being few leds behind.

This is because the software sends/receives MQTT messages and talk to
serial. All of this takes time and the first car leaving the Device starts
the process before and the board receiving the cars will process some more Messages
sent for the FIRTS car before being able to take care of the Second Car.

And, if any other message is originated in the OLRNetwork during the "Car Change Device"
process, a lot more LAG may be introduced.

-- See "First Publicly Available Version 1.0 Improvement" below


### What does this means

- A Relay Race between different internet connected places can work reasonably
  well with [shifter.io] network Broker.

- As expected, Relay Race between devices in the same room using internet Broker (shiftr.io)
  are heavily influenced by Network hiccups in "Car Change Device" phase.

- Relay Race between devices in the same room using [Local Ethernet-connected RPI running mosquitto]
  works well if you postulate something like this:<BR>
  ``"Hyper Space Bridge" is very narrow. Only one Car at the time can get through.
    In case 2 cars reach it at almost the same time, the first one arriving
    get in before, gaining some time over the second one.``


